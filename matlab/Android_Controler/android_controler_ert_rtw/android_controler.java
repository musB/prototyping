package com.example.android_controler;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;
import android.util.Log;
import java.util.ArrayList;
import android.content.res.Configuration;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.ToggleButton;
import java.util.Hashtable;

public class android_controler extends AppCompatActivity implements SensorEventListener, OnFragmentInteractionListener {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
     private Hashtable<Integer,Float> buttonStates = new Hashtable<Integer,Float>();
     private float[] mAccelerometerData = { 0.0f, 0.0f, 0.0f };
     private SensorManager mSensorManager;

     private void registerSensorManager() {
        mSensorManager.registerListener(this,
            mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
            SensorManager.SENSOR_DELAY_FASTEST);
     }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //Uncomment the following line to specify a custom App Title
        //toolbar.setTitle("My custom Title");
        setSupportActionBar(toolbar);

        // Create a FragmentPagerAdapter that returns individual fragments
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(SectionsPagerAdapter.getNumTabs()-1);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        // Initiate the SensorManager
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        thisClass = this;
     }

    private android_controler thisClass;
    private final Thread BgThread = new Thread() {
    @Override
    public void run() {
            String argv[] = new String[] {"MainActivity","android_controler"};
            naMain(argv, thisClass);
        }
    };

    public void flashMessage(final String inMessage) {
        runOnUiThread(new Runnable() {
              public void run() {
                    Toast.makeText(getBaseContext(), inMessage, Toast.LENGTH_SHORT).show();
              }
        });
    }

    @Override
    protected void onDestroy() {
         naTerminate();
         super.onDestroy();
         System.exit(0); //to kill all our threads.
    }

	@Override
    public void onFragmentCreate(String name) {

    }

    @Override
   public void onFragmentStart(String name) {
       switch (name) {
           case "Info":
               break;
            case "App":
                if (!BgThread.isAlive()) {
                    BgThread.start();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onFragmentResume(String name) {
    }

    @Override
    public void onFragmentPause(String name) {
    }
    @Override
    protected void onResume() {
         super.onResume();
         registerSensorManager();
    }

    @Override
    protected void onPause() {
         mSensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void registerButtonFcn(int id) {
        String buttonid     = "button"+id;
        final ToggleButton button = (ToggleButton)findViewById(getResources().getIdentifier(buttonid, "id", getPackageName()));
        setButtonState(button);
        button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
                setButtonState(button);
			}
		});
    }

    // update the hash table with button id and state
    public void setButtonState(ToggleButton button) {
    	if(button.isChecked()) {
            buttonStates.put(button.getId(),1.0f);
		} else {
            buttonStates.put(button.getId(),0.0f);
		}
    }

    public float getButtonState(int id) {
        String buttonid = "button"+id;
        return buttonStates.get(getResources().getIdentifier(buttonid, "id", getPackageName()));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float [] values = event.values;
        //Comment out if you want to log the data in logcat
        //String logMessage = String.format("%d: 0'%g'", event.sensor.getType(), values[0]);
        //Log.d("Sensor Data IN:", logMessage);
        switch(event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                mAccelerometerData[0] = values[0];
                mAccelerometerData[1] = values[1];
                mAccelerometerData[2] = values[2];
                break;
        }
    }

    // Get SensorEvent Data throws exception if the data is null
    public float[] getAccelerometerData() {
        return mAccelerometerData;
    }

    private native int naMain(String[] argv, android_controler pThis);
    private native void naTerminate();
    static {
        System.loadLibrary("android_controler");
    }

}
