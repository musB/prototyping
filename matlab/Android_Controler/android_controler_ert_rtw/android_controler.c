/*
 * File: android_controler.c
 *
 * Code generated for Simulink model 'android_controler'.
 *
 * Model version                  : 1.10
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Sat Dec 16 14:29:59 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "android_controler.h"
#include "android_controler_private.h"
#include "android_controler_dt.h"

/* Block signals (auto storage) */
B_android_controler_T android_controler_B;

/* Block states (auto storage) */
DW_android_controler_T android_controler_DW;

/* Real-time model */
RT_MODEL_android_controler_T android_controler_M_;
RT_MODEL_android_controler_T *const android_controler_M = &android_controler_M_;

/* Model step function */
void android_controler_step(void)
{
  boolean_T p;
  boolean_T p_0;
  real32_T out[3];

  /* Start for MATLABSystem: '<S1>/Accelerometer' incorporates:
   *  MATLABSystem: '<S1>/Accelerometer'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj.SampleTime ==
        android_controler_P.Accelerometer_SampleTime)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj.SampleTime =
      android_controler_P.Accelerometer_SampleTime;
  }

  out[0] = 0.0F;
  out[1] = 0.0F;
  out[2] = 0.0F;
  GET_ACCELEROMETER_DATA(&out[0], &out[1], &out[2]);

  /* MATLABSystem: '<S1>/Accelerometer' incorporates:
   *  Start for MATLABSystem: '<S1>/Accelerometer'
   */
  android_controler_B.Accelerometer_o1 = out[0];
  android_controler_B.Accelerometer_o2 = out[1];
  android_controler_B.Accelerometer_o3 = out[2];

  /* Start for MATLABSystem: '<S8>/Button' incorporates:
   *  MATLABSystem: '<S8>/Button'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj_dk.SampleTime ==
        android_controler_P.Button_SampleTime)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj_dk.SampleTime =
      android_controler_P.Button_SampleTime;
  }

  /* MATLABSystem: '<S8>/Button' incorporates:
   *  Start for MATLABSystem: '<S8>/Button'
   */
  p = BUTTON_GETSTATE(1.0);

  /* Logic: '<Root>/Logical Operator1' incorporates:
   *  Constant: '<S2>/Constant'
   *  MATLABSystem: '<S8>/Button'
   *  RelationalOperator: '<S2>/Compare'
   */
  android_controler_B.LogicalOperator1 = ((android_controler_B.Accelerometer_o2 >=
    android_controler_P.CompareToConstant_const) || p);

  /* Start for MATLABSystem: '<S9>/Button' incorporates:
   *  MATLABSystem: '<S9>/Button'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj_d.SampleTime ==
        android_controler_P.Button_SampleTime_n)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj_d.SampleTime =
      android_controler_P.Button_SampleTime_n;
  }

  /* MATLABSystem: '<S9>/Button' incorporates:
   *  Start for MATLABSystem: '<S9>/Button'
   */
  p = BUTTON_GETSTATE(2.0);

  /* Logic: '<Root>/Logical Operator' incorporates:
   *  Constant: '<S4>/Constant'
   *  MATLABSystem: '<S9>/Button'
   *  RelationalOperator: '<S4>/Compare'
   */
  android_controler_B.LogicalOperator = (p ||
    (android_controler_B.Accelerometer_o1 >=
     android_controler_P.CompareToConstant2_const));

  /* Start for MATLABSystem: '<S10>/Button' incorporates:
   *  MATLABSystem: '<S10>/Button'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj_g.SampleTime ==
        android_controler_P.Button_SampleTime_k)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj_g.SampleTime =
      android_controler_P.Button_SampleTime_k;
  }

  /* MATLABSystem: '<S10>/Button' incorporates:
   *  Start for MATLABSystem: '<S10>/Button'
   */
  p = BUTTON_GETSTATE(3.0);

  /* Logic: '<Root>/Logical Operator2' incorporates:
   *  Constant: '<S7>/Constant'
   *  MATLABSystem: '<S10>/Button'
   *  RelationalOperator: '<S7>/Compare'
   */
  android_controler_B.LogicalOperator2 = ((android_controler_B.Accelerometer_o1 <=
    android_controler_P.CompareToConstant5_const) || p);

  /* Start for MATLABSystem: '<S11>/Button' incorporates:
   *  MATLABSystem: '<S11>/Button'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj_l.SampleTime ==
        android_controler_P.Button_SampleTime_c)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj_l.SampleTime =
      android_controler_P.Button_SampleTime_c;
  }

  /* MATLABSystem: '<S11>/Button' incorporates:
   *  Start for MATLABSystem: '<S11>/Button'
   */
  p = BUTTON_GETSTATE(4.0);

  /* Logic: '<Root>/Logical Operator4' incorporates:
   *  Constant: '<S6>/Constant'
   *  MATLABSystem: '<S11>/Button'
   *  RelationalOperator: '<S6>/Compare'
   */
  android_controler_B.LogicalOperator4 = ((android_controler_B.Accelerometer_o3 <=
    android_controler_P.CompareToConstant4_const) || p);

  /* Start for MATLABSystem: '<S12>/Button' incorporates:
   *  MATLABSystem: '<S12>/Button'
   */
  p = false;
  p_0 = true;
  if (!(android_controler_DW.obj_a.SampleTime ==
        android_controler_P.Button_SampleTime_h)) {
    p_0 = false;
  }

  if (p_0) {
    p = true;
  }

  if (!p) {
    android_controler_DW.obj_a.SampleTime =
      android_controler_P.Button_SampleTime_h;
  }

  /* MATLABSystem: '<S12>/Button' incorporates:
   *  Start for MATLABSystem: '<S12>/Button'
   */
  p = BUTTON_GETSTATE(5.0);

  /* Logic: '<Root>/Logical Operator3' incorporates:
   *  Constant: '<S5>/Constant'
   *  MATLABSystem: '<S12>/Button'
   *  RelationalOperator: '<S5>/Compare'
   */
  android_controler_B.LogicalOperator3 = ((android_controler_B.Accelerometer_o2 <=
    android_controler_P.CompareToConstant3_const) || p);

  /* External mode */
  rtExtModeUploadCheckTrigger(1);

  {                                    /* Sample time: [0.1s, 0.0s] */
    rtExtModeUpload(0, android_controler_M->Timing.taskTime0);
  }

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.1s, 0.0s] */
    if ((rtmGetTFinal(android_controler_M)!=-1) &&
        !((rtmGetTFinal(android_controler_M)-
           android_controler_M->Timing.taskTime0) >
          android_controler_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(android_controler_M, "Simulation finished");
    }

    if (rtmGetStopRequested(android_controler_M)) {
      rtmSetErrorStatus(android_controler_M, "Simulation finished");
    }
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   */
  android_controler_M->Timing.taskTime0 =
    (++android_controler_M->Timing.clockTick0) *
    android_controler_M->Timing.stepSize0;
}

/* Model initialize function */
void android_controler_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)android_controler_M, 0,
                sizeof(RT_MODEL_android_controler_T));
  rtmSetTFinal(android_controler_M, -1);
  android_controler_M->Timing.stepSize0 = 0.1;

  /* External mode info */
  android_controler_M->Sizes.checksums[0] = (3030048473U);
  android_controler_M->Sizes.checksums[1] = (758971308U);
  android_controler_M->Sizes.checksums[2] = (2856984802U);
  android_controler_M->Sizes.checksums[3] = (2381021020U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[7];
    android_controler_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = &rtAlwaysEnabled;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(android_controler_M->extModeInfo,
      &android_controler_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(android_controler_M->extModeInfo,
                        android_controler_M->Sizes.checksums);
    rteiSetTPtr(android_controler_M->extModeInfo, rtmGetTPtr(android_controler_M));
  }

  /* block I/O */
  (void) memset(((void *) &android_controler_B), 0,
                sizeof(B_android_controler_T));

  /* states (dwork) */
  (void) memset((void *)&android_controler_DW, 0,
                sizeof(DW_android_controler_T));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    android_controler_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.BTransTable = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.PTransTable = &rtPTransTable;
  }

  /* Start for MATLABSystem: '<S1>/Accelerometer' */
  android_controler_DW.obj.isInitialized = 0;
  android_controler_DW.obj.SampleTime =
    android_controler_P.Accelerometer_SampleTime;
  android_controler_DW.obj.isInitialized = 1;
  INITIALIZE_ACCELEROMETER();

  /* Start for MATLABSystem: '<S8>/Button' */
  android_controler_DW.obj_dk.isInitialized = 0;
  android_controler_DW.obj_dk.SampleTime = android_controler_P.Button_SampleTime;
  android_controler_DW.obj_dk.isInitialized = 1;
  BUTTON_INIT(1.0);

  /* Start for MATLABSystem: '<S9>/Button' */
  android_controler_DW.obj_d.isInitialized = 0;
  android_controler_DW.obj_d.SampleTime =
    android_controler_P.Button_SampleTime_n;
  android_controler_DW.obj_d.isInitialized = 1;
  BUTTON_INIT(2.0);

  /* Start for MATLABSystem: '<S10>/Button' */
  android_controler_DW.obj_g.isInitialized = 0;
  android_controler_DW.obj_g.SampleTime =
    android_controler_P.Button_SampleTime_k;
  android_controler_DW.obj_g.isInitialized = 1;
  BUTTON_INIT(3.0);

  /* Start for MATLABSystem: '<S11>/Button' */
  android_controler_DW.obj_l.isInitialized = 0;
  android_controler_DW.obj_l.SampleTime =
    android_controler_P.Button_SampleTime_c;
  android_controler_DW.obj_l.isInitialized = 1;
  BUTTON_INIT(4.0);

  /* Start for MATLABSystem: '<S12>/Button' */
  android_controler_DW.obj_a.isInitialized = 0;
  android_controler_DW.obj_a.SampleTime =
    android_controler_P.Button_SampleTime_h;
  android_controler_DW.obj_a.isInitialized = 1;
  BUTTON_INIT(5.0);
}

/* Model terminate function */
void android_controler_terminate(void)
{
  /* Start for MATLABSystem: '<S1>/Accelerometer' incorporates:
   *  Terminate for MATLABSystem: '<S1>/Accelerometer'
   */
  if (android_controler_DW.obj.isInitialized == 1) {
    android_controler_DW.obj.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S1>/Accelerometer' */

  /* Start for MATLABSystem: '<S8>/Button' incorporates:
   *  Terminate for MATLABSystem: '<S8>/Button'
   */
  if (android_controler_DW.obj_dk.isInitialized == 1) {
    android_controler_DW.obj_dk.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S8>/Button' */

  /* Start for MATLABSystem: '<S9>/Button' incorporates:
   *  Terminate for MATLABSystem: '<S9>/Button'
   */
  if (android_controler_DW.obj_d.isInitialized == 1) {
    android_controler_DW.obj_d.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S9>/Button' */

  /* Start for MATLABSystem: '<S10>/Button' incorporates:
   *  Terminate for MATLABSystem: '<S10>/Button'
   */
  if (android_controler_DW.obj_g.isInitialized == 1) {
    android_controler_DW.obj_g.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S10>/Button' */

  /* Start for MATLABSystem: '<S11>/Button' incorporates:
   *  Terminate for MATLABSystem: '<S11>/Button'
   */
  if (android_controler_DW.obj_l.isInitialized == 1) {
    android_controler_DW.obj_l.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S11>/Button' */

  /* Start for MATLABSystem: '<S12>/Button' incorporates:
   *  Terminate for MATLABSystem: '<S12>/Button'
   */
  if (android_controler_DW.obj_a.isInitialized == 1) {
    android_controler_DW.obj_a.isInitialized = 2;
  }

  /* End of Start for MATLABSystem: '<S12>/Button' */
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
