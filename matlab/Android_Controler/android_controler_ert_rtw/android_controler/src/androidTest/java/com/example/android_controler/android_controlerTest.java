package com.example.android_controler;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.example.android_controler.android_controlerTest \
 * com.example.android_controler.tests/android.test.InstrumentationTestRunner
 */
public class android_controlerTest extends ActivityInstrumentationTestCase2<android_controler> {

    public android_controlerTest() {
        super("com.example.android_controler", android_controler.class);
    }

}
