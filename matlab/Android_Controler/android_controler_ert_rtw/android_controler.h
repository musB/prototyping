/*
 * File: android_controler.h
 *
 * Code generated for Simulink model 'android_controler'.
 *
 * Model version                  : 1.10
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Sat Dec 16 14:29:59 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_android_controler_h_
#define RTW_HEADER_android_controler_h_
#include <float.h>
#include <string.h>
#include <stddef.h>
#ifndef android_controler_COMMON_INCLUDES_
# define android_controler_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "driver_android_accelerometer.h"
#include "driver_android_button.h"
#endif                                 /* android_controler_COMMON_INCLUDES_ */

#include "android_controler_types.h"

/* Shared type includes */
#include "multiword_types.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

/* Block signals (auto storage) */
typedef struct {
  real32_T Accelerometer_o1;           /* '<S1>/Accelerometer' */
  real32_T Accelerometer_o2;           /* '<S1>/Accelerometer' */
  real32_T Accelerometer_o3;           /* '<S1>/Accelerometer' */
  boolean_T LogicalOperator1;          /* '<Root>/Logical Operator1' */
  boolean_T LogicalOperator;           /* '<Root>/Logical Operator' */
  boolean_T LogicalOperator2;          /* '<Root>/Logical Operator2' */
  boolean_T LogicalOperator4;          /* '<Root>/Logical Operator4' */
  boolean_T LogicalOperator3;          /* '<Root>/Logical Operator3' */
} B_android_controler_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  codertarget_internal_androidA_T obj; /* '<S1>/Accelerometer' */
  codertarget_internal_androidB_T obj_a;/* '<S12>/Button' */
  codertarget_internal_androidB_T obj_l;/* '<S11>/Button' */
  codertarget_internal_androidB_T obj_g;/* '<S10>/Button' */
  codertarget_internal_androidB_T obj_d;/* '<S9>/Button' */
  codertarget_internal_androidB_T obj_dk;/* '<S8>/Button' */
  void *Button_PWORK;                  /* '<S12>/Button' */
  void *Button_PWORK_j;                /* '<S11>/Button' */
  void *Button_PWORK_k;                /* '<S10>/Button' */
  void *Button_PWORK_n;                /* '<S9>/Button' */
  void *Button_PWORK_f;                /* '<S8>/Button' */
  void *Accelerometer_PWORK;           /* '<S1>/Accelerometer' */
} DW_android_controler_T;

/* Parameters (auto storage) */
struct P_android_controler_T_ {
  real32_T CompareToConstant_const;    /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S2>/Constant'
                                        */
  real32_T CompareToConstant2_const;   /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S4>/Constant'
                                        */
  real32_T CompareToConstant5_const;   /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S7>/Constant'
                                        */
  real32_T CompareToConstant4_const;   /* Mask Parameter: CompareToConstant4_const
                                        * Referenced by: '<S6>/Constant'
                                        */
  real32_T CompareToConstant3_const;   /* Mask Parameter: CompareToConstant3_const
                                        * Referenced by: '<S5>/Constant'
                                        */
  real_T Accelerometer_SampleTime;     /* Expression: SampleTime
                                        * Referenced by: '<S1>/Accelerometer'
                                        */
  real_T Button_SampleTime;            /* Expression: SampleTime
                                        * Referenced by: '<S8>/Button'
                                        */
  real_T Button_SampleTime_n;          /* Expression: SampleTime
                                        * Referenced by: '<S9>/Button'
                                        */
  real_T Button_SampleTime_k;          /* Expression: SampleTime
                                        * Referenced by: '<S10>/Button'
                                        */
  real_T Button_SampleTime_c;          /* Expression: SampleTime
                                        * Referenced by: '<S11>/Button'
                                        */
  real_T Button_SampleTime_h;          /* Expression: SampleTime
                                        * Referenced by: '<S12>/Button'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_android_controler_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_android_controler_T android_controler_P;

/* Block signals (auto storage) */
extern B_android_controler_T android_controler_B;

/* Block states (auto storage) */
extern DW_android_controler_T android_controler_DW;

/* Model entry point functions */
extern void android_controler_initialize(void);
extern void android_controler_step(void);
extern void android_controler_terminate(void);

/* Real-time Model object */
extern RT_MODEL_android_controler_T *const android_controler_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S3>/Compare' : Unused code path elimination
 * Block '<S3>/Constant' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'android_controler'
 * '<S1>'   : 'android_controler/Accelerometer'
 * '<S2>'   : 'android_controler/Compare To Constant'
 * '<S3>'   : 'android_controler/Compare To Constant1'
 * '<S4>'   : 'android_controler/Compare To Constant2'
 * '<S5>'   : 'android_controler/Compare To Constant3'
 * '<S6>'   : 'android_controler/Compare To Constant4'
 * '<S7>'   : 'android_controler/Compare To Constant5'
 * '<S8>'   : 'android_controler/DOWN '
 * '<S9>'   : 'android_controler/LEFT '
 * '<S10>'  : 'android_controler/RIGHT '
 * '<S11>'  : 'android_controler/SELECTOR '
 * '<S12>'  : 'android_controler/UP '
 */
#endif                                 /* RTW_HEADER_android_controler_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
