/*
 * File: android_controler_data.c
 *
 * Code generated for Simulink model 'android_controler'.
 *
 * Model version                  : 1.10
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Sat Dec 16 14:29:59 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "android_controler.h"
#include "android_controler_private.h"

/* Block parameters (auto storage) */
P_android_controler_T android_controler_P = {
  8.0F,                                /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S2>/Constant'
                                        */
  8.0F,                                /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S4>/Constant'
                                        */
  -8.0F,                               /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S7>/Constant'
                                        */
  -8.0F,                               /* Mask Parameter: CompareToConstant4_const
                                        * Referenced by: '<S6>/Constant'
                                        */
  -8.0F,                               /* Mask Parameter: CompareToConstant3_const
                                        * Referenced by: '<S5>/Constant'
                                        */
  0.1,                                 /* Expression: SampleTime
                                        * Referenced by: '<S1>/Accelerometer'
                                        */
  0.1,                                 /* Expression: SampleTime
                                        * Referenced by: '<S8>/Button'
                                        */
  0.1,                                 /* Expression: SampleTime
                                        * Referenced by: '<S9>/Button'
                                        */
  0.1,                                 /* Expression: SampleTime
                                        * Referenced by: '<S10>/Button'
                                        */
  0.1,                                 /* Expression: SampleTime
                                        * Referenced by: '<S11>/Button'
                                        */
  0.1                                  /* Expression: SampleTime
                                        * Referenced by: '<S12>/Button'
                                        */
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
