/*
 * android_controler_dt.h
 *
 * Code generation for model "android_controler".
 *
 * Model version              : 1.10
 * Simulink Coder version : 8.12 (R2017a) 16-Feb-2017
 * C source code generated on : Sat Dec 16 14:29:59 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(codertarget_internal_androidA_T),
  sizeof(codertarget_internal_androidB_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "codertarget_internal_androidA_T",
  "codertarget_internal_androidB_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&android_controler_B.Accelerometer_o1), 1, 0, 3 },

  { (char_T *)(&android_controler_B.LogicalOperator1), 8, 0, 5 }
  ,

  { (char_T *)(&android_controler_DW.obj), 14, 0, 1 },

  { (char_T *)(&android_controler_DW.obj_a), 15, 0, 5 },

  { (char_T *)(&android_controler_DW.Button_PWORK), 11, 0, 6 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  5U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&android_controler_P.CompareToConstant_const), 1, 0, 5 },

  { (char_T *)(&android_controler_P.Accelerometer_SampleTime), 0, 0, 6 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  2U,
  rtPTransitions
};

/* [EOF] android_controler_dt.h */
