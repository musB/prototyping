function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/DOWNdata */
	this.urlHashMap["android_controler:49"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:49";
	/* <Root>/Display1 */
	this.urlHashMap["android_controler:8"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:8";
	/* <Root>/Display3 */
	this.urlHashMap["android_controler:10"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:10";
	/* <Root>/Display7 */
	this.urlHashMap["android_controler:38"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:38";
	/* <Root>/LEFTdata */
	this.urlHashMap["android_controler:51"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:51";
	/* <Root>/Logical
Operator */
	this.urlHashMap["android_controler:55"] = "android_controler.c:124&android_controler.h:80";
	/* <Root>/Logical
Operator1 */
	this.urlHashMap["android_controler:56"] = "android_controler.c:92&android_controler.h:79";
	/* <Root>/Logical
Operator2 */
	this.urlHashMap["android_controler:57"] = "android_controler.c:157&android_controler.h:81";
	/* <Root>/Logical
Operator3 */
	this.urlHashMap["android_controler:58"] = "android_controler.c:221&android_controler.h:83";
	/* <Root>/Logical
Operator4 */
	this.urlHashMap["android_controler:59"] = "android_controler.c:189&android_controler.h:82";
	/* <Root>/Rightdata */
	this.urlHashMap["android_controler:50"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:50";
	/* <Root>/Selectoredata */
	this.urlHashMap["android_controler:52"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:52";
	/* <Root>/UPdata */
	this.urlHashMap["android_controler:45"] = "msg=rtwMsg_CodeGenerationReducedBlock&block=android_controler:45";
	/* <S1>/Accelerometer */
	this.urlHashMap["android_controler:1:276"] = "android_controler.c:37,38,61,62,323,368,369,375&android_controler.h:76,77,78,88,99,120&android_controler_data.c:37";
	/* <S2>/Compare */
	this.urlHashMap["android_controler:39:2"] = "android_controler.c:95";
	/* <S2>/Constant */
	this.urlHashMap["android_controler:39:3"] = "android_controler.c:93&android_controler.h:105&android_controler_data.c:22";
	/* <S3>/Compare */
	this.urlHashMap["android_controler:40:2"] = "msg=rtwMsg_reducedBlock&block=android_controler:40:2";
	/* <S3>/Constant */
	this.urlHashMap["android_controler:40:3"] = "msg=rtwMsg_reducedBlock&block=android_controler:40:3";
	/* <S4>/Compare */
	this.urlHashMap["android_controler:41:2"] = "android_controler.c:127";
	/* <S4>/Constant */
	this.urlHashMap["android_controler:41:3"] = "android_controler.c:125&android_controler.h:108&android_controler_data.c:25";
	/* <S5>/Compare */
	this.urlHashMap["android_controler:42:2"] = "android_controler.c:224";
	/* <S5>/Constant */
	this.urlHashMap["android_controler:42:3"] = "android_controler.c:222&android_controler.h:117&android_controler_data.c:34";
	/* <S6>/Compare */
	this.urlHashMap["android_controler:43:2"] = "android_controler.c:192";
	/* <S6>/Constant */
	this.urlHashMap["android_controler:43:3"] = "android_controler.c:190&android_controler.h:114&android_controler_data.c:31";
	/* <S7>/Compare */
	this.urlHashMap["android_controler:44:2"] = "android_controler.c:160";
	/* <S7>/Constant */
	this.urlHashMap["android_controler:44:3"] = "android_controler.c:158&android_controler.h:111&android_controler_data.c:28";
	/* <S8>/Button */
	this.urlHashMap["android_controler:20:369"] = "android_controler.c:68,69,87,88,94,330,377,378,384&android_controler.h:93,98,123&android_controler_data.c:40";
	/* <S9>/Button */
	this.urlHashMap["android_controler:5:369"] = "android_controler.c:100,101,119,120,126,336,386,387,393&android_controler.h:92,97,126&android_controler_data.c:43";
	/* <S10>/Button */
	this.urlHashMap["android_controler:19:369"] = "android_controler.c:133,134,152,153,159,343,395,396,402&android_controler.h:91,96,129&android_controler_data.c:46";
	/* <S11>/Button */
	this.urlHashMap["android_controler:18:369"] = "android_controler.c:165,166,184,185,191,350,404,405,411&android_controler.h:90,95,132&android_controler_data.c:49";
	/* <S12>/Button */
	this.urlHashMap["android_controler:15:369"] = "android_controler.c:197,198,216,217,223,357,413,414,420&android_controler.h:89,94,135&android_controler_data.c:52";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "android_controler"};
	this.sidHashMap["android_controler"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "android_controler:1"};
	this.sidHashMap["android_controler:1"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "android_controler:39"};
	this.sidHashMap["android_controler:39"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "android_controler:40"};
	this.sidHashMap["android_controler:40"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "android_controler:41"};
	this.sidHashMap["android_controler:41"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "android_controler:42"};
	this.sidHashMap["android_controler:42"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "android_controler:43"};
	this.sidHashMap["android_controler:43"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "android_controler:44"};
	this.sidHashMap["android_controler:44"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<S8>"] = {sid: "android_controler:20"};
	this.sidHashMap["android_controler:20"] = {rtwname: "<S8>"};
	this.rtwnameHashMap["<S9>"] = {sid: "android_controler:5"};
	this.sidHashMap["android_controler:5"] = {rtwname: "<S9>"};
	this.rtwnameHashMap["<S10>"] = {sid: "android_controler:19"};
	this.sidHashMap["android_controler:19"] = {rtwname: "<S10>"};
	this.rtwnameHashMap["<S11>"] = {sid: "android_controler:18"};
	this.sidHashMap["android_controler:18"] = {rtwname: "<S11>"};
	this.rtwnameHashMap["<S12>"] = {sid: "android_controler:15"};
	this.sidHashMap["android_controler:15"] = {rtwname: "<S12>"};
	this.rtwnameHashMap["<Root>/Accelerometer"] = {sid: "android_controler:1"};
	this.sidHashMap["android_controler:1"] = {rtwname: "<Root>/Accelerometer"};
	this.rtwnameHashMap["<Root>/Compare To Constant"] = {sid: "android_controler:39"};
	this.sidHashMap["android_controler:39"] = {rtwname: "<Root>/Compare To Constant"};
	this.rtwnameHashMap["<Root>/Compare To Constant1"] = {sid: "android_controler:40"};
	this.sidHashMap["android_controler:40"] = {rtwname: "<Root>/Compare To Constant1"};
	this.rtwnameHashMap["<Root>/Compare To Constant2"] = {sid: "android_controler:41"};
	this.sidHashMap["android_controler:41"] = {rtwname: "<Root>/Compare To Constant2"};
	this.rtwnameHashMap["<Root>/Compare To Constant3"] = {sid: "android_controler:42"};
	this.sidHashMap["android_controler:42"] = {rtwname: "<Root>/Compare To Constant3"};
	this.rtwnameHashMap["<Root>/Compare To Constant4"] = {sid: "android_controler:43"};
	this.sidHashMap["android_controler:43"] = {rtwname: "<Root>/Compare To Constant4"};
	this.rtwnameHashMap["<Root>/Compare To Constant5"] = {sid: "android_controler:44"};
	this.sidHashMap["android_controler:44"] = {rtwname: "<Root>/Compare To Constant5"};
	this.rtwnameHashMap["<Root>/DOWN "] = {sid: "android_controler:20"};
	this.sidHashMap["android_controler:20"] = {rtwname: "<Root>/DOWN "};
	this.rtwnameHashMap["<Root>/DOWNdata"] = {sid: "android_controler:49"};
	this.sidHashMap["android_controler:49"] = {rtwname: "<Root>/DOWNdata"};
	this.rtwnameHashMap["<Root>/Display1"] = {sid: "android_controler:8"};
	this.sidHashMap["android_controler:8"] = {rtwname: "<Root>/Display1"};
	this.rtwnameHashMap["<Root>/Display3"] = {sid: "android_controler:10"};
	this.sidHashMap["android_controler:10"] = {rtwname: "<Root>/Display3"};
	this.rtwnameHashMap["<Root>/Display7"] = {sid: "android_controler:38"};
	this.sidHashMap["android_controler:38"] = {rtwname: "<Root>/Display7"};
	this.rtwnameHashMap["<Root>/LEFT "] = {sid: "android_controler:5"};
	this.sidHashMap["android_controler:5"] = {rtwname: "<Root>/LEFT "};
	this.rtwnameHashMap["<Root>/LEFTdata"] = {sid: "android_controler:51"};
	this.sidHashMap["android_controler:51"] = {rtwname: "<Root>/LEFTdata"};
	this.rtwnameHashMap["<Root>/Logical Operator"] = {sid: "android_controler:55"};
	this.sidHashMap["android_controler:55"] = {rtwname: "<Root>/Logical Operator"};
	this.rtwnameHashMap["<Root>/Logical Operator1"] = {sid: "android_controler:56"};
	this.sidHashMap["android_controler:56"] = {rtwname: "<Root>/Logical Operator1"};
	this.rtwnameHashMap["<Root>/Logical Operator2"] = {sid: "android_controler:57"};
	this.sidHashMap["android_controler:57"] = {rtwname: "<Root>/Logical Operator2"};
	this.rtwnameHashMap["<Root>/Logical Operator3"] = {sid: "android_controler:58"};
	this.sidHashMap["android_controler:58"] = {rtwname: "<Root>/Logical Operator3"};
	this.rtwnameHashMap["<Root>/Logical Operator4"] = {sid: "android_controler:59"};
	this.sidHashMap["android_controler:59"] = {rtwname: "<Root>/Logical Operator4"};
	this.rtwnameHashMap["<Root>/RIGHT "] = {sid: "android_controler:19"};
	this.sidHashMap["android_controler:19"] = {rtwname: "<Root>/RIGHT "};
	this.rtwnameHashMap["<Root>/Rightdata"] = {sid: "android_controler:50"};
	this.sidHashMap["android_controler:50"] = {rtwname: "<Root>/Rightdata"};
	this.rtwnameHashMap["<Root>/SELECTOR "] = {sid: "android_controler:18"};
	this.sidHashMap["android_controler:18"] = {rtwname: "<Root>/SELECTOR "};
	this.rtwnameHashMap["<Root>/Selectoredata"] = {sid: "android_controler:52"};
	this.sidHashMap["android_controler:52"] = {rtwname: "<Root>/Selectoredata"};
	this.rtwnameHashMap["<Root>/UP "] = {sid: "android_controler:15"};
	this.sidHashMap["android_controler:15"] = {rtwname: "<Root>/UP "};
	this.rtwnameHashMap["<Root>/UPdata"] = {sid: "android_controler:45"};
	this.sidHashMap["android_controler:45"] = {rtwname: "<Root>/UPdata"};
	this.rtwnameHashMap["<S1>/Accelerometer"] = {sid: "android_controler:1:276"};
	this.sidHashMap["android_controler:1:276"] = {rtwname: "<S1>/Accelerometer"};
	this.rtwnameHashMap["<S1>/Out1"] = {sid: "android_controler:1:279"};
	this.sidHashMap["android_controler:1:279"] = {rtwname: "<S1>/Out1"};
	this.rtwnameHashMap["<S1>/Out2"] = {sid: "android_controler:1:280"};
	this.sidHashMap["android_controler:1:280"] = {rtwname: "<S1>/Out2"};
	this.rtwnameHashMap["<S1>/Out3"] = {sid: "android_controler:1:281"};
	this.sidHashMap["android_controler:1:281"] = {rtwname: "<S1>/Out3"};
	this.rtwnameHashMap["<S2>/u"] = {sid: "android_controler:39:1"};
	this.sidHashMap["android_controler:39:1"] = {rtwname: "<S2>/u"};
	this.rtwnameHashMap["<S2>/Compare"] = {sid: "android_controler:39:2"};
	this.sidHashMap["android_controler:39:2"] = {rtwname: "<S2>/Compare"};
	this.rtwnameHashMap["<S2>/Constant"] = {sid: "android_controler:39:3"};
	this.sidHashMap["android_controler:39:3"] = {rtwname: "<S2>/Constant"};
	this.rtwnameHashMap["<S2>/y"] = {sid: "android_controler:39:4"};
	this.sidHashMap["android_controler:39:4"] = {rtwname: "<S2>/y"};
	this.rtwnameHashMap["<S3>/u"] = {sid: "android_controler:40:1"};
	this.sidHashMap["android_controler:40:1"] = {rtwname: "<S3>/u"};
	this.rtwnameHashMap["<S3>/Compare"] = {sid: "android_controler:40:2"};
	this.sidHashMap["android_controler:40:2"] = {rtwname: "<S3>/Compare"};
	this.rtwnameHashMap["<S3>/Constant"] = {sid: "android_controler:40:3"};
	this.sidHashMap["android_controler:40:3"] = {rtwname: "<S3>/Constant"};
	this.rtwnameHashMap["<S3>/y"] = {sid: "android_controler:40:4"};
	this.sidHashMap["android_controler:40:4"] = {rtwname: "<S3>/y"};
	this.rtwnameHashMap["<S4>/u"] = {sid: "android_controler:41:1"};
	this.sidHashMap["android_controler:41:1"] = {rtwname: "<S4>/u"};
	this.rtwnameHashMap["<S4>/Compare"] = {sid: "android_controler:41:2"};
	this.sidHashMap["android_controler:41:2"] = {rtwname: "<S4>/Compare"};
	this.rtwnameHashMap["<S4>/Constant"] = {sid: "android_controler:41:3"};
	this.sidHashMap["android_controler:41:3"] = {rtwname: "<S4>/Constant"};
	this.rtwnameHashMap["<S4>/y"] = {sid: "android_controler:41:4"};
	this.sidHashMap["android_controler:41:4"] = {rtwname: "<S4>/y"};
	this.rtwnameHashMap["<S5>/u"] = {sid: "android_controler:42:1"};
	this.sidHashMap["android_controler:42:1"] = {rtwname: "<S5>/u"};
	this.rtwnameHashMap["<S5>/Compare"] = {sid: "android_controler:42:2"};
	this.sidHashMap["android_controler:42:2"] = {rtwname: "<S5>/Compare"};
	this.rtwnameHashMap["<S5>/Constant"] = {sid: "android_controler:42:3"};
	this.sidHashMap["android_controler:42:3"] = {rtwname: "<S5>/Constant"};
	this.rtwnameHashMap["<S5>/y"] = {sid: "android_controler:42:4"};
	this.sidHashMap["android_controler:42:4"] = {rtwname: "<S5>/y"};
	this.rtwnameHashMap["<S6>/u"] = {sid: "android_controler:43:1"};
	this.sidHashMap["android_controler:43:1"] = {rtwname: "<S6>/u"};
	this.rtwnameHashMap["<S6>/Compare"] = {sid: "android_controler:43:2"};
	this.sidHashMap["android_controler:43:2"] = {rtwname: "<S6>/Compare"};
	this.rtwnameHashMap["<S6>/Constant"] = {sid: "android_controler:43:3"};
	this.sidHashMap["android_controler:43:3"] = {rtwname: "<S6>/Constant"};
	this.rtwnameHashMap["<S6>/y"] = {sid: "android_controler:43:4"};
	this.sidHashMap["android_controler:43:4"] = {rtwname: "<S6>/y"};
	this.rtwnameHashMap["<S7>/u"] = {sid: "android_controler:44:1"};
	this.sidHashMap["android_controler:44:1"] = {rtwname: "<S7>/u"};
	this.rtwnameHashMap["<S7>/Compare"] = {sid: "android_controler:44:2"};
	this.sidHashMap["android_controler:44:2"] = {rtwname: "<S7>/Compare"};
	this.rtwnameHashMap["<S7>/Constant"] = {sid: "android_controler:44:3"};
	this.sidHashMap["android_controler:44:3"] = {rtwname: "<S7>/Constant"};
	this.rtwnameHashMap["<S7>/y"] = {sid: "android_controler:44:4"};
	this.sidHashMap["android_controler:44:4"] = {rtwname: "<S7>/y"};
	this.rtwnameHashMap["<S8>/Button"] = {sid: "android_controler:20:369"};
	this.sidHashMap["android_controler:20:369"] = {rtwname: "<S8>/Button"};
	this.rtwnameHashMap["<S8>/Out1"] = {sid: "android_controler:20:370"};
	this.sidHashMap["android_controler:20:370"] = {rtwname: "<S8>/Out1"};
	this.rtwnameHashMap["<S9>/Button"] = {sid: "android_controler:5:369"};
	this.sidHashMap["android_controler:5:369"] = {rtwname: "<S9>/Button"};
	this.rtwnameHashMap["<S9>/Out1"] = {sid: "android_controler:5:370"};
	this.sidHashMap["android_controler:5:370"] = {rtwname: "<S9>/Out1"};
	this.rtwnameHashMap["<S10>/Button"] = {sid: "android_controler:19:369"};
	this.sidHashMap["android_controler:19:369"] = {rtwname: "<S10>/Button"};
	this.rtwnameHashMap["<S10>/Out1"] = {sid: "android_controler:19:370"};
	this.sidHashMap["android_controler:19:370"] = {rtwname: "<S10>/Out1"};
	this.rtwnameHashMap["<S11>/Button"] = {sid: "android_controler:18:369"};
	this.sidHashMap["android_controler:18:369"] = {rtwname: "<S11>/Button"};
	this.rtwnameHashMap["<S11>/Out1"] = {sid: "android_controler:18:370"};
	this.sidHashMap["android_controler:18:370"] = {rtwname: "<S11>/Out1"};
	this.rtwnameHashMap["<S12>/Button"] = {sid: "android_controler:15:369"};
	this.sidHashMap["android_controler:15:369"] = {rtwname: "<S12>/Button"};
	this.rtwnameHashMap["<S12>/Out1"] = {sid: "android_controler:15:370"};
	this.sidHashMap["android_controler:15:370"] = {rtwname: "<S12>/Out1"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
