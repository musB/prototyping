/*
 * driver_android_button.c
 *
 * Driver code
 *
 * Copyright 2013-2016 The MathWorks, Inc.
 */
#include <jni.h>
#include <stdlib.h>

extern JavaVM *cachedJvm;
extern jobject cachedActivityObj;
extern jclass cachedMainActivityCls;
static jmethodID sgRegButtonID, sgGetButtonID;

void initButton(int id)
{
    JNIEnv *pEnv;
    (*cachedJvm)->AttachCurrentThread(cachedJvm, &pEnv, NULL);
    
    sgRegButtonID = (*pEnv)->GetMethodID(pEnv, cachedMainActivityCls, "registerButtonFcn","(I)V");
    if (sgRegButtonID == NULL)
        return; /* registerButtonFcn method not found */
    
    sgGetButtonID = (*pEnv)->GetMethodID(pEnv, cachedMainActivityCls, "getButtonState","(I)F");    
    if (sgGetButtonID == NULL)
        return; /* getButtonState method not found */
    
    (*pEnv)->CallVoidMethod(pEnv, cachedActivityObj, sgRegButtonID,id);       
}


float getButtonState(int id)
{
    if (sgGetButtonID != NULL)
    {
        JNIEnv *pEnv;
        jfloat value;        
        
        (*cachedJvm)->AttachCurrentThread(cachedJvm, &pEnv, NULL);
        value = (jfloat)(*pEnv)->CallFloatMethod(pEnv, cachedActivityObj, sgGetButtonID,id);       
        if ((*pEnv)->ExceptionCheck(pEnv)) return 0;
        
        return value;
    }
}

void terminateButton()
{
}