/*
 * File: android_controler_types.h
 *
 * Code generated for Simulink model 'android_controler'.
 *
 * Model version                  : 1.10
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Sat Dec 16 14:29:59 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_android_controler_types_h_
#define RTW_HEADER_android_controler_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#ifndef typedef_codertarget_internal_androidA_T
#define typedef_codertarget_internal_androidA_T

typedef struct {
  int32_T isInitialized;
  real_T SampleTime;
} codertarget_internal_androidA_T;

#endif                                 /*typedef_codertarget_internal_androidA_T*/

#ifndef typedef_codertarget_internal_androidB_T
#define typedef_codertarget_internal_androidB_T

typedef struct {
  int32_T isInitialized;
  real_T SampleTime;
} codertarget_internal_androidB_T;

#endif                                 /*typedef_codertarget_internal_androidB_T*/

#ifndef typedef_struct_T_android_controler_T
#define typedef_struct_T_android_controler_T

typedef struct {
  char_T f1[7];
} struct_T_android_controler_T;

#endif                                 /*typedef_struct_T_android_controler_T*/

#ifndef typedef_struct_T_android_controler_p_T
#define typedef_struct_T_android_controler_p_T

typedef struct {
  char_T f1[6];
  char_T f2[6];
} struct_T_android_controler_p_T;

#endif                                 /*typedef_struct_T_android_controler_p_T*/

/* Parameters (auto storage) */
typedef struct P_android_controler_T_ P_android_controler_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_android_controler_T RT_MODEL_android_controler_T;

#endif                                 /* RTW_HEADER_android_controler_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
