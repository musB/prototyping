package com.example.androidraspberrypigettingstarted;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.example.androidraspberrypigettingstarted.androidraspberrypigettingstartedTest \
 * com.example.androidraspberrypigettingstarted.tests/android.test.InstrumentationTestRunner
 */
public class androidraspberrypigettingstartedTest extends ActivityInstrumentationTestCase2<androidraspberrypigettingstarted> {

    public androidraspberrypigettingstartedTest() {
        super("com.example.androidraspberrypigettingstarted", androidraspberrypigettingstarted.class);
    }

}
