#ifndef __c2_globalfunctionforBigrobotBisTestAudio_h__
#define __c2_globalfunctionforBigrobotBisTestAudio_h__

/* Include files */
#include "sf_runtime/sfc_sf.h"
#include "sf_runtime/sfc_mex.h"
#include "rtwtypes.h"
#include "multiword_types.h"

/* Type Definitions */
#ifndef typedef_SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
#define typedef_SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct

typedef struct {
  SimStruct *S;
  ChartInfoStruct chartInfo;
  uint32_T chartNumber;
  uint32_T instanceNumber;
  boolean_T c2_dataWrittenToVector[13];
  uint8_T c2_doSetSimStateSideEffects;
  const mxArray *c2_setSimStateSideEffectsInfo;
  void *c2_fEmlrtCtx;
  int32_T *c2_sfEvent;
  uint8_T *c2_is_active_c2_globalfunctionforBigrobotBisTestAudio;
  uint8_T *c2_is_c2_globalfunctionforBigrobotBisTestAudio;
  uint8_T *c2_is_LINE_FOLLOWER_IN;
  uint8_T *c2_is_GeometricalShape;
  uint8_T *c2_is_Triangle1;
  uint8_T *c2_is_StraightLine;
  uint8_T *c2_is_Square;
  uint8_T *c2_is_Circle;
  uint8_T *c2_is_ObstacleAvoidance;
  uint8_T *c2_is_Joystick;
  real_T *c2_direction;
  real_T *c2_side;
  real_T *c2_C;
  real_T *c2_d;
  real_T *c2_directionD;
  real_T *c2_Up;
  real_T *c2_Right;
  real_T *c2_Down;
  real_T *c2_Left;
  real_T *c2_Middle;
  real_T *c2_speed;
  real_T *c2_speed2;
  real_T *c2_Stop;
  real_T *c2_Q;
  real_T *c2_P;
  real_T *c2_reset;
  boolean_T *c2_LeftSensor;
  boolean_T *c2_RightSensor;
  real_T *c2_Sel1;
  real_T *c2_Sel2;
  real_T *c2_Sel0;
  boolean_T *c2_NE;
  boolean_T *c2_E;
  boolean_T *c2_NO;
  boolean_T *c2_O;
  boolean_T *c2_N;
  real_T *c2_sound;
  real_T *c2_counter;
  uint8_T *c2_temporalCounter_i1;
} SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct;

#endif                                 /*typedef_SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*/

/* Named Constants */

/* Variable Declarations */
extern struct SfDebugInstanceStruct *sfGlobalDebugInstanceStruct;

/* Variable Definitions */

/* Function Declarations */
extern const mxArray
  *sf_c2_globalfunctionforBigrobotBisTestAudio_get_eml_resolved_functions_info
  (void);

/* Function Definitions */
extern void sf_c2_globalfunctionforBigrobotBisTestAudio_get_check_sum(mxArray
  *plhs[]);
extern void c2_globalfunctionforBigrobotBisTestAudio_method_dispatcher(SimStruct
  *S, int_T method, void *data);

#endif
