/* Include files */

#include "globalfunctionforBigrobotBisTestAudio_sfun.h"
#include "c2_globalfunctionforBigrobotBisTestAudio.h"
#define CHARTINSTANCE_CHARTNUMBER      (chartInstance->chartNumber)
#define CHARTINSTANCE_INSTANCENUMBER   (chartInstance->instanceNumber)
#include "globalfunctionforBigrobotBisTestAudio_sfun_debug_macros.h"
#define _SF_MEX_LISTEN_FOR_CTRL_C(S)   sf_mex_listen_for_ctrl_c_with_debugger(S, sfGlobalDebugInstanceStruct);

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization);
static void chart_debug_initialize_data_addresses(SimStruct *S);
static const mxArray* sf_opaque_get_hover_data_for_msg(void *chartInstance,
  int32_T msgSSID);

/* Type Definitions */

/* Named Constants */
#define CALL_EVENT                     (-1)
#define c2_IN_NO_ACTIVE_CHILD          ((uint8_T)0U)
#define c2_IN_GeometricalShape         ((uint8_T)1U)
#define c2_IN_Joystick                 ((uint8_T)2U)
#define c2_IN_LINE_FOLLOWER_IN         ((uint8_T)3U)
#define c2_IN_ObstacleAvoidance        ((uint8_T)4U)
#define c2_IN_Selecter                 ((uint8_T)5U)
#define c2_IN_Tempo                    ((uint8_T)6U)
#define c2_IN_StraightForward          ((uint8_T)1U)
#define c2_IN_TourneDroite             ((uint8_T)2U)
#define c2_IN_TourneGauche             ((uint8_T)3U)
#define c2_IN_Circle                   ((uint8_T)1U)
#define c2_IN_Square                   ((uint8_T)2U)
#define c2_IN_Start                    ((uint8_T)3U)
#define c2_IN_StraightLine             ((uint8_T)4U)
#define c2_IN_Triangle1                ((uint8_T)5U)
#define c2_IN_Straight                 ((uint8_T)1U)
#define c2_IN_Straight1                ((uint8_T)2U)
#define c2_IN_Straight2                ((uint8_T)3U)
#define c2_IN_Turn                     ((uint8_T)4U)
#define c2_IN_Turn1                    ((uint8_T)5U)
#define c2_IN_off                      ((uint8_T)6U)
#define c2_IN_Off                      ((uint8_T)1U)
#define c2_b_IN_Straight               ((uint8_T)2U)
#define c2_IN_Straight3                ((uint8_T)4U)
#define c2_b_IN_Turn                   ((uint8_T)5U)
#define c2_b_IN_Turn1                  ((uint8_T)6U)
#define c2_IN_Turn2                    ((uint8_T)7U)
#define c2_b_IN_off                    ((uint8_T)8U)
#define c2_IN_CENTER                   ((uint8_T)1U)
#define c2_IN_LEFTMOST                 ((uint8_T)2U)
#define c2_IN_RIGHT                    ((uint8_T)3U)
#define c2_IN_RIGHTMOST                ((uint8_T)4U)
#define c2_IN_Stop                     ((uint8_T)5U)
#define c2_IN_DOWN                     ((uint8_T)1U)
#define c2_IN_LEFT                     ((uint8_T)2U)
#define c2_IN_OFF                      ((uint8_T)3U)
#define c2_b_IN_RIGHT                  ((uint8_T)4U)
#define c2_IN_UP                       ((uint8_T)5U)

/* Variable Declarations */

/* Variable Definitions */
static real_T _sfTime_;
static const char * c2_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_b_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_c_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_d_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_e_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_f_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_g_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_h_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_i_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_j_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_k_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_l_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_m_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_n_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_o_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_p_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_q_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_r_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_s_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_t_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_u_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_v_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_w_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_x_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_y_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_ab_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_bb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_cb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_db_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_eb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_fb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_gb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_hb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_ib_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_jb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_kb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_lb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_mb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_nb_debug_family_names[2] = { "nargin", "nargout" };

static const char * c2_ob_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_pb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_qb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_rb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_sb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_tb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ub_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_vb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_wb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_xb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_yb_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ac_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_bc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_cc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_dc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ec_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_fc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_gc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_hc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ic_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_jc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_kc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_lc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_mc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_nc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_oc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_pc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_qc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_rc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_sc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_tc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_uc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_vc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_wc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_xc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_yc_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ad_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_bd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_cd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_dd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ed_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_fd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_gd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_hd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_id_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_jd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_kd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ld_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_md_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_nd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_od_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_pd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_qd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_rd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_sd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_td_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ud_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_vd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_wd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_xd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_yd_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ae_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_be_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ce_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_de_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

static const char * c2_ee_debug_family_names[3] = { "nargin", "nargout",
  "sf_internal_predicateOutput" };

/* Function Declarations */
static void initialize_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void initialize_params_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void enable_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void disable_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_update_debugger_state_c2_globalfunctionforBigrobotBisTestAudi
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void ext_mode_exec_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static const mxArray *get_sim_state_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void set_sim_state_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_st);
static void c2_set_sim_state_side_effects_c2_globalfunctionforBigrobotBisTes
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void finalize_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void sf_gateway_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void mdl_start_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_chartstep_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void initSimStructsc2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_LINE_FOLLOWER_IN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_exit_internal_LINE_FOLLOWER_IN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_StraightForward
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_TourneDroite
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_TourneGauche
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_GeometricalShape
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_exit_internal_GeometricalShape
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_internal_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_exit_internal_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Turn(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                    *chartInstance);
static void c2_Turn1(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance);
static void c2_enter_atomic_StraightLine
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_StraightLine
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_Square
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Square(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                      *chartInstance);
static void c2_exit_internal_Square
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_b_Turn(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                      *chartInstance);
static void c2_Straight3
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Turn2(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance);
static void c2_b_Turn1(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *
  chartInstance);
static void c2_enter_atomic_Start
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Start(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance);
static void c2_enter_atomic_Circle
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_ObstacleAvoidance
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_exit_internal_ObstacleAvoidance
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_Stop
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_RIGHT(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance);
static void c2_enter_atomic_CENTER
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_LEFTMOST
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_Selecter
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_Joystick(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
  *chartInstance);
static void c2_exit_internal_Joystick
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_UP
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_UP(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                  *chartInstance);
static void c2_enter_atomic_LEFT
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_enter_atomic_RIGHT
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void c2_b_RIGHT(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *
  chartInstance);
static void c2_enter_atomic_DOWN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber);
static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData);
static real_T c2_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_nargout, const char_T *c2_identifier);
static real_T c2_b_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static boolean_T c2_c_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static int32_T c2_d_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData);
static uint8_T c2_e_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_is_LINE_FOLLOWER_IN, const char_T *c2_identifier);
static uint8_T c2_f_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData);
static void c2_g_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_dataWrittenToVector, const char_T *c2_identifier,
   boolean_T c2_y[13]);
static void c2_h_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId, boolean_T c2_y[13]);
static const mxArray *c2_i_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_setSimStateSideEffectsInfo, const char_T *c2_identifier);
static const mxArray *c2_j_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId);
static const mxArray *sf_get_hover_data_for_msg
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   int32_T c2_ssid);
static void c2_init_sf_message_store_memory
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void init_dsm_address_info
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);
static void init_simulink_io_address
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance);

/* Function Definitions */
static void initialize_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  if (sf_is_first_init_cond(chartInstance->S)) {
    initSimStructsc2_globalfunctionforBigrobotBisTestAudio(chartInstance);
    chart_debug_initialize_data_addresses(chartInstance->S);
  }

  *chartInstance->c2_sfEvent = CALL_EVENT;
  _sfTime_ = sf_get_time(chartInstance->S);
  chartInstance->c2_doSetSimStateSideEffects = 0U;
  chartInstance->c2_setSimStateSideEffectsInfo = NULL;
  *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_temporalCounter_i1 = 0U;
  *chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio = 0U;
  *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
    c2_IN_NO_ACTIVE_CHILD;
}

static void initialize_params_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void enable_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void disable_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  _sfTime_ = sf_get_time(chartInstance->S);
}

static void c2_update_debugger_state_c2_globalfunctionforBigrobotBisTestAudi
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_prevAniVal;
  c2_prevAniVal = _SFD_GET_ANIMATION();
  _SFD_SET_ANIMATION(0U);
  _SFD_SET_HONOR_BREAKPOINTS(0U);
  if (*chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio ==
      1U) {
    _SFD_CC_CALL(CHART_ACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_LINE_FOLLOWER_IN) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 30U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 30U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_LINE_FOLLOWER_IN == c2_IN_StraightForward) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_LINE_FOLLOWER_IN == c2_IN_TourneDroite) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_LINE_FOLLOWER_IN == c2_IN_TourneGauche) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_GeometricalShape) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_GeometricalShape == c2_IN_Triangle1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_Straight) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_Turn) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_off) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 23U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 23U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_Straight1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 19U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 19U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_Turn1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Triangle1 == c2_IN_Straight2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_GeometricalShape == c2_IN_StraightLine) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_StraightLine == c2_b_IN_Straight) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_StraightLine == c2_IN_Off) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_GeometricalShape == c2_IN_Square) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_b_IN_off) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 12U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 12U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_IN_Straight) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_b_IN_Turn) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_IN_Straight3) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_IN_Straight1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_IN_Turn2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_b_IN_Turn1) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Square == c2_IN_Straight2) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_GeometricalShape == c2_IN_Start) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_GeometricalShape == c2_IN_Circle) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Circle == c2_b_IN_Straight) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Circle == c2_IN_Off) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_ObstacleAvoidance) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 34U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 34U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_ObstacleAvoidance == c2_IN_Stop) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_ObstacleAvoidance == c2_IN_RIGHT) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_ObstacleAvoidance == c2_IN_RIGHTMOST) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_ObstacleAvoidance == c2_IN_CENTER) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_ObstacleAvoidance == c2_IN_LEFTMOST) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_Selecter) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_Tempo) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 41U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 41U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
      c2_IN_Joystick) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 24U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 24U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Joystick == c2_IN_UP) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Joystick == c2_IN_LEFT) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Joystick == c2_b_IN_RIGHT) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Joystick == c2_IN_OFF) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
  }

  if (*chartInstance->c2_is_Joystick == c2_IN_DOWN) {
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
  } else {
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
  }

  _SFD_SET_ANIMATION(c2_prevAniVal);
  _SFD_SET_HONOR_BREAKPOINTS(1U);
  _SFD_ANIMATE();
}

static void ext_mode_exec_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  c2_update_debugger_state_c2_globalfunctionforBigrobotBisTestAudi(chartInstance);
}

static const mxArray *get_sim_state_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  const mxArray *c2_st;
  const mxArray *c2_y = NULL;
  real_T c2_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_h_y = NULL;
  real_T c2_h_hoistedGlobal;
  const mxArray *c2_i_y = NULL;
  real_T c2_i_hoistedGlobal;
  const mxArray *c2_j_y = NULL;
  real_T c2_j_hoistedGlobal;
  const mxArray *c2_k_y = NULL;
  real_T c2_k_hoistedGlobal;
  const mxArray *c2_l_y = NULL;
  real_T c2_l_hoistedGlobal;
  const mxArray *c2_m_y = NULL;
  real_T c2_m_hoistedGlobal;
  const mxArray *c2_n_y = NULL;
  uint8_T c2_n_hoistedGlobal;
  const mxArray *c2_o_y = NULL;
  uint8_T c2_o_hoistedGlobal;
  const mxArray *c2_p_y = NULL;
  uint8_T c2_p_hoistedGlobal;
  const mxArray *c2_q_y = NULL;
  uint8_T c2_q_hoistedGlobal;
  const mxArray *c2_r_y = NULL;
  uint8_T c2_r_hoistedGlobal;
  const mxArray *c2_s_y = NULL;
  uint8_T c2_s_hoistedGlobal;
  const mxArray *c2_t_y = NULL;
  uint8_T c2_t_hoistedGlobal;
  const mxArray *c2_u_y = NULL;
  uint8_T c2_u_hoistedGlobal;
  const mxArray *c2_v_y = NULL;
  uint8_T c2_v_hoistedGlobal;
  const mxArray *c2_w_y = NULL;
  uint8_T c2_w_hoistedGlobal;
  const mxArray *c2_x_y = NULL;
  uint8_T c2_x_hoistedGlobal;
  const mxArray *c2_y_y = NULL;
  const mxArray *c2_ab_y = NULL;
  c2_st = NULL;
  c2_st = NULL;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_createcellmatrix(25, 1), false);
  c2_hoistedGlobal = *chartInstance->c2_Sel0;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_setcell(c2_y, 0, c2_b_y);
  c2_b_hoistedGlobal = *chartInstance->c2_Sel1;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 1, c2_c_y);
  c2_c_hoistedGlobal = *chartInstance->c2_Sel2;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 2, c2_d_y);
  c2_d_hoistedGlobal = *chartInstance->c2_direction;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 3, c2_e_y);
  c2_e_hoistedGlobal = *chartInstance->c2_directionD;
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 4, c2_f_y);
  c2_f_hoistedGlobal = *chartInstance->c2_reset;
  c2_g_y = NULL;
  sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 5, c2_g_y);
  c2_g_hoistedGlobal = *chartInstance->c2_sound;
  c2_h_y = NULL;
  sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 6, c2_h_y);
  c2_h_hoistedGlobal = *chartInstance->c2_speed;
  c2_i_y = NULL;
  sf_mex_assign(&c2_i_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 7, c2_i_y);
  c2_i_hoistedGlobal = *chartInstance->c2_speed2;
  c2_j_y = NULL;
  sf_mex_assign(&c2_j_y, sf_mex_create("y", &c2_i_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 8, c2_j_y);
  c2_j_hoistedGlobal = *chartInstance->c2_P;
  c2_k_y = NULL;
  sf_mex_assign(&c2_k_y, sf_mex_create("y", &c2_j_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 9, c2_k_y);
  c2_k_hoistedGlobal = *chartInstance->c2_Q;
  c2_l_y = NULL;
  sf_mex_assign(&c2_l_y, sf_mex_create("y", &c2_k_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 10, c2_l_y);
  c2_l_hoistedGlobal = *chartInstance->c2_Stop;
  c2_m_y = NULL;
  sf_mex_assign(&c2_m_y, sf_mex_create("y", &c2_l_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 11, c2_m_y);
  c2_m_hoistedGlobal = *chartInstance->c2_counter;
  c2_n_y = NULL;
  sf_mex_assign(&c2_n_y, sf_mex_create("y", &c2_m_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 12, c2_n_y);
  c2_n_hoistedGlobal =
    *chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio;
  c2_o_y = NULL;
  sf_mex_assign(&c2_o_y, sf_mex_create("y", &c2_n_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 13, c2_o_y);
  c2_o_hoistedGlobal =
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio;
  c2_p_y = NULL;
  sf_mex_assign(&c2_p_y, sf_mex_create("y", &c2_o_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 14, c2_p_y);
  c2_p_hoistedGlobal = *chartInstance->c2_is_Square;
  c2_q_y = NULL;
  sf_mex_assign(&c2_q_y, sf_mex_create("y", &c2_p_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 15, c2_q_y);
  c2_q_hoistedGlobal = *chartInstance->c2_is_Triangle1;
  c2_r_y = NULL;
  sf_mex_assign(&c2_r_y, sf_mex_create("y", &c2_q_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 16, c2_r_y);
  c2_r_hoistedGlobal = *chartInstance->c2_is_StraightLine;
  c2_s_y = NULL;
  sf_mex_assign(&c2_s_y, sf_mex_create("y", &c2_r_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 17, c2_s_y);
  c2_s_hoistedGlobal = *chartInstance->c2_is_Circle;
  c2_t_y = NULL;
  sf_mex_assign(&c2_t_y, sf_mex_create("y", &c2_s_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 18, c2_t_y);
  c2_t_hoistedGlobal = *chartInstance->c2_is_GeometricalShape;
  c2_u_y = NULL;
  sf_mex_assign(&c2_u_y, sf_mex_create("y", &c2_t_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 19, c2_u_y);
  c2_u_hoistedGlobal = *chartInstance->c2_is_Joystick;
  c2_v_y = NULL;
  sf_mex_assign(&c2_v_y, sf_mex_create("y", &c2_u_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 20, c2_v_y);
  c2_v_hoistedGlobal = *chartInstance->c2_is_ObstacleAvoidance;
  c2_w_y = NULL;
  sf_mex_assign(&c2_w_y, sf_mex_create("y", &c2_v_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 21, c2_w_y);
  c2_w_hoistedGlobal = *chartInstance->c2_is_LINE_FOLLOWER_IN;
  c2_x_y = NULL;
  sf_mex_assign(&c2_x_y, sf_mex_create("y", &c2_w_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 22, c2_x_y);
  c2_x_hoistedGlobal = *chartInstance->c2_temporalCounter_i1;
  c2_y_y = NULL;
  sf_mex_assign(&c2_y_y, sf_mex_create("y", &c2_x_hoistedGlobal, 3, 0U, 0U, 0U,
    0), false);
  sf_mex_setcell(c2_y, 23, c2_y_y);
  c2_ab_y = NULL;
  sf_mex_assign(&c2_ab_y, sf_mex_create("y",
    chartInstance->c2_dataWrittenToVector, 11, 0U, 1U, 0U, 1, 13), false);
  sf_mex_setcell(c2_y, 24, c2_ab_y);
  sf_mex_assign(&c2_st, c2_y, false);
  return c2_st;
}

static void set_sim_state_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_st)
{
  const mxArray *c2_u;
  boolean_T c2_bv0[13];
  int32_T c2_i0;
  c2_u = sf_mex_dup(c2_st);
  *chartInstance->c2_Sel0 = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Sel0", c2_u, 0)), "Sel0");
  *chartInstance->c2_Sel1 = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Sel1", c2_u, 1)), "Sel1");
  *chartInstance->c2_Sel2 = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Sel2", c2_u, 2)), "Sel2");
  *chartInstance->c2_direction = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("direction", c2_u, 3)), "direction");
  *chartInstance->c2_directionD = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("directionD", c2_u, 4)), "directionD");
  *chartInstance->c2_reset = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("reset", c2_u, 5)), "reset");
  *chartInstance->c2_sound = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("sound", c2_u, 6)), "sound");
  *chartInstance->c2_speed = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("speed", c2_u, 7)), "speed");
  *chartInstance->c2_speed2 = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("speed2", c2_u, 8)), "speed2");
  *chartInstance->c2_P = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("P", c2_u, 9)), "P");
  *chartInstance->c2_Q = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Q", c2_u, 10)), "Q");
  *chartInstance->c2_Stop = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("Stop", c2_u, 11)), "Stop");
  *chartInstance->c2_counter = c2_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("counter", c2_u, 12)), "counter");
  *chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio =
    c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_active_c2_globalfunctionforBigrobotBisTestAudio", c2_u, 13)),
    "is_active_c2_globalfunctionforBigrobotBisTestAudio");
  *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
    c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "is_c2_globalfunctionforBigrobotBisTestAudio", c2_u, 14)),
    "is_c2_globalfunctionforBigrobotBisTestAudio");
  *chartInstance->c2_is_Square = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("is_Square", c2_u, 15)), "is_Square");
  *chartInstance->c2_is_Triangle1 = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_Triangle1", c2_u, 16)), "is_Triangle1");
  *chartInstance->c2_is_StraightLine = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_StraightLine", c2_u, 17)), "is_StraightLine");
  *chartInstance->c2_is_Circle = c2_e_emlrt_marshallIn(chartInstance, sf_mex_dup
    (sf_mex_getcell("is_Circle", c2_u, 18)), "is_Circle");
  *chartInstance->c2_is_GeometricalShape = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_GeometricalShape", c2_u, 19)),
    "is_GeometricalShape");
  *chartInstance->c2_is_Joystick = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_Joystick", c2_u, 20)), "is_Joystick");
  *chartInstance->c2_is_ObstacleAvoidance = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_ObstacleAvoidance", c2_u, 21)),
    "is_ObstacleAvoidance");
  *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("is_LINE_FOLLOWER_IN", c2_u, 22)),
    "is_LINE_FOLLOWER_IN");
  *chartInstance->c2_temporalCounter_i1 = c2_e_emlrt_marshallIn(chartInstance,
    sf_mex_dup(sf_mex_getcell("temporalCounter_i1", c2_u, 23)),
    "temporalCounter_i1");
  c2_g_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "dataWrittenToVector", c2_u, 24)), "dataWrittenToVector", c2_bv0);
  for (c2_i0 = 0; c2_i0 < 13; c2_i0++) {
    chartInstance->c2_dataWrittenToVector[c2_i0] = c2_bv0[c2_i0];
  }

  sf_mex_assign(&chartInstance->c2_setSimStateSideEffectsInfo,
                c2_i_emlrt_marshallIn(chartInstance, sf_mex_dup(sf_mex_getcell(
    "setSimStateSideEffectsInfo", c2_u, 25)), "setSimStateSideEffectsInfo"),
                true);
  sf_mex_destroy(&c2_u);
  chartInstance->c2_doSetSimStateSideEffects = 1U;
  c2_update_debugger_state_c2_globalfunctionforBigrobotBisTestAudi(chartInstance);
  sf_mex_destroy(&c2_st);
}

static void c2_set_sim_state_side_effects_c2_globalfunctionforBigrobotBisTes
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  if (chartInstance->c2_doSetSimStateSideEffects != 0) {
    if ((*chartInstance->c2_is_ObstacleAvoidance == c2_IN_CENTER) && (sf_mex_sub
         (chartInstance->c2_setSimStateSideEffectsInfo,
          "setSimStateSideEffectsInfo", 1, 37) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    if ((*chartInstance->c2_is_ObstacleAvoidance == c2_IN_LEFTMOST) &&
        (sf_mex_sub(chartInstance->c2_setSimStateSideEffectsInfo,
                    "setSimStateSideEffectsInfo", 1, 38) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    if ((*chartInstance->c2_is_ObstacleAvoidance == c2_IN_RIGHT) && (sf_mex_sub
         (chartInstance->c2_setSimStateSideEffectsInfo,
          "setSimStateSideEffectsInfo", 1, 39) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    if ((*chartInstance->c2_is_ObstacleAvoidance == c2_IN_RIGHTMOST) &&
        (sf_mex_sub(chartInstance->c2_setSimStateSideEffectsInfo,
                    "setSimStateSideEffectsInfo", 1, 40) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    if ((*chartInstance->c2_is_ObstacleAvoidance == c2_IN_Stop) && (sf_mex_sub
         (chartInstance->c2_setSimStateSideEffectsInfo,
          "setSimStateSideEffectsInfo", 1, 41) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    if ((*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio ==
         c2_IN_Tempo) && (sf_mex_sub
                          (chartInstance->c2_setSimStateSideEffectsInfo,
                           "setSimStateSideEffectsInfo", 1, 43) == 0.0)) {
      *chartInstance->c2_temporalCounter_i1 = 0U;
    }

    chartInstance->c2_doSetSimStateSideEffects = 0U;
  }
}

static void finalize_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  sf_mex_destroy(&chartInstance->c2_setSimStateSideEffectsInfo);
}

static void sf_gateway_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  c2_set_sim_state_side_effects_c2_globalfunctionforBigrobotBisTes(chartInstance);
  _SFD_SYMBOL_SCOPE_PUSH(0U, 0U);
  _sfTime_ = sf_get_time(chartInstance->S);
  if (*chartInstance->c2_temporalCounter_i1 < 15U) {
    (*chartInstance->c2_temporalCounter_i1)++;
  }

  _SFD_CC_CALL(CHART_ENTER_SFUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_N, 18U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_O, 17U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_NO, 16U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_E, 15U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_NE, 14U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_RightSensor, 13U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK((real_T)*chartInstance->c2_LeftSensor, 12U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Middle, 11U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Left, 10U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Down, 9U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Right, 8U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Up, 7U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_d, 6U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_C, 5U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_side, 4U, 1U, 0U,
                        *chartInstance->c2_sfEvent, false);
  *chartInstance->c2_sfEvent = CALL_EVENT;
  c2_chartstep_c2_globalfunctionforBigrobotBisTestAudio(chartInstance);
  _SFD_SYMBOL_SCOPE_POP();
  _SFD_CHECK_FOR_STATE_INCONSISTENCY
    (_globalfunctionforBigrobotBisTestAudioMachineNumber_,
     chartInstance->chartNumber, chartInstance->instanceNumber);
}

static void mdl_start_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  c2_init_sf_message_store_memory(chartInstance);
}

static void c2_chartstep_c2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_b_nargin = 0.0;
  real_T c2_nargout = 1.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_out;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_d_nargin = 0.0;
  real_T c2_d_nargout = 0.0;
  real_T c2_e_nargin = 0.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_d_out;
  real_T c2_f_nargin = 0.0;
  real_T c2_hoistedGlobal;
  real_T c2_f_nargout = 0.0;
  const mxArray *c2_y = NULL;
  real_T c2_g_nargin = 0.0;
  real_T c2_g_nargout = 1.0;
  boolean_T c2_e_out;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_h_nargin = 0.0;
  real_T c2_h_nargout = 0.0;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  _SFD_CC_CALL(CHART_ENTER_DURING_FUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
  if (*chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio ==
      0U) {
    _SFD_CC_CALL(CHART_ENTER_ENTRY_FUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio = 1U;
    _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
      c2_IN_Selecter;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Selecter(chartInstance);
  } else {
    switch (*chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio) {
     case c2_IN_GeometricalShape:
      CV_CHART_EVAL(1, 0, 1);
      c2_GeometricalShape(chartInstance);
      break;

     case c2_IN_Joystick:
      CV_CHART_EVAL(1, 0, 2);
      c2_Joystick(chartInstance);
      break;

     case c2_IN_LINE_FOLLOWER_IN:
      CV_CHART_EVAL(1, 0, 3);
      c2_LINE_FOLLOWER_IN(chartInstance);
      break;

     case c2_IN_ObstacleAvoidance:
      CV_CHART_EVAL(1, 0, 4);
      c2_ObstacleAvoidance(chartInstance);
      break;

     case c2_IN_Selecter:
      CV_CHART_EVAL(1, 0, 5);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 51U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ce_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_out = CV_EML_IF(51, 0, 0, CV_RELATIONAL_EVAL(5U, 51U, 0,
        *chartInstance->c2_Down, 1.0, -1, 0U, *chartInstance->c2_Down == 1.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 51U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
          c2_IN_Tempo;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 41U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_temporalCounter_i1 = 0U;
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 32U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_yd_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_c_out = CV_EML_IF(32, 0, 0, CV_RELATIONAL_EVAL(5U, 32U, 0,
          *chartInstance->c2_Right, 1.0, -1, 0U, *chartInstance->c2_Right == 1.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
            c2_IN_GeometricalShape;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_e_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_sound = 0.0;
          chartInstance->c2_dataWrittenToVector[11U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_sound, 27U, 4U, 0U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[11U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(27U, 4U, 0U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "sound");
          c2_b_hoistedGlobal = *chartInstance->c2_sound;
          c2_b_y = NULL;
          sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_b_y);
          _SFD_SYMBOL_SCOPE_POP();
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_GeometricalShape = c2_IN_Start;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_Start(chartInstance);
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 55U,
                       *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_be_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          c2_d_out = CV_EML_IF(55, 0, 0, CV_RELATIONAL_EVAL(5U, 55U, 0,
            *chartInstance->c2_Left, 1.0, -1, 0U, *chartInstance->c2_Left == 1.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c2_d_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 55U, *chartInstance->c2_sfEvent);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
              c2_IN_ObstacleAvoidance;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 34U, *chartInstance->c2_sfEvent);
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 74U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_is_ObstacleAvoidance = c2_IN_LEFTMOST;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_temporalCounter_i1 = 0U;
            c2_enter_atomic_LEFTMOST(chartInstance);
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 56U,
                         *chartInstance->c2_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_wd_debug_family_names,
              c2_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U,
              c2_b_sf_marshallOut, c2_b_sf_marshallIn);
            c2_e_out = CV_EML_IF(56, 0, 0, CV_RELATIONAL_EVAL(5U, 56U, 0,
              *chartInstance->c2_Up, 1.0, -1, 0U, *chartInstance->c2_Up == 1.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c2_e_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 56U,
                           *chartInstance->c2_sfEvent);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
              *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
                c2_IN_LINE_FOLLOWER_IN;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 30U, *chartInstance->c2_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_debug_family_names,
                c2_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U,
                c2_sf_marshallOut, c2_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
                c2_sf_marshallOut, c2_sf_marshallIn);
              *chartInstance->c2_sound = 1.0;
              chartInstance->c2_dataWrittenToVector[11U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c2_sound, 27U, 4U, 30U,
                                    *chartInstance->c2_sfEvent, false);
              if (!chartInstance->c2_dataWrittenToVector[11U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(27U, 4U, 30U,
                  *chartInstance->c2_sfEvent, false);
              }

              sf_mex_printf("%s =\\n", "sound");
              c2_c_hoistedGlobal = *chartInstance->c2_sound;
              c2_c_y = NULL;
              sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0,
                0U, 0U, 0U, 0), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                                c2_c_y);
              _SFD_SYMBOL_SCOPE_POP();
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 60U,
                           *chartInstance->c2_sfEvent);
              *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_StraightForward;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
              c2_enter_atomic_StraightForward(chartInstance);
            } else {
              _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 40U,
                           *chartInstance->c2_sfEvent);
            }
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 40U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_Tempo:
      CV_CHART_EVAL(1, 0, 6);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 53U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ee_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_b_out = CV_EML_IF(53, 0, 0, *chartInstance->c2_temporalCounter_i1 >= 3);
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 53U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 41U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
          c2_IN_Joystick;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 24U, *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_jb_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        *chartInstance->c2_sound = 3.0;
        chartInstance->c2_dataWrittenToVector[11U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_sound, 27U, 4U, 24U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[11U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(27U, 4U, 24U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "sound");
        c2_hoistedGlobal = *chartInstance->c2_sound;
        c2_y = NULL;
        sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U,
          0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
        _SFD_SYMBOL_SCOPE_POP();
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_IN_OFF;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 41U,
                     *chartInstance->c2_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 41U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_CHART_EVAL(1, 0, 0);

      /* Unreachable state, for coverage only */
      *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
        c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
      break;
    }
  }

  _SFD_CC_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
}

static void initSimStructsc2_globalfunctionforBigrobotBisTestAudio
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void c2_LINE_FOLLOWER_IN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_c_nargin = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  real_T c2_c_nargout = 1.0;
  real_T c2_d_nargout = 1.0;
  boolean_T c2_b_out;
  boolean_T c2_c_out;
  boolean_T c2_d_out;
  real_T c2_e_nargin = 0.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_e_out;
  real_T c2_f_nargin = 0.0;
  real_T c2_g_nargin = 0.0;
  real_T c2_f_nargout = 1.0;
  real_T c2_g_nargout = 1.0;
  boolean_T c2_f_out;
  boolean_T c2_g_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 57U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_vd_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(57, 0, 0, CV_RELATIONAL_EVAL(5U, 57U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 57U, *chartInstance->c2_sfEvent);
    c2_exit_internal_LINE_FOLLOWER_IN(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 30U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
      c2_IN_Selecter;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Selecter(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 30U,
                 *chartInstance->c2_sfEvent);
    switch (*chartInstance->c2_is_LINE_FOLLOWER_IN) {
     case c2_IN_StraightForward:
      CV_STATE_EVAL(30, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 59U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_pb_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_b_out = CV_EML_IF(59, 0, 0, CV_RELATIONAL_EVAL(5U, 59U, 0, (real_T)
        *chartInstance->c2_LeftSensor, 1.0, -1, 0U, (int32_T)
        *chartInstance->c2_LeftSensor));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 59U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_TourneGauche;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_TourneGauche(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 64U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_rb_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_e_out = CV_EML_IF(64, 0, 0, CV_RELATIONAL_EVAL(5U, 64U, 0, (real_T)
          *chartInstance->c2_RightSensor, 1.0, -1, 0U, (int32_T)
          *chartInstance->c2_RightSensor));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 64U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_TourneDroite;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_TourneDroite(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 31U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 31U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_TourneDroite:
      CV_STATE_EVAL(30, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 61U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_qb_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      if (CV_EML_COND(61, 0, 0, CV_RELATIONAL_EVAL(5U, 61U, 0, (real_T)
            *chartInstance->c2_RightSensor, 0.0, -1, 0U,
            !*chartInstance->c2_RightSensor)) && CV_EML_COND(61, 0, 1,
           CV_RELATIONAL_EVAL(5U, 61U, 1, (real_T)*chartInstance->c2_LeftSensor,
            0.0, -1, 0U, !*chartInstance->c2_LeftSensor))) {
        CV_EML_MCDC(61, 0, 0, true);
        CV_EML_IF(61, 0, 0, true);
        c2_c_out = true;
      } else {
        CV_EML_MCDC(61, 0, 0, false);
        CV_EML_IF(61, 0, 0, false);
        c2_c_out = false;
      }

      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 61U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_StraightForward;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_StraightForward(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 62U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_sb_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_f_out = CV_EML_IF(62, 0, 0, CV_RELATIONAL_EVAL(5U, 62U, 0, (real_T)
          *chartInstance->c2_LeftSensor, 1.0, -1, 0U, (int32_T)
          *chartInstance->c2_LeftSensor));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_f_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 62U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_TourneGauche;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_TourneGauche(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 32U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 32U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_TourneGauche:
      CV_STATE_EVAL(30, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 58U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ob_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      if (CV_EML_COND(58, 0, 0, CV_RELATIONAL_EVAL(5U, 58U, 0, (real_T)
            *chartInstance->c2_RightSensor, 0.0, -1, 0U,
            !*chartInstance->c2_RightSensor)) && CV_EML_COND(58, 0, 1,
           CV_RELATIONAL_EVAL(5U, 58U, 1, (real_T)*chartInstance->c2_LeftSensor,
            0.0, -1, 0U, !*chartInstance->c2_LeftSensor))) {
        CV_EML_MCDC(58, 0, 0, true);
        CV_EML_IF(58, 0, 0, true);
        c2_d_out = true;
      } else {
        CV_EML_MCDC(58, 0, 0, false);
        CV_EML_IF(58, 0, 0, false);
        c2_d_out = false;
      }

      _SFD_SYMBOL_SCOPE_POP();
      if (c2_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 58U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_StraightForward;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_StraightForward(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 63U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_tb_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_g_out = CV_EML_IF(63, 0, 0, CV_RELATIONAL_EVAL(5U, 63U, 0, (real_T)
          *chartInstance->c2_RightSensor, 1.0, -1, 0U, (int32_T)
          *chartInstance->c2_RightSensor));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_g_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 63U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_TourneDroite;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_TourneDroite(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 33U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 33U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_STATE_EVAL(30, 0, 0);

      /* Unreachable state, for coverage only */
      *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 30U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_LINE_FOLLOWER_IN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_LINE_FOLLOWER_IN) {
   case c2_IN_StraightForward:
    CV_STATE_EVAL(30, 1, 1);
    *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_TourneDroite:
    CV_STATE_EVAL(30, 1, 2);
    *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 32U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_TourneGauche:
    CV_STATE_EVAL(30, 1, 3);
    *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 33U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(30, 1, 0);
    *chartInstance->c2_is_LINE_FOLLOWER_IN = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 31U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_enter_atomic_StraightForward
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_b_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 31U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 31U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_hoistedGlobal = *chartInstance->c2_direction;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 31U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 31U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_b_hoistedGlobal = *chartInstance->c2_directionD;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_speed = 0.35;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 31U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 31U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_c_hoistedGlobal = *chartInstance->c2_speed;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_speed2 = 0.35;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 31U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 31U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_d_hoistedGlobal = *chartInstance->c2_speed2;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_atomic_TourneDroite
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_c_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_direction = 1.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 32U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 32U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_hoistedGlobal = *chartInstance->c2_direction;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 32U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 32U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_b_hoistedGlobal = *chartInstance->c2_directionD;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_speed = 0.15;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 32U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 32U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_c_hoistedGlobal = *chartInstance->c2_speed;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_speed2 = 0.4;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 32U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 32U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_d_hoistedGlobal = *chartInstance->c2_speed2;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_atomic_TourneGauche
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_d_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 33U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 33U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_hoistedGlobal = *chartInstance->c2_direction;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_directionD = 0.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 33U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 33U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_b_hoistedGlobal = *chartInstance->c2_directionD;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_speed = 0.4;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 33U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 33U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_c_hoistedGlobal = *chartInstance->c2_speed;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_speed2 = 0.15;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 33U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 33U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_d_hoistedGlobal = *chartInstance->c2_speed2;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_GeometricalShape
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  real_T c2_d_nargin = 0.0;
  real_T c2_d_nargout = 1.0;
  boolean_T c2_d_out;
  real_T c2_hoistedGlobal;
  real_T c2_b_hoistedGlobal;
  real_T c2_c_hoistedGlobal;
  real_T c2_A;
  real_T c2_x;
  real_T c2_b_x;
  real_T c2_y;
  real_T c2_d_hoistedGlobal;
  real_T c2_e_hoistedGlobal;
  real_T c2_f_hoistedGlobal;
  real_T c2_b_A;
  real_T c2_c_x;
  real_T c2_d_x;
  real_T c2_b_y;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_e_nargin = 0.0;
  real_T c2_e_nargout = 0.0;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_h_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_i_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  boolean_T guard1 = false;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 52U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ae_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(52, 0, 0, CV_RELATIONAL_EVAL(5U, 52U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 52U, *chartInstance->c2_sfEvent);
    c2_exit_internal_GeometricalShape(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
      c2_IN_Selecter;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Selecter(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 0U, *chartInstance->c2_sfEvent);
    switch (*chartInstance->c2_is_GeometricalShape) {
     case c2_IN_Circle:
      CV_STATE_EVAL(0, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 20U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_pc_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_b_out = CV_EML_IF(20, 0, 0, CV_RELATIONAL_EVAL(5U, 20U, 0,
        *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
      _SFD_SYMBOL_SCOPE_POP();
      guard1 = false;
      if (c2_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
        guard1 = true;
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 29U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_uc_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 29U,
            *chartInstance->c2_sfEvent, false);
        }

        c2_c_out = CV_EML_IF(29, 0, 0, CV_RELATIONAL_EVAL(5U, 29U, 0,
          *chartInstance->c2_Stop, 1.0, -1, 0U, *chartInstance->c2_Stop == 1.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
          guard1 = true;
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 1U,
                       *chartInstance->c2_sfEvent);
          switch (*chartInstance->c2_is_Circle) {
           case c2_IN_Off:
            CV_STATE_EVAL(1, 0, 1);
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 2U,
                         *chartInstance->c2_sfEvent);
            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 2U,
                         *chartInstance->c2_sfEvent);
            break;

           case c2_b_IN_Straight:
            CV_STATE_EVAL(1, 0, 2);
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 19U,
                         *chartInstance->c2_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ic_debug_family_names,
              c2_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U,
              c2_b_sf_marshallOut, c2_b_sf_marshallIn);
            c2_hoistedGlobal = *chartInstance->c2_d;
            c2_b_hoistedGlobal = *chartInstance->c2_side;
            c2_c_hoistedGlobal = *chartInstance->c2_C;
            c2_A = c2_c_hoistedGlobal;
            c2_x = c2_A;
            c2_b_x = c2_x;
            c2_y = c2_b_x / 2.0;
            c2_d_hoistedGlobal = *chartInstance->c2_d;
            c2_e_hoistedGlobal = *chartInstance->c2_side;
            c2_f_hoistedGlobal = *chartInstance->c2_C;
            c2_b_A = c2_f_hoistedGlobal;
            c2_c_x = c2_b_A;
            c2_d_x = c2_c_x;
            c2_b_y = c2_d_x / 2.0;
            c2_d_out = CV_EML_IF(19, 0, 0, CV_RELATIONAL_EVAL(5U, 19U, 0,
              c2_hoistedGlobal, 6.2831853071795862 * (c2_b_hoistedGlobal + c2_y),
              -1, 5U, c2_d_hoistedGlobal >= 6.2831853071795862 *
              (c2_e_hoistedGlobal + c2_b_y)));
            _SFD_SYMBOL_SCOPE_POP();
            if (c2_d_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 19U,
                           *chartInstance->c2_sfEvent);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
              *chartInstance->c2_is_Circle = c2_IN_Off;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
              _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_cb_debug_family_names,
                c2_b_debug_family_var_map);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U,
                c2_sf_marshallOut, c2_sf_marshallIn);
              _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
                c2_sf_marshallOut, c2_sf_marshallIn);
              *chartInstance->c2_speed = 0.0;
              chartInstance->c2_dataWrittenToVector[2U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 2U,
                                    *chartInstance->c2_sfEvent, false);
              if (!chartInstance->c2_dataWrittenToVector[2U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 2U,
                  *chartInstance->c2_sfEvent, false);
              }

              sf_mex_printf("%s =\\n", "speed");
              c2_g_hoistedGlobal = *chartInstance->c2_speed;
              c2_c_y = NULL;
              sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0,
                0U, 0U, 0U, 0), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                                c2_c_y);
              *chartInstance->c2_speed2 = 0.0;
              chartInstance->c2_dataWrittenToVector[3U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 2U,
                                    *chartInstance->c2_sfEvent, false);
              if (!chartInstance->c2_dataWrittenToVector[3U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 2U,
                  *chartInstance->c2_sfEvent, false);
              }

              sf_mex_printf("%s =\\n", "speed2");
              c2_h_hoistedGlobal = *chartInstance->c2_speed2;
              c2_d_y = NULL;
              sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0,
                0U, 0U, 0U, 0), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                                c2_d_y);
              *chartInstance->c2_Stop = 1.0;
              chartInstance->c2_dataWrittenToVector[4U] = true;
              _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 2U,
                                    *chartInstance->c2_sfEvent, false);
              if (!chartInstance->c2_dataWrittenToVector[4U]) {
                _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 2U,
                  *chartInstance->c2_sfEvent, false);
              }

              sf_mex_printf("%s =\\n", "Stop");
              c2_i_hoistedGlobal = *chartInstance->c2_Stop;
              c2_e_y = NULL;
              sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_i_hoistedGlobal, 0,
                0U, 0U, 0U, 0), false);
              sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                                c2_e_y);
              _SFD_SYMBOL_SCOPE_POP();
            } else {
              _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 3U,
                           *chartInstance->c2_sfEvent);
            }

            _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 3U,
                         *chartInstance->c2_sfEvent);
            break;

           default:
            CV_STATE_EVAL(1, 0, 0);

            /* Unreachable state, for coverage only */
            *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
            break;
          }
        }
      }

      if (guard1) {
        switch (*chartInstance->c2_is_Circle) {
         case c2_IN_Off:
          CV_STATE_EVAL(1, 1, 1);
          *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
          break;

         case c2_b_IN_Straight:
          CV_STATE_EVAL(1, 1, 2);
          *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
          break;

         default:
          CV_STATE_EVAL(1, 1, 0);
          *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
          break;
        }

        _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_GeometricalShape = c2_IN_Start;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_Start(chartInstance);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 1U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_Square:
      CV_STATE_EVAL(0, 0, 2);
      c2_Square(chartInstance);
      break;

     case c2_IN_Start:
      CV_STATE_EVAL(0, 0, 3);
      c2_Start(chartInstance);
      break;

     case c2_IN_StraightLine:
      CV_STATE_EVAL(0, 0, 4);
      c2_StraightLine(chartInstance);
      break;

     case c2_IN_Triangle1:
      CV_STATE_EVAL(0, 0, 5);
      c2_Triangle1(chartInstance);
      break;

     default:
      CV_STATE_EVAL(0, 0, 0);

      /* Unreachable state, for coverage only */
      *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 0U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_GeometricalShape
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_GeometricalShape) {
   case c2_IN_Circle:
    CV_STATE_EVAL(0, 1, 1);
    switch (*chartInstance->c2_is_Circle) {
     case c2_IN_Off:
      CV_STATE_EVAL(1, 1, 1);
      *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
      break;

     case c2_b_IN_Straight:
      CV_STATE_EVAL(1, 1, 2);
      *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_STATE_EVAL(1, 1, 0);
      *chartInstance->c2_is_Circle = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
      break;
    }

    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Square:
    CV_STATE_EVAL(0, 1, 2);
    c2_exit_internal_Square(chartInstance);
    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Start:
    CV_STATE_EVAL(0, 1, 3);
    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_StraightLine:
    CV_STATE_EVAL(0, 1, 4);
    switch (*chartInstance->c2_is_StraightLine) {
     case c2_IN_Off:
      CV_STATE_EVAL(14, 1, 1);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
      break;

     case c2_b_IN_Straight:
      CV_STATE_EVAL(14, 1, 2);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_STATE_EVAL(14, 1, 0);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
      break;
    }

    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Triangle1:
    CV_STATE_EVAL(0, 1, 5);
    c2_exit_internal_Triangle1(chartInstance);
    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(0, 1, 0);
    *chartInstance->c2_is_GeometricalShape = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_enter_atomic_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_f_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_reset = 0.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 17U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 17U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_hoistedGlobal = *chartInstance->c2_reset;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_internal_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  real_T c2_A;
  real_T c2_x;
  real_T c2_b_x;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
  *chartInstance->c2_is_Triangle1 = c2_IN_Straight;
  _SFD_CS_CALL(STATE_ACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_g_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 1.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 1.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  c2_e_hoistedGlobal = *chartInstance->c2_C;
  c2_A = c2_e_hoistedGlobal * 118.0 * 3.1415926535897931;
  c2_x = c2_A;
  c2_b_x = c2_x;
  *chartInstance->c2_Q = c2_b_x / 360.0;
  chartInstance->c2_dataWrittenToVector[5U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Q, 2U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[5U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Q");
  c2_f_hoistedGlobal = *chartInstance->c2_Q;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_e_y);
  *chartInstance->c2_Stop = 0.0;
  chartInstance->c2_dataWrittenToVector[4U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 18U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[4U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 18U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Stop");
  c2_g_hoistedGlobal = *chartInstance->c2_Stop;
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_f_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_e_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  real_T c2_d_nargout = 1.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_c_out;
  boolean_T c2_d_out;
  boolean_T c2_e_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_f_nargin = 0.0;
  real_T c2_f_nargout = 0.0;
  real_T c2_g_nargin = 0.0;
  real_T c2_h_nargin = 0.0;
  real_T c2_g_nargout = 0.0;
  real_T c2_h_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  real_T c2_h_hoistedGlobal;
  real_T c2_i_hoistedGlobal;
  const mxArray *c2_h_y = NULL;
  const mxArray *c2_i_y = NULL;
  real_T c2_j_hoistedGlobal;
  const mxArray *c2_j_y = NULL;
  real_T c2_k_hoistedGlobal;
  const mxArray *c2_k_y = NULL;
  boolean_T guard1 = false;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 9U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_kc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(9, 0, 0, CV_RELATIONAL_EVAL(5U, 9U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  guard1 = false;
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
    guard1 = true;
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 27U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_jc_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 27U, *chartInstance->c2_sfEvent,
        false);
    }

    c2_b_out = CV_EML_IF(27, 0, 0, CV_RELATIONAL_EVAL(5U, 27U, 0,
      *chartInstance->c2_Stop, 1.0, -1, 0U, *chartInstance->c2_Stop == 1.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
      guard1 = true;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 17U,
                   *chartInstance->c2_sfEvent);
      switch (*chartInstance->c2_is_Triangle1) {
       case c2_IN_Straight:
        CV_STATE_EVAL(17, 0, 1);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 11U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ub_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_c_out = CV_EML_IF(11, 0, 0, CV_RELATIONAL_EVAL(5U, 11U, 0,
          *chartInstance->c2_d, *chartInstance->c2_side, -1, 5U,
          *chartInstance->c2_d >= *chartInstance->c2_side));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Triangle1 = c2_IN_Turn;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_h_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 21U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 21U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_hoistedGlobal = *chartInstance->c2_speed;
          c2_y = NULL;
          sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U,
            0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 21U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 21U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_d_hoistedGlobal = *chartInstance->c2_speed2;
          c2_d_y = NULL;
          sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_d_y);
          *chartInstance->c2_direction = 1.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 21U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 21U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_g_hoistedGlobal = *chartInstance->c2_direction;
          c2_g_y = NULL;
          sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_g_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 21U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 21U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_j_hoistedGlobal = *chartInstance->c2_directionD;
          c2_j_y = NULL;
          sf_mex_assign(&c2_j_y, sf_mex_create("y", &c2_j_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_j_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 18U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 18U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Straight1:
        CV_STATE_EVAL(17, 0, 2);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 14U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_xb_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 14U,
            *chartInstance->c2_sfEvent, false);
        }

        c2_d_out = CV_EML_IF(14, 0, 0, CV_RELATIONAL_EVAL(5U, 14U, 0,
          *chartInstance->c2_d, 2.0 * *chartInstance->c2_side +
          *chartInstance->c2_Q, -1, 5U, *chartInstance->c2_d >= 2.0 *
          *chartInstance->c2_side + *chartInstance->c2_Q));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 19U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Triangle1 = c2_IN_Turn1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_k_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 22U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 22U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_b_hoistedGlobal = *chartInstance->c2_speed;
          c2_b_y = NULL;
          sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_b_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 22U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 22U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_e_hoistedGlobal = *chartInstance->c2_speed2;
          c2_e_y = NULL;
          sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_e_y);
          *chartInstance->c2_direction = 1.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 22U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 22U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_h_hoistedGlobal = *chartInstance->c2_direction;
          c2_h_y = NULL;
          sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_h_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 22U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 22U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_k_hoistedGlobal = *chartInstance->c2_directionD;
          c2_k_y = NULL;
          sf_mex_assign(&c2_k_y, sf_mex_create("y", &c2_k_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_k_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 19U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 19U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Straight2:
        CV_STATE_EVAL(17, 0, 3);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 13U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_wb_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[5U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 13U,
            *chartInstance->c2_sfEvent, false);
        }

        c2_e_out = CV_EML_IF(13, 0, 0, CV_RELATIONAL_EVAL(5U, 13U, 0,
          *chartInstance->c2_d, 3.0 * *chartInstance->c2_side + 2.0 *
          *chartInstance->c2_Q, -1, 5U, *chartInstance->c2_d >= 3.0 *
          *chartInstance->c2_side + 2.0 * *chartInstance->c2_Q));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Triangle1 = c2_IN_off;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 23U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_i_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 0.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 23U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 23U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_c_hoistedGlobal = *chartInstance->c2_speed;
          c2_c_y = NULL;
          sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_c_y);
          *chartInstance->c2_speed2 = 0.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 23U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 23U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_f_hoistedGlobal = *chartInstance->c2_speed2;
          c2_f_y = NULL;
          sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_f_y);
          *chartInstance->c2_Stop = 1.0;
          chartInstance->c2_dataWrittenToVector[4U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 23U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 23U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "Stop");
          c2_i_hoistedGlobal = *chartInstance->c2_Stop;
          c2_i_y = NULL;
          sf_mex_assign(&c2_i_y, sf_mex_create("y", &c2_i_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_i_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 20U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 20U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Turn:
        CV_STATE_EVAL(17, 0, 4);
        c2_Turn(chartInstance);
        break;

       case c2_IN_Turn1:
        CV_STATE_EVAL(17, 0, 5);
        c2_Turn1(chartInstance);
        break;

       case c2_IN_off:
        CV_STATE_EVAL(17, 0, 6);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 23U,
                     *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 23U, *chartInstance->c2_sfEvent);
        break;

       default:
        CV_STATE_EVAL(17, 0, 0);

        /* Unreachable state, for coverage only */
        *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
        break;
      }
    }
  }

  if (guard1) {
    c2_exit_internal_Triangle1(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_GeometricalShape = c2_IN_Start;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Start(chartInstance);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 17U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_Triangle1
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_Triangle1) {
   case c2_IN_Straight:
    CV_STATE_EVAL(17, 1, 1);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Straight1:
    CV_STATE_EVAL(17, 1, 2);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 19U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Straight2:
    CV_STATE_EVAL(17, 1, 3);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Turn:
    CV_STATE_EVAL(17, 1, 4);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Turn1:
    CV_STATE_EVAL(17, 1, 5);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_off:
    CV_STATE_EVAL(17, 1, 6);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 23U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(17, 1, 0);
    *chartInstance->c2_is_Triangle1 = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_Turn(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                    *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 12U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_vb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[5U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 12U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(12, 0, 0, CV_RELATIONAL_EVAL(5U, 12U, 0,
    *chartInstance->c2_d, *chartInstance->c2_side + *chartInstance->c2_Q, -1, 5U,
    *chartInstance->c2_d >= *chartInstance->c2_side + *chartInstance->c2_Q));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 12U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Triangle1 = c2_IN_Straight1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 19U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_j_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 1.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 19U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 19U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 1.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 19U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 19U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_direction = 0.0;
    chartInstance->c2_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 19U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 19U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "direction");
    c2_c_hoistedGlobal = *chartInstance->c2_direction;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    *chartInstance->c2_directionD = 1.0;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 19U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 19U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "directionD");
    c2_d_hoistedGlobal = *chartInstance->c2_directionD;
    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 21U,
                 *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 21U, *chartInstance->c2_sfEvent);
}

static void c2_Turn1(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 15U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_yb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[5U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(2U, 5U, 15U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(15, 0, 0, CV_RELATIONAL_EVAL(5U, 15U, 0,
    *chartInstance->c2_d, 2.0 * *chartInstance->c2_side + 2.0 *
    *chartInstance->c2_Q, -1, 5U, *chartInstance->c2_d >= 2.0 *
    *chartInstance->c2_side + 2.0 * *chartInstance->c2_Q));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Triangle1 = c2_IN_Straight2;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 20U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_l_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 1.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 20U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 20U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 1.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 20U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 20U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_direction = 0.0;
    chartInstance->c2_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 20U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 20U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "direction");
    c2_c_hoistedGlobal = *chartInstance->c2_direction;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    *chartInstance->c2_directionD = 1.0;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 20U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 20U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "directionD");
    c2_d_hoistedGlobal = *chartInstance->c2_directionD;
    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 22U,
                 *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 22U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_StraightLine
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_m_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_reset = 0.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 14U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 14U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_hoistedGlobal = *chartInstance->c2_reset;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_StraightLine
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_d_nargin = 0.0;
  real_T c2_d_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  boolean_T guard1 = false;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 22U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_nc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(22, 0, 0, CV_RELATIONAL_EVAL(5U, 22U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  guard1 = false;
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 22U, *chartInstance->c2_sfEvent);
    guard1 = true;
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 28U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_rc_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 28U, *chartInstance->c2_sfEvent,
        false);
    }

    c2_b_out = CV_EML_IF(28, 0, 0, CV_RELATIONAL_EVAL(5U, 28U, 0,
      *chartInstance->c2_Stop, 1.0, -1, 0U, *chartInstance->c2_Stop == 1.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
      guard1 = true;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 14U,
                   *chartInstance->c2_sfEvent);
      switch (*chartInstance->c2_is_StraightLine) {
       case c2_IN_Off:
        CV_STATE_EVAL(14, 0, 1);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 15U,
                     *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 15U, *chartInstance->c2_sfEvent);
        break;

       case c2_b_IN_Straight:
        CV_STATE_EVAL(14, 0, 2);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 17U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ac_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_c_out = CV_EML_IF(17, 0, 0, CV_RELATIONAL_EVAL(5U, 17U, 0,
          *chartInstance->c2_d, *chartInstance->c2_side, -1, 5U,
          *chartInstance->c2_d >= *chartInstance->c2_side));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_StraightLine = c2_IN_Off;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_o_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 0.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 15U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 15U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_hoistedGlobal = *chartInstance->c2_speed;
          c2_y = NULL;
          sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U,
            0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_y);
          *chartInstance->c2_speed2 = 0.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 15U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 15U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_b_hoistedGlobal = *chartInstance->c2_speed2;
          c2_b_y = NULL;
          sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_b_y);
          *chartInstance->c2_Stop = 1.0;
          chartInstance->c2_dataWrittenToVector[4U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 15U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 15U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "Stop");
          c2_c_hoistedGlobal = *chartInstance->c2_Stop;
          c2_c_y = NULL;
          sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_c_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 16U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 16U, *chartInstance->c2_sfEvent);
        break;

       default:
        CV_STATE_EVAL(14, 0, 0);

        /* Unreachable state, for coverage only */
        *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
        break;
      }
    }
  }

  if (guard1) {
    switch (*chartInstance->c2_is_StraightLine) {
     case c2_IN_Off:
      CV_STATE_EVAL(14, 1, 1);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
      break;

     case c2_b_IN_Straight:
      CV_STATE_EVAL(14, 1, 2);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_STATE_EVAL(14, 1, 0);
      *chartInstance->c2_is_StraightLine = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 15U, *chartInstance->c2_sfEvent);
      break;
    }

    _SFD_CS_CALL(STATE_INACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_GeometricalShape = c2_IN_Start;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Start(chartInstance);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 14U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_Square
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_p_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_reset = 0.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 4U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 4U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_hoistedGlobal = *chartInstance->c2_reset;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_Square(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                      *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_e_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  real_T c2_d_nargout = 1.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_c_out;
  boolean_T c2_d_out;
  boolean_T c2_e_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_f_nargin = 0.0;
  real_T c2_f_nargout = 0.0;
  real_T c2_g_nargin = 0.0;
  real_T c2_h_nargin = 0.0;
  real_T c2_g_nargout = 0.0;
  real_T c2_h_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  real_T c2_h_hoistedGlobal;
  real_T c2_i_hoistedGlobal;
  const mxArray *c2_h_y = NULL;
  const mxArray *c2_i_y = NULL;
  real_T c2_j_hoistedGlobal;
  const mxArray *c2_j_y = NULL;
  real_T c2_k_hoistedGlobal;
  real_T c2_l_hoistedGlobal;
  const mxArray *c2_k_y = NULL;
  const mxArray *c2_l_y = NULL;
  boolean_T guard1 = false;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 21U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_mc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(21, 0, 0, CV_RELATIONAL_EVAL(5U, 21U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  guard1 = false;
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 21U, *chartInstance->c2_sfEvent);
    guard1 = true;
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 30U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_sc_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 5U, 30U, *chartInstance->c2_sfEvent,
        false);
    }

    c2_b_out = CV_EML_IF(30, 0, 0, CV_RELATIONAL_EVAL(5U, 30U, 0,
      *chartInstance->c2_Stop, 1.0, -1, 0U, *chartInstance->c2_Stop == 1.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 30U, *chartInstance->c2_sfEvent);
      guard1 = true;
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 4U,
                   *chartInstance->c2_sfEvent);
      switch (*chartInstance->c2_is_Square) {
       case c2_IN_Straight:
        CV_STATE_EVAL(4, 0, 1);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 1U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_cc_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_c_out = CV_EML_IF(1, 0, 0, CV_RELATIONAL_EVAL(5U, 1U, 0,
          *chartInstance->c2_d, *chartInstance->c2_side, -1, 5U,
          *chartInstance->c2_d >= *chartInstance->c2_side));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_c_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Square = c2_b_IN_Turn;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_s_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 9U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 9U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_hoistedGlobal = *chartInstance->c2_speed;
          c2_y = NULL;
          sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U,
            0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 9U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 9U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_d_hoistedGlobal = *chartInstance->c2_speed2;
          c2_d_y = NULL;
          sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_d_y);
          *chartInstance->c2_direction = 1.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 9U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 9U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_g_hoistedGlobal = *chartInstance->c2_direction;
          c2_g_y = NULL;
          sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_g_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 9U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 9U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_j_hoistedGlobal = *chartInstance->c2_directionD;
          c2_j_y = NULL;
          sf_mex_assign(&c2_j_y, sf_mex_create("y", &c2_j_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_j_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 5U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 5U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Straight1:
        CV_STATE_EVAL(4, 0, 2);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 4U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_fc_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 4U,
            *chartInstance->c2_sfEvent, false);
        }

        c2_d_out = CV_EML_IF(4, 0, 0, CV_RELATIONAL_EVAL(5U, 4U, 0,
          *chartInstance->c2_d, 2.0 * *chartInstance->c2_side +
          *chartInstance->c2_P, -1, 5U, *chartInstance->c2_d >= 2.0 *
          *chartInstance->c2_side + *chartInstance->c2_P));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Square = c2_b_IN_Turn1;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_w_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 10U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 10U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_b_hoistedGlobal = *chartInstance->c2_speed;
          c2_b_y = NULL;
          sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_b_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 10U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 10U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_e_hoistedGlobal = *chartInstance->c2_speed2;
          c2_e_y = NULL;
          sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_e_y);
          *chartInstance->c2_direction = 1.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 10U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 10U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_h_hoistedGlobal = *chartInstance->c2_direction;
          c2_h_y = NULL;
          sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_h_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 10U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 10U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_k_hoistedGlobal = *chartInstance->c2_directionD;
          c2_k_y = NULL;
          sf_mex_assign(&c2_k_y, sf_mex_create("y", &c2_k_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_k_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 6U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 6U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Straight2:
        CV_STATE_EVAL(4, 0, 3);
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 2U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_hc_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        if (!chartInstance->c2_dataWrittenToVector[6U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 2U,
            *chartInstance->c2_sfEvent, false);
        }

        c2_e_out = CV_EML_IF(2, 0, 0, CV_RELATIONAL_EVAL(5U, 2U, 0,
          *chartInstance->c2_d, 3.0 * *chartInstance->c2_side + 2.0 *
          *chartInstance->c2_P, -1, 5U, *chartInstance->c2_d >= 3.0 *
          *chartInstance->c2_side + 2.0 * *chartInstance->c2_P));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 2U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Square = c2_IN_Turn2;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_v_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 11U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 11U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_c_hoistedGlobal = *chartInstance->c2_speed;
          c2_c_y = NULL;
          sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_c_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 11U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 11U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_f_hoistedGlobal = *chartInstance->c2_speed2;
          c2_f_y = NULL;
          sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_f_y);
          *chartInstance->c2_direction = 1.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 11U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 11U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_i_hoistedGlobal = *chartInstance->c2_direction;
          c2_i_y = NULL;
          sf_mex_assign(&c2_i_y, sf_mex_create("y", &c2_i_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_i_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 11U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 11U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_l_hoistedGlobal = *chartInstance->c2_directionD;
          c2_l_y = NULL;
          sf_mex_assign(&c2_l_y, sf_mex_create("y", &c2_l_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_l_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 7U,
                       *chartInstance->c2_sfEvent);
        }

        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 7U, *chartInstance->c2_sfEvent);
        break;

       case c2_IN_Straight3:
        CV_STATE_EVAL(4, 0, 4);
        c2_Straight3(chartInstance);
        break;

       case c2_b_IN_Turn:
        CV_STATE_EVAL(4, 0, 5);
        c2_b_Turn(chartInstance);
        break;

       case c2_b_IN_Turn1:
        CV_STATE_EVAL(4, 0, 6);
        c2_b_Turn1(chartInstance);
        break;

       case c2_IN_Turn2:
        CV_STATE_EVAL(4, 0, 7);
        c2_Turn2(chartInstance);
        break;

       case c2_b_IN_off:
        CV_STATE_EVAL(4, 0, 8);
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 12U,
                     *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 12U, *chartInstance->c2_sfEvent);
        break;

       default:
        CV_STATE_EVAL(4, 0, 0);

        /* Unreachable state, for coverage only */
        *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
        break;
      }
    }
  }

  if (guard1) {
    c2_exit_internal_Square(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_GeometricalShape = c2_IN_Start;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Start(chartInstance);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 4U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_Square
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_Square) {
   case c2_IN_Straight:
    CV_STATE_EVAL(4, 1, 1);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Straight1:
    CV_STATE_EVAL(4, 1, 2);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Straight2:
    CV_STATE_EVAL(4, 1, 3);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Straight3:
    CV_STATE_EVAL(4, 1, 4);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
    break;

   case c2_b_IN_Turn:
    CV_STATE_EVAL(4, 1, 5);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
    break;

   case c2_b_IN_Turn1:
    CV_STATE_EVAL(4, 1, 6);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Turn2:
    CV_STATE_EVAL(4, 1, 7);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
    break;

   case c2_b_IN_off:
    CV_STATE_EVAL(4, 1, 8);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 12U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(4, 1, 0);
    *chartInstance->c2_is_Square = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_b_Turn(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                      *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 0U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_dc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[6U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 0U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(0, 0, 0, CV_RELATIONAL_EVAL(5U, 0U, 0, *chartInstance->c2_d,
    *chartInstance->c2_side + *chartInstance->c2_P, -1, 5U, *chartInstance->c2_d
    >= *chartInstance->c2_side + *chartInstance->c2_P));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 0U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 9U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Square = c2_IN_Straight1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_u_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 1.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 6U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 6U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 1.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 6U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 6U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_direction = 0.0;
    chartInstance->c2_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 6U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 6U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "direction");
    c2_c_hoistedGlobal = *chartInstance->c2_direction;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    *chartInstance->c2_directionD = 1.0;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 6U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 6U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "directionD");
    c2_d_hoistedGlobal = *chartInstance->c2_directionD;
    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 9U, *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 9U, *chartInstance->c2_sfEvent);
}

static void c2_Straight3
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 7U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_bc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[6U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 7U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(7, 0, 0, CV_RELATIONAL_EVAL(5U, 7U, 0, *chartInstance->c2_d,
    4.0 * *chartInstance->c2_side + 3.0 * *chartInstance->c2_P, -1, 5U,
    *chartInstance->c2_d >= 4.0 * *chartInstance->c2_side + 3.0 *
    *chartInstance->c2_P));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Square = c2_b_IN_off;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 12U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_q_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 0.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 12U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 12U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 0.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 12U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 12U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_Stop = 1.0;
    chartInstance->c2_dataWrittenToVector[4U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 12U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[4U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 12U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "Stop");
    c2_c_hoistedGlobal = *chartInstance->c2_Stop;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 8U, *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 8U, *chartInstance->c2_sfEvent);
}

static void c2_Turn2(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 6U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ec_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[6U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 6U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(6, 0, 0, CV_RELATIONAL_EVAL(5U, 6U, 0, *chartInstance->c2_d,
    3.0 * *chartInstance->c2_side + 3.0 * *chartInstance->c2_P, -1, 5U,
    *chartInstance->c2_d >= 3.0 * *chartInstance->c2_side + 3.0 *
    *chartInstance->c2_P));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 6U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 11U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Square = c2_IN_Straight3;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 8U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_t_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 1.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 8U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 8U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 1.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 8U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 8U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_direction = 0.0;
    chartInstance->c2_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 8U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 8U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "direction");
    c2_c_hoistedGlobal = *chartInstance->c2_direction;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    *chartInstance->c2_directionD = 1.0;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 8U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 8U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "directionD");
    c2_d_hoistedGlobal = *chartInstance->c2_directionD;
    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 11U,
                 *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 11U, *chartInstance->c2_sfEvent);
}

static void c2_b_Turn1(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *
  chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 5U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_gc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  if (!chartInstance->c2_dataWrittenToVector[6U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 5U, 5U, *chartInstance->c2_sfEvent,
      false);
  }

  c2_out = CV_EML_IF(5, 0, 0, CV_RELATIONAL_EVAL(5U, 5U, 0, *chartInstance->c2_d,
    2.0 * *chartInstance->c2_side + 2.0 * *chartInstance->c2_P, -1, 5U,
    *chartInstance->c2_d >= 2.0 * *chartInstance->c2_side + 2.0 *
    *chartInstance->c2_P));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 10U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Square = c2_IN_Straight2;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 7U, *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_x_debug_family_names,
      c2_b_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    *chartInstance->c2_speed = 1.0;
    chartInstance->c2_dataWrittenToVector[2U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 7U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[2U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 7U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed");
    c2_hoistedGlobal = *chartInstance->c2_speed;
    c2_y = NULL;
    sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                  false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
    *chartInstance->c2_speed2 = 1.0;
    chartInstance->c2_dataWrittenToVector[3U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 7U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[3U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 7U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "speed2");
    c2_b_hoistedGlobal = *chartInstance->c2_speed2;
    c2_b_y = NULL;
    sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
    *chartInstance->c2_direction = 0.0;
    chartInstance->c2_dataWrittenToVector[0U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 7U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[0U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 7U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "direction");
    c2_c_hoistedGlobal = *chartInstance->c2_direction;
    c2_c_y = NULL;
    sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
    *chartInstance->c2_directionD = 1.0;
    chartInstance->c2_dataWrittenToVector[1U] = true;
    _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 7U,
                          *chartInstance->c2_sfEvent, false);
    if (!chartInstance->c2_dataWrittenToVector[1U]) {
      _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 7U, *chartInstance->c2_sfEvent,
        false);
    }

    sf_mex_printf("%s =\\n", "directionD");
    c2_d_hoistedGlobal = *chartInstance->c2_directionD;
    c2_d_y = NULL;
    sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
      0), false);
    sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
    _SFD_SYMBOL_SCOPE_POP();
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 10U,
                 *chartInstance->c2_sfEvent);
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 10U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_Start
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_y_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_reset = 1.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 13U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 13U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_hoistedGlobal = *chartInstance->c2_reset;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_Start(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_d_nargin = 0.0;
  real_T c2_e_nargin = 0.0;
  real_T c2_d_nargout = 0.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_d_out;
  real_T c2_f_nargin = 0.0;
  real_T c2_hoistedGlobal;
  real_T c2_f_nargout = 0.0;
  const mxArray *c2_y = NULL;
  real_T c2_g_nargin = 0.0;
  real_T c2_b_hoistedGlobal;
  real_T c2_g_nargout = 0.0;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  real_T c2_h_hoistedGlobal;
  const mxArray *c2_h_y = NULL;
  real_T c2_i_hoistedGlobal;
  const mxArray *c2_i_y = NULL;
  real_T c2_j_hoistedGlobal;
  const mxArray *c2_j_y = NULL;
  real_T c2_k_hoistedGlobal;
  const mxArray *c2_k_y = NULL;
  real_T c2_l_hoistedGlobal;
  const mxArray *c2_l_y = NULL;
  real_T c2_m_hoistedGlobal;
  const mxArray *c2_m_y = NULL;
  real_T c2_n_hoistedGlobal;
  const mxArray *c2_n_y = NULL;
  real_T c2_o_hoistedGlobal;
  const mxArray *c2_o_y = NULL;
  real_T c2_p_hoistedGlobal;
  const mxArray *c2_p_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 23U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_lc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(23, 0, 0, CV_RELATIONAL_EVAL(5U, 23U, 0,
    *chartInstance->c2_Up, 1.0, -1, 0U, *chartInstance->c2_Up == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 23U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_GeometricalShape = c2_IN_Triangle1;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 17U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Triangle1(chartInstance);
    c2_enter_internal_Triangle1(chartInstance);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 24U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_qc_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    c2_b_out = CV_EML_IF(24, 0, 0, CV_RELATIONAL_EVAL(5U, 24U, 0,
      *chartInstance->c2_Right, 1.0, -1, 0U, *chartInstance->c2_Right == 1.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 24U, *chartInstance->c2_sfEvent);
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_is_GeometricalShape = c2_IN_StraightLine;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 14U, *chartInstance->c2_sfEvent);
      c2_enter_atomic_StraightLine(chartInstance);
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_is_StraightLine = c2_b_IN_Straight;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 16U, *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_n_debug_family_names,
        c2_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      *chartInstance->c2_speed = 1.0;
      chartInstance->c2_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 16U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[2U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 16U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "speed");
      c2_hoistedGlobal = *chartInstance->c2_speed;
      c2_y = NULL;
      sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U,
        0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
      *chartInstance->c2_speed2 = 1.0;
      chartInstance->c2_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 16U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[3U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 16U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "speed2");
      c2_c_hoistedGlobal = *chartInstance->c2_speed2;
      c2_c_y = NULL;
      sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
      *chartInstance->c2_direction = 0.0;
      chartInstance->c2_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 16U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[0U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 16U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "direction");
      c2_f_hoistedGlobal = *chartInstance->c2_direction;
      c2_f_y = NULL;
      sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_f_y);
      *chartInstance->c2_directionD = 1.0;
      chartInstance->c2_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 16U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[1U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 16U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "directionD");
      c2_i_hoistedGlobal = *chartInstance->c2_directionD;
      c2_i_y = NULL;
      sf_mex_assign(&c2_i_y, sf_mex_create("y", &c2_i_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_i_y);
      *chartInstance->c2_Stop = 0.0;
      chartInstance->c2_dataWrittenToVector[4U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 16U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[4U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 16U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "Stop");
      c2_l_hoistedGlobal = *chartInstance->c2_Stop;
      c2_l_y = NULL;
      sf_mex_assign(&c2_l_y, sf_mex_create("y", &c2_l_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_l_y);
      _SFD_SYMBOL_SCOPE_POP();
    } else {
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 25U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_tc_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_c_out = CV_EML_IF(25, 0, 0, CV_RELATIONAL_EVAL(5U, 25U, 0,
        *chartInstance->c2_Down, 1.0, -1, 0U, *chartInstance->c2_Down == 1.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_GeometricalShape = c2_IN_Circle;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 1U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_Circle(chartInstance);
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 18U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Circle = c2_b_IN_Straight;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_bb_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        *chartInstance->c2_speed = 1.0;
        chartInstance->c2_dataWrittenToVector[2U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 3U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 3U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "speed");
        c2_b_hoistedGlobal = *chartInstance->c2_speed;
        c2_b_y = NULL;
        sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_b_y);
        *chartInstance->c2_speed2 = 0.4;
        chartInstance->c2_dataWrittenToVector[3U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 3U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 3U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "speed2");
        c2_e_hoistedGlobal = *chartInstance->c2_speed2;
        c2_e_y = NULL;
        sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_e_y);
        *chartInstance->c2_direction = 0.0;
        chartInstance->c2_dataWrittenToVector[0U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 3U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 3U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "direction");
        c2_h_hoistedGlobal = *chartInstance->c2_direction;
        c2_h_y = NULL;
        sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_h_y);
        *chartInstance->c2_directionD = 1.0;
        chartInstance->c2_dataWrittenToVector[1U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 3U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 3U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "directionD");
        c2_k_hoistedGlobal = *chartInstance->c2_directionD;
        c2_k_y = NULL;
        sf_mex_assign(&c2_k_y, sf_mex_create("y", &c2_k_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_k_y);
        *chartInstance->c2_Stop = 0.0;
        chartInstance->c2_dataWrittenToVector[4U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 3U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[4U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 3U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "Stop");
        c2_n_hoistedGlobal = *chartInstance->c2_Stop;
        c2_n_y = NULL;
        sf_mex_assign(&c2_n_y, sf_mex_create("y", &c2_n_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_n_y);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 26U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_oc_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_d_out = CV_EML_IF(26, 0, 0, CV_RELATIONAL_EVAL(5U, 26U, 0,
          *chartInstance->c2_Left, 1.0, -1, 0U, *chartInstance->c2_Left == 1.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_d_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 13U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_GeometricalShape = c2_IN_Square;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 4U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_Square(chartInstance);
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 3U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Square = c2_IN_Straight;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 5U, *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_r_debug_family_names,
            c2_b_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          *chartInstance->c2_speed = 1.0;
          chartInstance->c2_dataWrittenToVector[2U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[2U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed");
          c2_d_hoistedGlobal = *chartInstance->c2_speed;
          c2_d_y = NULL;
          sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_d_y);
          *chartInstance->c2_speed2 = 1.0;
          chartInstance->c2_dataWrittenToVector[3U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[3U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "speed2");
          c2_g_hoistedGlobal = *chartInstance->c2_speed2;
          c2_g_y = NULL;
          sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_g_y);
          *chartInstance->c2_direction = 0.0;
          chartInstance->c2_dataWrittenToVector[0U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[0U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "direction");
          c2_j_hoistedGlobal = *chartInstance->c2_direction;
          c2_j_y = NULL;
          sf_mex_assign(&c2_j_y, sf_mex_create("y", &c2_j_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_j_y);
          *chartInstance->c2_directionD = 1.0;
          chartInstance->c2_dataWrittenToVector[1U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[1U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "directionD");
          c2_m_hoistedGlobal = *chartInstance->c2_directionD;
          c2_m_y = NULL;
          sf_mex_assign(&c2_m_y, sf_mex_create("y", &c2_m_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_m_y);
          *chartInstance->c2_P = 0.767944870877505 * *chartInstance->c2_C;
          chartInstance->c2_dataWrittenToVector[6U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_P, 1U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[6U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(1U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "P");
          c2_o_hoistedGlobal = *chartInstance->c2_P;
          c2_o_y = NULL;
          sf_mex_assign(&c2_o_y, sf_mex_create("y", &c2_o_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_o_y);
          *chartInstance->c2_Stop = 0.0;
          chartInstance->c2_dataWrittenToVector[4U] = true;
          _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Stop, 3U, 4U, 5U,
                                *chartInstance->c2_sfEvent, false);
          if (!chartInstance->c2_dataWrittenToVector[4U]) {
            _SFD_DATA_READ_BEFORE_WRITE_ERROR(3U, 4U, 5U,
              *chartInstance->c2_sfEvent, false);
          }

          sf_mex_printf("%s =\\n", "Stop");
          c2_p_hoistedGlobal = *chartInstance->c2_Stop;
          c2_p_y = NULL;
          sf_mex_assign(&c2_p_y, sf_mex_create("y", &c2_p_hoistedGlobal, 0, 0U,
            0U, 0U, 0), false);
          sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                            c2_p_y);
          _SFD_SYMBOL_SCOPE_POP();
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 13U,
                       *chartInstance->c2_sfEvent);
        }
      }
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 13U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_Circle
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_ab_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_reset = 0.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 1U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 1U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_hoistedGlobal = *chartInstance->c2_reset;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_ObstacleAvoidance
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_c_nargin = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_e_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  real_T c2_c_nargout = 1.0;
  real_T c2_d_nargout = 1.0;
  real_T c2_e_nargout = 1.0;
  boolean_T c2_b_out;
  boolean_T c2_c_out;
  boolean_T c2_d_out;
  boolean_T c2_e_out;
  real_T c2_f_nargin = 0.0;
  real_T c2_g_nargin = 0.0;
  real_T c2_h_nargin = 0.0;
  real_T c2_f_nargout = 1.0;
  real_T c2_g_nargout = 1.0;
  real_T c2_h_nargout = 1.0;
  boolean_T c2_f_out;
  boolean_T c2_g_out;
  boolean_T c2_h_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_i_nargin = 0.0;
  real_T c2_i_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 54U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_xd_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(54, 0, 0, CV_RELATIONAL_EVAL(5U, 54U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 54U, *chartInstance->c2_sfEvent);
    c2_exit_internal_ObstacleAvoidance(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 34U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
      c2_IN_Selecter;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Selecter(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 34U,
                 *chartInstance->c2_sfEvent);
    switch (*chartInstance->c2_is_ObstacleAvoidance) {
     case c2_IN_CENTER:
      CV_STATE_EVAL(34, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 73U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_cd_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_b_out = CV_EML_IF(73, 0, 0, *chartInstance->c2_temporalCounter_i1 >= 2);
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 73U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_ObstacleAvoidance = c2_IN_RIGHT;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_temporalCounter_i1 = 0U;
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_eb_debug_family_names,
          c2_b_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        *chartInstance->c2_Sel2 = 1.0;
        chartInstance->c2_dataWrittenToVector[9U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel2, 25U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[9U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(25U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "Sel2");
        c2_hoistedGlobal = *chartInstance->c2_Sel2;
        c2_y = NULL;
        sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U,
          0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
        *chartInstance->c2_Sel1 = 0.0;
        chartInstance->c2_dataWrittenToVector[8U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel1, 24U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[8U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(24U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "Sel1");
        c2_b_hoistedGlobal = *chartInstance->c2_Sel1;
        c2_b_y = NULL;
        sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_b_y);
        *chartInstance->c2_Sel0 = 0.0;
        chartInstance->c2_dataWrittenToVector[10U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel0, 26U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[10U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(26U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "Sel0");
        c2_c_hoistedGlobal = *chartInstance->c2_Sel0;
        c2_c_y = NULL;
        sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_c_y);
        *chartInstance->c2_direction = 0.0;
        chartInstance->c2_dataWrittenToVector[0U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[0U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "direction");
        c2_d_hoistedGlobal = *chartInstance->c2_direction;
        c2_d_y = NULL;
        sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_d_y);
        *chartInstance->c2_directionD = 1.0;
        chartInstance->c2_dataWrittenToVector[1U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[1U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "directionD");
        c2_e_hoistedGlobal = *chartInstance->c2_directionD;
        c2_e_y = NULL;
        sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_e_y);
        *chartInstance->c2_speed = 0.5;
        chartInstance->c2_dataWrittenToVector[2U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[2U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "speed");
        c2_f_hoistedGlobal = *chartInstance->c2_speed;
        c2_f_y = NULL;
        sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_f_y);
        *chartInstance->c2_speed2 = 0.5;
        chartInstance->c2_dataWrittenToVector[3U] = true;
        _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 37U,
                              *chartInstance->c2_sfEvent, false);
        if (!chartInstance->c2_dataWrittenToVector[3U]) {
          _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 37U,
            *chartInstance->c2_sfEvent, false);
        }

        sf_mex_printf("%s =\\n", "speed2");
        c2_g_hoistedGlobal = *chartInstance->c2_speed2;
        c2_g_y = NULL;
        sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U,
          0U, 0), false);
        sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14,
                          c2_g_y);
        _SFD_SYMBOL_SCOPE_POP();
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 70U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ad_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_f_out = CV_EML_IF(70, 0, 0, CV_RELATIONAL_EVAL(5U, 70U, 0, (real_T)
          *chartInstance->c2_N, 1.0, -1, 0U, (int32_T)*chartInstance->c2_N));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_f_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 70U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_ObstacleAvoidance = c2_IN_Stop;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_temporalCounter_i1 = 0U;
          c2_enter_atomic_Stop(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 35U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 35U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_LEFTMOST:
      CV_STATE_EVAL(34, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 67U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_yc_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_c_out = CV_EML_IF(67, 0, 0, CV_RELATIONAL_EVAL(5U, 67U, 0, (real_T)
        *chartInstance->c2_O, 1.0, -1, 0U, (int32_T)*chartInstance->c2_O));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 67U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_ObstacleAvoidance = c2_IN_Stop;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_temporalCounter_i1 = 0U;
        c2_enter_atomic_Stop(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 65U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_dd_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_g_out = CV_EML_IF(65, 0, 0, *chartInstance->c2_temporalCounter_i1 >=
                             2);
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_g_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 65U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_ObstacleAvoidance = c2_IN_CENTER;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_temporalCounter_i1 = 0U;
          c2_enter_atomic_CENTER(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 36U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 36U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_RIGHT:
      CV_STATE_EVAL(34, 0, 3);
      c2_RIGHT(chartInstance);
      break;

     case c2_IN_RIGHTMOST:
      CV_STATE_EVAL(34, 0, 4);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 69U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_vc_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_d_out = CV_EML_IF(69, 0, 0, CV_RELATIONAL_EVAL(5U, 69U, 0, (real_T)
        *chartInstance->c2_E, 1.0, -1, 0U, (int32_T)*chartInstance->c2_E));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 69U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_ObstacleAvoidance = c2_IN_Stop;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_temporalCounter_i1 = 0U;
        c2_enter_atomic_Stop(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 72U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ed_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_h_out = CV_EML_IF(72, 0, 0, *chartInstance->c2_temporalCounter_i1 >=
                             2);
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_h_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 72U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_ObstacleAvoidance = c2_IN_LEFTMOST;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_temporalCounter_i1 = 0U;
          c2_enter_atomic_LEFTMOST(chartInstance);
        } else {
          _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 38U,
                       *chartInstance->c2_sfEvent);
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 38U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_Stop:
      CV_STATE_EVAL(34, 0, 5);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 66U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_xc_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_e_out = CV_EML_IF(66, 0, 0, *chartInstance->c2_temporalCounter_i1 >= 10);
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_e_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 66U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_ObstacleAvoidance = c2_IN_CENTER;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_temporalCounter_i1 = 0U;
        c2_enter_atomic_CENTER(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 39U,
                     *chartInstance->c2_sfEvent);
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 39U, *chartInstance->c2_sfEvent);
      break;

     default:
      CV_STATE_EVAL(34, 0, 0);

      /* Unreachable state, for coverage only */
      *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 34U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_ObstacleAvoidance
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_ObstacleAvoidance) {
   case c2_IN_CENTER:
    CV_STATE_EVAL(34, 1, 1);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_LEFTMOST:
    CV_STATE_EVAL(34, 1, 2);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_RIGHT:
    CV_STATE_EVAL(34, 1, 3);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_RIGHTMOST:
    CV_STATE_EVAL(34, 1, 4);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_Stop:
    CV_STATE_EVAL(34, 1, 5);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(34, 1, 0);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_enter_atomic_Stop
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_db_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 0.5;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 39U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 39U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 0.5;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 39U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 39U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 1.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 39U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 39U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 39U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 39U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_RIGHT(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                     *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  uint32_T c2_b_debug_family_var_map[2];
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 68U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_wc_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(68, 0, 0, CV_RELATIONAL_EVAL(5U, 68U, 0, (real_T)
    *chartInstance->c2_NE, 1.0, -1, 0U, (int32_T)*chartInstance->c2_NE));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 68U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_ObstacleAvoidance = c2_IN_Stop;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_temporalCounter_i1 = 0U;
    c2_enter_atomic_Stop(chartInstance);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 71U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_bd_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    c2_b_out = CV_EML_IF(71, 0, 0, *chartInstance->c2_temporalCounter_i1 >= 2);
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 71U, *chartInstance->c2_sfEvent);
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_is_ObstacleAvoidance = c2_IN_RIGHTMOST;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_temporalCounter_i1 = 0U;
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_fb_debug_family_names,
        c2_b_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      *chartInstance->c2_Sel2 = 1.0;
      chartInstance->c2_dataWrittenToVector[9U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel2, 25U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[9U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(25U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "Sel2");
      c2_hoistedGlobal = *chartInstance->c2_Sel2;
      c2_y = NULL;
      sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U,
        0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
      *chartInstance->c2_Sel1 = 0.0;
      chartInstance->c2_dataWrittenToVector[8U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel1, 24U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[8U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(24U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "Sel1");
      c2_b_hoistedGlobal = *chartInstance->c2_Sel1;
      c2_b_y = NULL;
      sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
      *chartInstance->c2_Sel0 = 1.0;
      chartInstance->c2_dataWrittenToVector[10U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel0, 26U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[10U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(26U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "Sel0");
      c2_c_hoistedGlobal = *chartInstance->c2_Sel0;
      c2_c_y = NULL;
      sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
      *chartInstance->c2_direction = 0.0;
      chartInstance->c2_dataWrittenToVector[0U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[0U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "direction");
      c2_d_hoistedGlobal = *chartInstance->c2_direction;
      c2_d_y = NULL;
      sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
      *chartInstance->c2_directionD = 1.0;
      chartInstance->c2_dataWrittenToVector[1U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[1U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "directionD");
      c2_e_hoistedGlobal = *chartInstance->c2_directionD;
      c2_e_y = NULL;
      sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_e_y);
      *chartInstance->c2_speed = 0.5;
      chartInstance->c2_dataWrittenToVector[2U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[2U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "speed");
      c2_f_hoistedGlobal = *chartInstance->c2_speed;
      c2_f_y = NULL;
      sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_f_y);
      *chartInstance->c2_speed2 = 0.5;
      chartInstance->c2_dataWrittenToVector[3U] = true;
      _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 38U,
                            *chartInstance->c2_sfEvent, false);
      if (!chartInstance->c2_dataWrittenToVector[3U]) {
        _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 38U,
          *chartInstance->c2_sfEvent, false);
      }

      sf_mex_printf("%s =\\n", "speed2");
      c2_g_hoistedGlobal = *chartInstance->c2_speed2;
      c2_g_y = NULL;
      sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U,
        0U, 0), false);
      sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_g_y);
      _SFD_SYMBOL_SCOPE_POP();
    } else {
      _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 37U,
                   *chartInstance->c2_sfEvent);
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 37U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_CENTER
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  real_T c2_h_hoistedGlobal;
  const mxArray *c2_h_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_gb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_Sel2 = 0.0;
  chartInstance->c2_dataWrittenToVector[9U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel2, 25U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[9U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(25U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel2");
  c2_hoistedGlobal = *chartInstance->c2_Sel2;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_reset = 1.0;
  chartInstance->c2_dataWrittenToVector[7U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_reset, 23U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[7U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(23U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "reset");
  c2_b_hoistedGlobal = *chartInstance->c2_reset;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_Sel1 = 1.0;
  chartInstance->c2_dataWrittenToVector[8U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel1, 24U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[8U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(24U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel1");
  c2_c_hoistedGlobal = *chartInstance->c2_Sel1;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_Sel0 = 1.0;
  chartInstance->c2_dataWrittenToVector[10U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel0, 26U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[10U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(26U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel0");
  c2_d_hoistedGlobal = *chartInstance->c2_Sel0;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_e_hoistedGlobal = *chartInstance->c2_direction;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_e_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_f_hoistedGlobal = *chartInstance->c2_directionD;
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_f_y);
  *chartInstance->c2_speed = 0.5;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_g_hoistedGlobal = *chartInstance->c2_speed;
  c2_g_y = NULL;
  sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_g_y);
  *chartInstance->c2_speed2 = 0.5;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 35U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 35U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_h_hoistedGlobal = *chartInstance->c2_speed2;
  c2_h_y = NULL;
  sf_mex_assign(&c2_h_y, sf_mex_create("y", &c2_h_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_h_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_atomic_LEFTMOST
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  real_T c2_e_hoistedGlobal;
  const mxArray *c2_e_y = NULL;
  real_T c2_f_hoistedGlobal;
  const mxArray *c2_f_y = NULL;
  real_T c2_g_hoistedGlobal;
  const mxArray *c2_g_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_hb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_Sel2 = 0.0;
  chartInstance->c2_dataWrittenToVector[9U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel2, 25U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[9U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(25U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel2");
  c2_hoistedGlobal = *chartInstance->c2_Sel2;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_Sel1 = 0.0;
  chartInstance->c2_dataWrittenToVector[8U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel1, 24U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[8U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(24U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel1");
  c2_b_hoistedGlobal = *chartInstance->c2_Sel1;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_Sel0 = 1.0;
  chartInstance->c2_dataWrittenToVector[10U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_Sel0, 26U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[10U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(26U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "Sel0");
  c2_c_hoistedGlobal = *chartInstance->c2_Sel0;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_d_hoistedGlobal = *chartInstance->c2_direction;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_e_hoistedGlobal = *chartInstance->c2_directionD;
  c2_e_y = NULL;
  sf_mex_assign(&c2_e_y, sf_mex_create("y", &c2_e_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_e_y);
  *chartInstance->c2_speed = 0.5;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_f_hoistedGlobal = *chartInstance->c2_speed;
  c2_f_y = NULL;
  sf_mex_assign(&c2_f_y, sf_mex_create("y", &c2_f_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_f_y);
  *chartInstance->c2_speed2 = 0.5;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 36U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 36U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_g_hoistedGlobal = *chartInstance->c2_speed2;
  c2_g_y = NULL;
  sf_mex_assign(&c2_g_y, sf_mex_create("y", &c2_g_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_g_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_atomic_Selecter
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_ib_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 0.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 40U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 40U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 0.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 40U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 40U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_sound = 2.0;
  chartInstance->c2_dataWrittenToVector[11U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_sound, 27U, 4U, 40U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[11U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(27U, 4U, 40U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "sound");
  c2_c_hoistedGlobal = *chartInstance->c2_sound;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_Joystick(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
  *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_c_nargin = 0.0;
  real_T c2_d_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  real_T c2_c_nargout = 1.0;
  real_T c2_d_nargout = 1.0;
  boolean_T c2_b_out;
  boolean_T c2_c_out;
  boolean_T c2_d_out;
  real_T c2_e_nargin = 0.0;
  real_T c2_f_nargin = 0.0;
  real_T c2_g_nargin = 0.0;
  real_T c2_e_nargout = 1.0;
  real_T c2_f_nargout = 1.0;
  real_T c2_g_nargout = 1.0;
  boolean_T c2_e_out;
  boolean_T c2_f_out;
  boolean_T c2_g_out;
  real_T c2_h_nargin = 0.0;
  real_T c2_i_nargin = 0.0;
  real_T c2_j_nargin = 0.0;
  real_T c2_h_nargout = 1.0;
  real_T c2_i_nargout = 1.0;
  real_T c2_j_nargout = 1.0;
  boolean_T c2_h_out;
  boolean_T c2_i_out;
  boolean_T c2_j_out;
  real_T c2_k_nargin = 0.0;
  real_T c2_k_nargout = 1.0;
  boolean_T c2_k_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 50U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_de_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(50, 0, 0, CV_RELATIONAL_EVAL(5U, 50U, 0,
    *chartInstance->c2_Middle, 1.0, -1, 0U, *chartInstance->c2_Middle == 1.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 50U, *chartInstance->c2_sfEvent);
    c2_exit_internal_Joystick(chartInstance);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 24U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio =
      c2_IN_Selecter;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 40U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_Selecter(chartInstance);
  } else {
    _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 24U,
                 *chartInstance->c2_sfEvent);
    switch (*chartInstance->c2_is_Joystick) {
     case c2_IN_DOWN:
      CV_STATE_EVAL(24, 0, 1);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 37U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ud_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_b_out = CV_EML_IF(37, 0, 0, CV_RELATIONAL_EVAL(5U, 37U, 0,
        *chartInstance->c2_Right, 0.0, -1, 4U, *chartInstance->c2_Right > 0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_b_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 37U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_b_IN_RIGHT;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_RIGHT(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 41U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_sd_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_e_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_e_out = CV_EML_IF(41, 0, 0, CV_RELATIONAL_EVAL(5U, 41U, 0,
          *chartInstance->c2_Left, 0.0, -1, 4U, *chartInstance->c2_Left > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_e_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 41U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Joystick = c2_IN_LEFT;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_LEFT(chartInstance);
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 36U,
                       *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_ld_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_h_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          c2_h_out = CV_EML_IF(36, 0, 0, CV_RELATIONAL_EVAL(5U, 36U, 0,
            *chartInstance->c2_Up, 0.0, -1, 4U, *chartInstance->c2_Up > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c2_h_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 36U, *chartInstance->c2_sfEvent);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_is_Joystick = c2_IN_UP;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
            c2_enter_atomic_UP(chartInstance);
          } else {
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 25U,
                         *chartInstance->c2_sfEvent);
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 25U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_LEFT:
      CV_STATE_EVAL(24, 0, 2);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 49U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_rd_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_c_out = CV_EML_IF(49, 0, 0, CV_RELATIONAL_EVAL(5U, 49U, 0,
        *chartInstance->c2_Down, 0.0, -1, 4U, *chartInstance->c2_Down > 0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 49U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_IN_DOWN;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_DOWN(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 39U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_hd_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_f_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_f_out = CV_EML_IF(39, 0, 0, CV_RELATIONAL_EVAL(5U, 39U, 0,
          *chartInstance->c2_Up, 0.0, -1, 4U, *chartInstance->c2_Up > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_f_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 39U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Joystick = c2_IN_UP;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_UP(chartInstance);
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 45U,
                       *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_md_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_i_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          c2_i_out = CV_EML_IF(45, 0, 0, CV_RELATIONAL_EVAL(5U, 45U, 0,
            *chartInstance->c2_Right, 0.0, -1, 4U, *chartInstance->c2_Right >
            0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c2_i_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 45U, *chartInstance->c2_sfEvent);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_is_Joystick = c2_b_IN_RIGHT;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
            c2_enter_atomic_RIGHT(chartInstance);
          } else {
            _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 26U,
                         *chartInstance->c2_sfEvent);
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 26U, *chartInstance->c2_sfEvent);
      break;

     case c2_IN_OFF:
      CV_STATE_EVAL(24, 0, 3);
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 34U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_kd_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_d_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_d_out = CV_EML_IF(34, 0, 0, CV_RELATIONAL_EVAL(5U, 34U, 0,
        *chartInstance->c2_Up, 0.0, -1, 4U, *chartInstance->c2_Up > 0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_d_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 34U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_IN_UP;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_UP(chartInstance);
      } else {
        _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 48U,
                     *chartInstance->c2_sfEvent);
        _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_qd_debug_family_names,
          c2_debug_family_var_map);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargin, 0U, c2_sf_marshallOut,
          c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_nargout, 1U,
          c2_sf_marshallOut, c2_sf_marshallIn);
        _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_g_out, 2U, c2_b_sf_marshallOut,
          c2_b_sf_marshallIn);
        c2_g_out = CV_EML_IF(48, 0, 0, CV_RELATIONAL_EVAL(5U, 48U, 0,
          *chartInstance->c2_Down, 0.0, -1, 4U, *chartInstance->c2_Down > 0.0));
        _SFD_SYMBOL_SCOPE_POP();
        if (c2_g_out) {
          _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 48U, *chartInstance->c2_sfEvent);
          _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
          *chartInstance->c2_is_Joystick = c2_IN_DOWN;
          _SFD_CS_CALL(STATE_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
          c2_enter_atomic_DOWN(chartInstance);
        } else {
          _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 47U,
                       *chartInstance->c2_sfEvent);
          _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_od_debug_family_names,
            c2_debug_family_var_map);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_j_nargin, 0U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_j_nargout, 1U,
            c2_sf_marshallOut, c2_sf_marshallIn);
          _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_j_out, 2U,
            c2_b_sf_marshallOut, c2_b_sf_marshallIn);
          c2_j_out = CV_EML_IF(47, 0, 0, CV_RELATIONAL_EVAL(5U, 47U, 0,
            *chartInstance->c2_Left, 0.0, -1, 4U, *chartInstance->c2_Left > 0.0));
          _SFD_SYMBOL_SCOPE_POP();
          if (c2_j_out) {
            _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 47U, *chartInstance->c2_sfEvent);
            _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
            *chartInstance->c2_is_Joystick = c2_IN_LEFT;
            _SFD_CS_CALL(STATE_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
            c2_enter_atomic_LEFT(chartInstance);
          } else {
            _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 40U,
                         *chartInstance->c2_sfEvent);
            _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_nd_debug_family_names,
              c2_debug_family_var_map);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_k_nargin, 0U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_k_nargout, 1U,
              c2_sf_marshallOut, c2_sf_marshallIn);
            _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_k_out, 2U,
              c2_b_sf_marshallOut, c2_b_sf_marshallIn);
            c2_k_out = CV_EML_IF(40, 0, 0, CV_RELATIONAL_EVAL(5U, 40U, 0,
              *chartInstance->c2_Right, 0.0, -1, 4U, *chartInstance->c2_Right >
              0.0));
            _SFD_SYMBOL_SCOPE_POP();
            if (c2_k_out) {
              _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 40U,
                           *chartInstance->c2_sfEvent);
              _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
              *chartInstance->c2_is_Joystick = c2_b_IN_RIGHT;
              _SFD_CS_CALL(STATE_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
              c2_enter_atomic_RIGHT(chartInstance);
            } else {
              _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 27U,
                           *chartInstance->c2_sfEvent);
            }
          }
        }
      }

      _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 27U, *chartInstance->c2_sfEvent);
      break;

     case c2_b_IN_RIGHT:
      CV_STATE_EVAL(24, 0, 4);
      c2_b_RIGHT(chartInstance);
      break;

     case c2_IN_UP:
      CV_STATE_EVAL(24, 0, 5);
      c2_UP(chartInstance);
      break;

     default:
      CV_STATE_EVAL(24, 0, 0);

      /* Unreachable state, for coverage only */
      *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
      break;
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 24U, *chartInstance->c2_sfEvent);
}

static void c2_exit_internal_Joystick
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  switch (*chartInstance->c2_is_Joystick) {
   case c2_IN_DOWN:
    CV_STATE_EVAL(24, 1, 1);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_LEFT:
    CV_STATE_EVAL(24, 1, 2);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_OFF:
    CV_STATE_EVAL(24, 1, 3);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 27U, *chartInstance->c2_sfEvent);
    break;

   case c2_b_IN_RIGHT:
    CV_STATE_EVAL(24, 1, 4);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
    break;

   case c2_IN_UP:
    CV_STATE_EVAL(24, 1, 5);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
    break;

   default:
    CV_STATE_EVAL(24, 1, 0);
    *chartInstance->c2_is_Joystick = c2_IN_NO_ACTIVE_CHILD;
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
    break;
  }
}

static void c2_enter_atomic_UP
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_kb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 1.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 29U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 29U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 1.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 29U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 29U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 1.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 29U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 29U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 0.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 29U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 29U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_UP(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct
                  *chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 38U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_gd_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(38, 0, 0, CV_RELATIONAL_EVAL(5U, 38U, 0,
    *chartInstance->c2_Right, 0.0, -1, 4U, *chartInstance->c2_Right > 0.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 38U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Joystick = c2_b_IN_RIGHT;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_RIGHT(chartInstance);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 43U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_fd_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    c2_b_out = CV_EML_IF(43, 0, 0, CV_RELATIONAL_EVAL(5U, 43U, 0,
      *chartInstance->c2_Left, 0.0, -1, 4U, *chartInstance->c2_Left > 0.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 43U, *chartInstance->c2_sfEvent);
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_is_Joystick = c2_IN_LEFT;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
      c2_enter_atomic_LEFT(chartInstance);
    } else {
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 42U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_jd_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_c_out = CV_EML_IF(42, 0, 0, CV_RELATIONAL_EVAL(5U, 42U, 0,
        *chartInstance->c2_Down, 0.0, -1, 4U, *chartInstance->c2_Down > 0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 42U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_IN_DOWN;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_DOWN(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 29U,
                     *chartInstance->c2_sfEvent);
      }
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 29U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_LEFT
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_lb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 1.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 26U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 26U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 0.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 26U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 26U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 1.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 26U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 26U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 0.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 26U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 26U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_enter_atomic_RIGHT
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_mb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 0.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 28U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 28U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 1.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 28U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 28U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 1.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 28U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 28U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 0.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 28U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 28U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void c2_b_RIGHT(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *
  chartInstance)
{
  uint32_T c2_debug_family_var_map[3];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 1.0;
  boolean_T c2_out;
  real_T c2_b_nargin = 0.0;
  real_T c2_b_nargout = 1.0;
  boolean_T c2_b_out;
  real_T c2_c_nargin = 0.0;
  real_T c2_c_nargout = 1.0;
  boolean_T c2_c_out;
  _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 44U, *chartInstance->c2_sfEvent);
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_id_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_out, 2U, c2_b_sf_marshallOut,
    c2_b_sf_marshallIn);
  c2_out = CV_EML_IF(44, 0, 0, CV_RELATIONAL_EVAL(5U, 44U, 0,
    *chartInstance->c2_Up, 0.0, -1, 4U, *chartInstance->c2_Up > 0.0));
  _SFD_SYMBOL_SCOPE_POP();
  if (c2_out) {
    _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 44U, *chartInstance->c2_sfEvent);
    _SFD_CS_CALL(STATE_INACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
    *chartInstance->c2_is_Joystick = c2_IN_UP;
    _SFD_CS_CALL(STATE_ACTIVE_TAG, 29U, *chartInstance->c2_sfEvent);
    c2_enter_atomic_UP(chartInstance);
  } else {
    _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 35U,
                 *chartInstance->c2_sfEvent);
    _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_td_debug_family_names,
      c2_debug_family_var_map);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargin, 0U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_nargout, 1U, c2_sf_marshallOut,
      c2_sf_marshallIn);
    _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_b_out, 2U, c2_b_sf_marshallOut,
      c2_b_sf_marshallIn);
    c2_b_out = CV_EML_IF(35, 0, 0, CV_RELATIONAL_EVAL(5U, 35U, 0,
      *chartInstance->c2_Down, 0.0, -1, 4U, *chartInstance->c2_Down > 0.0));
    _SFD_SYMBOL_SCOPE_POP();
    if (c2_b_out) {
      _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 35U, *chartInstance->c2_sfEvent);
      _SFD_CS_CALL(STATE_INACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
      *chartInstance->c2_is_Joystick = c2_IN_DOWN;
      _SFD_CS_CALL(STATE_ACTIVE_TAG, 25U, *chartInstance->c2_sfEvent);
      c2_enter_atomic_DOWN(chartInstance);
    } else {
      _SFD_CT_CALL(TRANSITION_BEFORE_PROCESSING_TAG, 46U,
                   *chartInstance->c2_sfEvent);
      _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 3U, 3U, c2_pd_debug_family_names,
        c2_debug_family_var_map);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargin, 0U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_nargout, 1U, c2_sf_marshallOut,
        c2_sf_marshallIn);
      _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_c_out, 2U, c2_b_sf_marshallOut,
        c2_b_sf_marshallIn);
      c2_c_out = CV_EML_IF(46, 0, 0, CV_RELATIONAL_EVAL(5U, 46U, 0,
        *chartInstance->c2_Left, 0.0, -1, 4U, *chartInstance->c2_Left > 0.0));
      _SFD_SYMBOL_SCOPE_POP();
      if (c2_c_out) {
        _SFD_CT_CALL(TRANSITION_ACTIVE_TAG, 46U, *chartInstance->c2_sfEvent);
        _SFD_CS_CALL(STATE_INACTIVE_TAG, 28U, *chartInstance->c2_sfEvent);
        *chartInstance->c2_is_Joystick = c2_IN_LEFT;
        _SFD_CS_CALL(STATE_ACTIVE_TAG, 26U, *chartInstance->c2_sfEvent);
        c2_enter_atomic_LEFT(chartInstance);
      } else {
        _SFD_CS_CALL(STATE_ENTER_DURING_FUNCTION_TAG, 28U,
                     *chartInstance->c2_sfEvent);
      }
    }
  }

  _SFD_CS_CALL(EXIT_OUT_OF_FUNCTION_TAG, 28U, *chartInstance->c2_sfEvent);
}

static void c2_enter_atomic_DOWN
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  uint32_T c2_debug_family_var_map[2];
  real_T c2_nargin = 0.0;
  real_T c2_nargout = 0.0;
  real_T c2_hoistedGlobal;
  const mxArray *c2_y = NULL;
  real_T c2_b_hoistedGlobal;
  const mxArray *c2_b_y = NULL;
  real_T c2_c_hoistedGlobal;
  const mxArray *c2_c_y = NULL;
  real_T c2_d_hoistedGlobal;
  const mxArray *c2_d_y = NULL;
  _SFD_SYMBOL_SCOPE_PUSH_EML(0U, 2U, 2U, c2_nb_debug_family_names,
    c2_debug_family_var_map);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargin, 0U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  _SFD_SYMBOL_SCOPE_ADD_EML_IMPORTABLE(&c2_nargout, 1U, c2_sf_marshallOut,
    c2_sf_marshallIn);
  *chartInstance->c2_speed = 1.0;
  chartInstance->c2_dataWrittenToVector[2U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed, 21U, 4U, 25U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[2U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(21U, 4U, 25U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed");
  c2_hoistedGlobal = *chartInstance->c2_speed;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_hoistedGlobal, 0, 0U, 0U, 0U, 0),
                false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_y);
  *chartInstance->c2_speed2 = 1.0;
  chartInstance->c2_dataWrittenToVector[3U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_speed2, 22U, 4U, 25U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[3U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(22U, 4U, 25U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "speed2");
  c2_b_hoistedGlobal = *chartInstance->c2_speed2;
  c2_b_y = NULL;
  sf_mex_assign(&c2_b_y, sf_mex_create("y", &c2_b_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_b_y);
  *chartInstance->c2_direction = 0.0;
  chartInstance->c2_dataWrittenToVector[0U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_direction, 19U, 4U, 25U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[0U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(19U, 4U, 25U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "direction");
  c2_c_hoistedGlobal = *chartInstance->c2_direction;
  c2_c_y = NULL;
  sf_mex_assign(&c2_c_y, sf_mex_create("y", &c2_c_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_c_y);
  *chartInstance->c2_directionD = 1.0;
  chartInstance->c2_dataWrittenToVector[1U] = true;
  _SFD_DATA_RANGE_CHECK(*chartInstance->c2_directionD, 20U, 4U, 25U,
                        *chartInstance->c2_sfEvent, false);
  if (!chartInstance->c2_dataWrittenToVector[1U]) {
    _SFD_DATA_READ_BEFORE_WRITE_ERROR(20U, 4U, 25U, *chartInstance->c2_sfEvent,
      false);
  }

  sf_mex_printf("%s =\\n", "directionD");
  c2_d_hoistedGlobal = *chartInstance->c2_directionD;
  c2_d_y = NULL;
  sf_mex_assign(&c2_d_y, sf_mex_create("y", &c2_d_hoistedGlobal, 0, 0U, 0U, 0U,
    0), false);
  sf_mex_call_debug(sfGlobalDebugInstanceStruct, "disp", 0U, 1U, 14, c2_d_y);
  _SFD_SYMBOL_SCOPE_POP();
}

static void init_script_number_translation(uint32_T c2_machineNumber, uint32_T
  c2_chartNumber, uint32_T c2_instanceNumber)
{
  (void)(c2_machineNumber);
  (void)(c2_chartNumber);
  (void)(c2_instanceNumber);
}

static const mxArray *c2_sf_marshallOut(void *chartInstanceVoid, void *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  real_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_u = *(real_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 0, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static real_T c2_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_nargout, const char_T *c2_identifier)
{
  real_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  return c2_y;
}

static real_T c2_b_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  real_T c2_y;
  real_T c2_d0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_d0, 1, 0, 0U, 0, 0U, 0);
  c2_y = c2_d0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_nargout;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  real_T c2_y;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_nargout = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_b_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_nargout), &c2_thisId);
  sf_mex_destroy(&c2_nargout);
  *(real_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_b_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  boolean_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_u = *(boolean_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 11, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static boolean_T c2_c_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  boolean_T c2_y;
  boolean_T c2_b0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_b0, 1, 11, 0U, 0, 0U, 0);
  c2_y = c2_b0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_b_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_sf_internal_predicateOutput;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  boolean_T c2_y;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_sf_internal_predicateOutput = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_c_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_sf_internal_predicateOutput), &c2_thisId);
  sf_mex_destroy(&c2_sf_internal_predicateOutput);
  *(boolean_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

const mxArray
  *sf_c2_globalfunctionforBigrobotBisTestAudio_get_eml_resolved_functions_info
  (void)
{
  const mxArray *c2_nameCaptureInfo = NULL;
  c2_nameCaptureInfo = NULL;
  sf_mex_assign(&c2_nameCaptureInfo, sf_mex_create("nameCaptureInfo", NULL, 0,
    0U, 1U, 0U, 2, 0, 1), false);
  return c2_nameCaptureInfo;
}

static const mxArray *c2_c_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  int32_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_u = *(int32_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 6, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static int32_T c2_d_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  int32_T c2_y;
  int32_T c2_i1;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_i1, 1, 6, 0U, 0, 0U, 0);
  c2_y = c2_i1;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_c_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_sfEvent;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  int32_T c2_y;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_b_sfEvent = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_d_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_sfEvent),
    &c2_thisId);
  sf_mex_destroy(&c2_b_sfEvent);
  *(int32_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static const mxArray *c2_d_sf_marshallOut(void *chartInstanceVoid, void
  *c2_inData)
{
  const mxArray *c2_mxArrayOutData;
  uint8_T c2_u;
  const mxArray *c2_y = NULL;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_mxArrayOutData = NULL;
  c2_mxArrayOutData = NULL;
  c2_u = *(uint8_T *)c2_inData;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_create("y", &c2_u, 3, 0U, 0U, 0U, 0), false);
  sf_mex_assign(&c2_mxArrayOutData, c2_y, false);
  return c2_mxArrayOutData;
}

static uint8_T c2_e_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_is_LINE_FOLLOWER_IN, const char_T *c2_identifier)
{
  uint8_T c2_y;
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_LINE_FOLLOWER_IN), &c2_thisId);
  sf_mex_destroy(&c2_b_is_LINE_FOLLOWER_IN);
  return c2_y;
}

static uint8_T c2_f_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  uint8_T c2_y;
  uint8_T c2_u0;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), &c2_u0, 1, 3, 0U, 0, 0U, 0);
  c2_y = c2_u0;
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static void c2_d_sf_marshallIn(void *chartInstanceVoid, const mxArray
  *c2_mxArrayInData, const char_T *c2_varName, void *c2_outData)
{
  const mxArray *c2_b_is_LINE_FOLLOWER_IN;
  const char_T *c2_identifier;
  emlrtMsgIdentifier c2_thisId;
  uint8_T c2_y;
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    chartInstanceVoid;
  c2_b_is_LINE_FOLLOWER_IN = sf_mex_dup(c2_mxArrayInData);
  c2_identifier = c2_varName;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_y = c2_f_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_is_LINE_FOLLOWER_IN), &c2_thisId);
  sf_mex_destroy(&c2_b_is_LINE_FOLLOWER_IN);
  *(uint8_T *)c2_outData = c2_y;
  sf_mex_destroy(&c2_mxArrayInData);
}

static void c2_g_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_dataWrittenToVector, const char_T *c2_identifier,
   boolean_T c2_y[13])
{
  emlrtMsgIdentifier c2_thisId;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  c2_h_emlrt_marshallIn(chartInstance, sf_mex_dup(c2_b_dataWrittenToVector),
                        &c2_thisId, c2_y);
  sf_mex_destroy(&c2_b_dataWrittenToVector);
}

static void c2_h_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId, boolean_T c2_y[13])
{
  boolean_T c2_bv1[13];
  int32_T c2_i2;
  (void)chartInstance;
  sf_mex_import(c2_parentId, sf_mex_dup(c2_u), c2_bv1, 1, 11, 0U, 1, 0U, 1, 13);
  for (c2_i2 = 0; c2_i2 < 13; c2_i2++) {
    c2_y[c2_i2] = c2_bv1[c2_i2];
  }

  sf_mex_destroy(&c2_u);
}

static const mxArray *c2_i_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_b_setSimStateSideEffectsInfo, const char_T *c2_identifier)
{
  const mxArray *c2_y = NULL;
  emlrtMsgIdentifier c2_thisId;
  c2_y = NULL;
  c2_thisId.fIdentifier = (const char *)c2_identifier;
  c2_thisId.fParent = NULL;
  c2_thisId.bParentIsCell = false;
  sf_mex_assign(&c2_y, c2_j_emlrt_marshallIn(chartInstance, sf_mex_dup
    (c2_b_setSimStateSideEffectsInfo), &c2_thisId), false);
  sf_mex_destroy(&c2_b_setSimStateSideEffectsInfo);
  return c2_y;
}

static const mxArray *c2_j_emlrt_marshallIn
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   const mxArray *c2_u, const emlrtMsgIdentifier *c2_parentId)
{
  const mxArray *c2_y = NULL;
  (void)chartInstance;
  (void)c2_parentId;
  c2_y = NULL;
  sf_mex_assign(&c2_y, sf_mex_duplicatearraysafe(&c2_u), false);
  sf_mex_destroy(&c2_u);
  return c2_y;
}

static const mxArray *sf_get_hover_data_for_msg
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance,
   int32_T c2_ssid)
{
  const mxArray *c2_msgInfo;
  (void)chartInstance;
  (void)c2_ssid;
  c2_msgInfo = NULL;
  return NULL;
}

static void c2_init_sf_message_store_memory
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_dsm_address_info
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  (void)chartInstance;
}

static void init_simulink_io_address
  (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance)
{
  chartInstance->c2_fEmlrtCtx = (void *)sfrtGetEmlrtCtx(chartInstance->S);
  chartInstance->c2_sfEvent = (int32_T *)ssGetDWork_wrapper(chartInstance->S, 0);
  chartInstance->c2_is_active_c2_globalfunctionforBigrobotBisTestAudio =
    (uint8_T *)ssGetDWork_wrapper(chartInstance->S, 1);
  chartInstance->c2_is_c2_globalfunctionforBigrobotBisTestAudio = (uint8_T *)
    ssGetDWork_wrapper(chartInstance->S, 2);
  chartInstance->c2_is_LINE_FOLLOWER_IN = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 3);
  chartInstance->c2_is_GeometricalShape = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 4);
  chartInstance->c2_is_Triangle1 = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 5);
  chartInstance->c2_is_StraightLine = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 6);
  chartInstance->c2_is_Square = (uint8_T *)ssGetDWork_wrapper(chartInstance->S,
    7);
  chartInstance->c2_is_Circle = (uint8_T *)ssGetDWork_wrapper(chartInstance->S,
    8);
  chartInstance->c2_is_ObstacleAvoidance = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 9);
  chartInstance->c2_is_Joystick = (uint8_T *)ssGetDWork_wrapper(chartInstance->S,
    10);
  chartInstance->c2_direction = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 1);
  chartInstance->c2_side = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 0);
  chartInstance->c2_C = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    1);
  chartInstance->c2_d = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    2);
  chartInstance->c2_directionD = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 2);
  chartInstance->c2_Up = (real_T *)ssGetInputPortSignal_wrapper(chartInstance->S,
    3);
  chartInstance->c2_Right = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c2_Down = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c2_Left = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c2_Middle = (real_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 7);
  chartInstance->c2_speed = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 3);
  chartInstance->c2_speed2 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 4);
  chartInstance->c2_Stop = (real_T *)ssGetDWork_wrapper(chartInstance->S, 11);
  chartInstance->c2_Q = (real_T *)ssGetDWork_wrapper(chartInstance->S, 12);
  chartInstance->c2_P = (real_T *)ssGetDWork_wrapper(chartInstance->S, 13);
  chartInstance->c2_reset = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 5);
  chartInstance->c2_LeftSensor = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 8);
  chartInstance->c2_RightSensor = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 9);
  chartInstance->c2_Sel1 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 6);
  chartInstance->c2_Sel2 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 7);
  chartInstance->c2_Sel0 = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 8);
  chartInstance->c2_NE = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 10);
  chartInstance->c2_E = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 11);
  chartInstance->c2_NO = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 12);
  chartInstance->c2_O = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 13);
  chartInstance->c2_N = (boolean_T *)ssGetInputPortSignal_wrapper
    (chartInstance->S, 14);
  chartInstance->c2_sound = (real_T *)ssGetOutputPortSignal_wrapper
    (chartInstance->S, 9);
  chartInstance->c2_counter = (real_T *)ssGetDWork_wrapper(chartInstance->S, 14);
  chartInstance->c2_temporalCounter_i1 = (uint8_T *)ssGetDWork_wrapper
    (chartInstance->S, 15);
}

/* SFunction Glue Code */
#ifdef utFree
#undef utFree
#endif

#ifdef utMalloc
#undef utMalloc
#endif

#ifdef __cplusplus

extern "C" void *utMalloc(size_t size);
extern "C" void utFree(void*);

#else

extern void *utMalloc(size_t size);
extern void utFree(void*);

#endif

static uint32_T* sf_get_sfun_dwork_checksum(void);
void sf_c2_globalfunctionforBigrobotBisTestAudio_get_check_sum(mxArray *plhs[])
{
  ((real_T *)mxGetPr((plhs[0])))[0] = (real_T)(2789561690U);
  ((real_T *)mxGetPr((plhs[0])))[1] = (real_T)(466937500U);
  ((real_T *)mxGetPr((plhs[0])))[2] = (real_T)(3606917034U);
  ((real_T *)mxGetPr((plhs[0])))[3] = (real_T)(1884742332U);
}

mxArray* sf_c2_globalfunctionforBigrobotBisTestAudio_get_post_codegen_info(void);
mxArray *sf_c2_globalfunctionforBigrobotBisTestAudio_get_autoinheritance_info
  (void)
{
  const char *autoinheritanceFields[] = { "checksum", "inputs", "parameters",
    "outputs", "locals", "postCodegenInfo" };

  mxArray *mxAutoinheritanceInfo = mxCreateStructMatrix(1, 1, sizeof
    (autoinheritanceFields)/sizeof(autoinheritanceFields[0]),
    autoinheritanceFields);

  {
    mxArray *mxChecksum = mxCreateString("TPL8KMUUGdzQojacQ8GoDC");
    mxSetField(mxAutoinheritanceInfo,0,"checksum",mxChecksum);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,15,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,9,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,9,"type",mxType);
    }

    mxSetField(mxData,9,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,10,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,10,"type",mxType);
    }

    mxSetField(mxData,10,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,11,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,11,"type",mxType);
    }

    mxSetField(mxData,11,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,12,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,12,"type",mxType);
    }

    mxSetField(mxData,12,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,13,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,13,"type",mxType);
    }

    mxSetField(mxData,13,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,14,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(1));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,14,"type",mxType);
    }

    mxSetField(mxData,14,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"inputs",mxData);
  }

  {
    mxSetField(mxAutoinheritanceInfo,0,"parameters",mxCreateDoubleMatrix(0,0,
                mxREAL));
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,9,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,4,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,4,"type",mxType);
    }

    mxSetField(mxData,4,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,5,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,5,"type",mxType);
    }

    mxSetField(mxData,5,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,6,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,6,"type",mxType);
    }

    mxSetField(mxData,6,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,7,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,7,"type",mxType);
    }

    mxSetField(mxData,7,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,8,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,8,"type",mxType);
    }

    mxSetField(mxData,8,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"outputs",mxData);
  }

  {
    const char *dataFields[] = { "size", "type", "complexity" };

    mxArray *mxData = mxCreateStructMatrix(1,4,3,dataFields);

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,0,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,0,"type",mxType);
    }

    mxSetField(mxData,0,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,1,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,1,"type",mxType);
    }

    mxSetField(mxData,1,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,2,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,2,"type",mxType);
    }

    mxSetField(mxData,2,"complexity",mxCreateDoubleScalar(0));

    {
      mxArray *mxSize = mxCreateDoubleMatrix(1,0,mxREAL);
      double *pr = mxGetPr(mxSize);
      mxSetField(mxData,3,"size",mxSize);
    }

    {
      const char *typeFields[] = { "base", "fixpt", "isFixedPointType" };

      mxArray *mxType = mxCreateStructMatrix(1,1,sizeof(typeFields)/sizeof
        (typeFields[0]),typeFields);
      mxSetField(mxType,0,"base",mxCreateDoubleScalar(10));
      mxSetField(mxType,0,"fixpt",mxCreateDoubleMatrix(0,0,mxREAL));
      mxSetField(mxType,0,"isFixedPointType",mxCreateDoubleScalar(0));
      mxSetField(mxData,3,"type",mxType);
    }

    mxSetField(mxData,3,"complexity",mxCreateDoubleScalar(0));
    mxSetField(mxAutoinheritanceInfo,0,"locals",mxData);
  }

  {
    mxArray* mxPostCodegenInfo =
      sf_c2_globalfunctionforBigrobotBisTestAudio_get_post_codegen_info();
    mxSetField(mxAutoinheritanceInfo,0,"postCodegenInfo",mxPostCodegenInfo);
  }

  return(mxAutoinheritanceInfo);
}

mxArray *sf_c2_globalfunctionforBigrobotBisTestAudio_third_party_uses_info(void)
{
  mxArray * mxcell3p = mxCreateCellMatrix(1,0);
  return(mxcell3p);
}

mxArray *sf_c2_globalfunctionforBigrobotBisTestAudio_jit_fallback_info(void)
{
  const char *infoFields[] = { "fallbackType", "fallbackReason",
    "hiddenFallbackType", "hiddenFallbackReason", "incompatibleSymbol" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 5, infoFields);
  mxArray *fallbackType = mxCreateString("early");
  mxArray *fallbackReason = mxCreateString("ext_mode");
  mxArray *hiddenFallbackType = mxCreateString("");
  mxArray *hiddenFallbackReason = mxCreateString("");
  mxArray *incompatibleSymbol = mxCreateString("");
  mxSetField(mxInfo, 0, infoFields[0], fallbackType);
  mxSetField(mxInfo, 0, infoFields[1], fallbackReason);
  mxSetField(mxInfo, 0, infoFields[2], hiddenFallbackType);
  mxSetField(mxInfo, 0, infoFields[3], hiddenFallbackReason);
  mxSetField(mxInfo, 0, infoFields[4], incompatibleSymbol);
  return mxInfo;
}

mxArray *sf_c2_globalfunctionforBigrobotBisTestAudio_updateBuildInfo_args_info
  (void)
{
  mxArray *mxBIArgs = mxCreateCellMatrix(1,0);
  return mxBIArgs;
}

mxArray* sf_c2_globalfunctionforBigrobotBisTestAudio_get_post_codegen_info(void)
{
  const char* fieldNames[] = { "exportedFunctionsUsedByThisChart",
    "exportedFunctionsChecksum" };

  mwSize dims[2] = { 1, 1 };

  mxArray* mxPostCodegenInfo = mxCreateStructArray(2, dims, sizeof(fieldNames)/
    sizeof(fieldNames[0]), fieldNames);

  {
    mxArray* mxExportedFunctionsChecksum = mxCreateString("");
    mwSize exp_dims[2] = { 0, 1 };

    mxArray* mxExportedFunctionsUsedByThisChart = mxCreateCellArray(2, exp_dims);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsUsedByThisChart",
               mxExportedFunctionsUsedByThisChart);
    mxSetField(mxPostCodegenInfo, 0, "exportedFunctionsChecksum",
               mxExportedFunctionsChecksum);
  }

  return mxPostCodegenInfo;
}

static const mxArray
  *sf_get_sim_state_info_c2_globalfunctionforBigrobotBisTestAudio(void)
{
  const char *infoFields[] = { "chartChecksum", "varInfo" };

  mxArray *mxInfo = mxCreateStructMatrix(1, 1, 2, infoFields);
  const char *infoEncStr[] = {
    "100 S1x10'type','srcId','name','auxInfo'{{M[1],M[459],T\"Sel0\",},{M[1],M[457],T\"Sel1\",},{M[1],M[458],T\"Sel2\",},{M[1],M[5],T\"direction\",},{M[1],M[32],T\"directionD\",},{M[1],M[114],T\"reset\",},{M[1],M[495],T\"sound\",},{M[1],M[105],T\"speed\",},{M[1],M[106],T\"speed2\",},{M[3],M[113],T\"P\",}}",
    "100 S1x10'type','srcId','name','auxInfo'{{M[3],M[111],T\"Q\",},{M[3],M[46],T\"Stop\",},{M[3],M[52],T\"counter\",},{M[8],M[0],T\"is_active_c2_globalfunctionforBigrobotBisTestAudio\",},{M[9],M[0],T\"is_c2_globalfunctionforBigrobotBisTestAudio\",},{M[9],M[33],T\"is_Square\",},{M[9],M[51],T\"is_Triangle1\",},{M[9],M[69],T\"is_StraightLine\",},{M[9],M[78],T\"is_Circle\",},{M[9],M[115],T\"is_GeometricalShape\",}}",
    "100 S1x5'type','srcId','name','auxInfo'{{M[9],M[298],T\"is_Joystick\",},{M[9],M[390],T\"is_ObstacleAvoidance\",},{M[9],M[403],T\"is_LINE_FOLLOWER_IN\",},{M[11],M[0],T\"temporalCounter_i1\",S'et','os','ct'{{T\"at\",M1x6[490 488 487 493 494 382],M[1]}}},{M[15],M[0],T\"dataWrittenToVector\",}}"
  };

  mxArray *mxVarInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 25, 10);
  mxArray *mxChecksum = mxCreateDoubleMatrix(1, 4, mxREAL);
  sf_c2_globalfunctionforBigrobotBisTestAudio_get_check_sum(&mxChecksum);
  mxSetField(mxInfo, 0, infoFields[0], mxChecksum);
  mxSetField(mxInfo, 0, infoFields[1], mxVarInfo);
  return mxInfo;
}

static const mxArray* sf_opaque_get_hover_data_for_msg(void* chartInstance,
  int32_T msgSSID)
{
  return sf_get_hover_data_for_msg
    ( (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *) chartInstance,
     msgSSID);
}

static void chart_debug_initialization(SimStruct *S, unsigned int
  fullDebuggerInitialization)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance =
      (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S) && fullDebuggerInitialization==1) {
      /* do this only if simulation is starting */
      {
        unsigned int chartAlreadyPresent;
        chartAlreadyPresent = sf_debug_initialize_chart
          (sfGlobalDebugInstanceStruct,
           _globalfunctionforBigrobotBisTestAudioMachineNumber_,
           2,
           42,
           75,
           0,
           28,
           0,
           0,
           0,
           0,
           0,
           &chartInstance->chartNumber,
           &chartInstance->instanceNumber,
           (void *)S);

        /* Each instance must initialize its own list of scripts */
        init_script_number_translation
          (_globalfunctionforBigrobotBisTestAudioMachineNumber_,
           chartInstance->chartNumber,chartInstance->instanceNumber);
        if (chartAlreadyPresent==0) {
          /* this is the first instance */
          sf_debug_set_chart_disable_implicit_casting
            (sfGlobalDebugInstanceStruct,
             _globalfunctionforBigrobotBisTestAudioMachineNumber_,
             chartInstance->chartNumber,1);
          sf_debug_set_chart_event_thresholds(sfGlobalDebugInstanceStruct,
            _globalfunctionforBigrobotBisTestAudioMachineNumber_,
            chartInstance->chartNumber,
            0,
            0,
            0);
          _SFD_SET_DATA_PROPS(0,0,0,0,"counter");
          _SFD_SET_DATA_PROPS(1,0,0,0,"P");
          _SFD_SET_DATA_PROPS(2,0,0,0,"Q");
          _SFD_SET_DATA_PROPS(3,0,0,0,"Stop");
          _SFD_SET_DATA_PROPS(4,1,1,0,"side");
          _SFD_SET_DATA_PROPS(5,1,1,0,"C");
          _SFD_SET_DATA_PROPS(6,1,1,0,"d");
          _SFD_SET_DATA_PROPS(7,1,1,0,"Up");
          _SFD_SET_DATA_PROPS(8,1,1,0,"Right");
          _SFD_SET_DATA_PROPS(9,1,1,0,"Down");
          _SFD_SET_DATA_PROPS(10,1,1,0,"Left");
          _SFD_SET_DATA_PROPS(11,1,1,0,"Middle");
          _SFD_SET_DATA_PROPS(12,1,1,0,"LeftSensor");
          _SFD_SET_DATA_PROPS(13,1,1,0,"RightSensor");
          _SFD_SET_DATA_PROPS(14,1,1,0,"NE");
          _SFD_SET_DATA_PROPS(15,1,1,0,"E");
          _SFD_SET_DATA_PROPS(16,1,1,0,"NO");
          _SFD_SET_DATA_PROPS(17,1,1,0,"O");
          _SFD_SET_DATA_PROPS(18,1,1,0,"N");
          _SFD_SET_DATA_PROPS(19,2,0,1,"direction");
          _SFD_SET_DATA_PROPS(20,2,0,1,"directionD");
          _SFD_SET_DATA_PROPS(21,2,0,1,"speed");
          _SFD_SET_DATA_PROPS(22,2,0,1,"speed2");
          _SFD_SET_DATA_PROPS(23,2,0,1,"reset");
          _SFD_SET_DATA_PROPS(24,2,0,1,"Sel1");
          _SFD_SET_DATA_PROPS(25,2,0,1,"Sel2");
          _SFD_SET_DATA_PROPS(26,2,0,1,"Sel0");
          _SFD_SET_DATA_PROPS(27,2,0,1,"sound");
          _SFD_STATE_INFO(0,0,0);
          _SFD_STATE_INFO(1,0,0);
          _SFD_STATE_INFO(2,0,0);
          _SFD_STATE_INFO(3,0,0);
          _SFD_STATE_INFO(4,0,0);
          _SFD_STATE_INFO(5,0,0);
          _SFD_STATE_INFO(6,0,0);
          _SFD_STATE_INFO(7,0,0);
          _SFD_STATE_INFO(8,0,0);
          _SFD_STATE_INFO(9,0,0);
          _SFD_STATE_INFO(10,0,0);
          _SFD_STATE_INFO(11,0,0);
          _SFD_STATE_INFO(12,0,0);
          _SFD_STATE_INFO(13,0,0);
          _SFD_STATE_INFO(14,0,0);
          _SFD_STATE_INFO(15,0,0);
          _SFD_STATE_INFO(16,0,0);
          _SFD_STATE_INFO(17,0,0);
          _SFD_STATE_INFO(18,0,0);
          _SFD_STATE_INFO(19,0,0);
          _SFD_STATE_INFO(20,0,0);
          _SFD_STATE_INFO(21,0,0);
          _SFD_STATE_INFO(22,0,0);
          _SFD_STATE_INFO(23,0,0);
          _SFD_STATE_INFO(24,0,0);
          _SFD_STATE_INFO(25,0,0);
          _SFD_STATE_INFO(26,0,0);
          _SFD_STATE_INFO(27,0,0);
          _SFD_STATE_INFO(28,0,0);
          _SFD_STATE_INFO(29,0,0);
          _SFD_STATE_INFO(30,0,0);
          _SFD_STATE_INFO(31,0,0);
          _SFD_STATE_INFO(32,0,0);
          _SFD_STATE_INFO(33,0,0);
          _SFD_STATE_INFO(34,0,0);
          _SFD_STATE_INFO(35,0,0);
          _SFD_STATE_INFO(36,0,0);
          _SFD_STATE_INFO(37,0,0);
          _SFD_STATE_INFO(38,0,0);
          _SFD_STATE_INFO(39,0,0);
          _SFD_STATE_INFO(40,0,0);
          _SFD_STATE_INFO(41,0,0);
          _SFD_CH_SUBSTATE_COUNT(6);
          _SFD_CH_SUBSTATE_DECOMP(0);
          _SFD_CH_SUBSTATE_INDEX(0,0);
          _SFD_CH_SUBSTATE_INDEX(1,24);
          _SFD_CH_SUBSTATE_INDEX(2,30);
          _SFD_CH_SUBSTATE_INDEX(3,34);
          _SFD_CH_SUBSTATE_INDEX(4,40);
          _SFD_CH_SUBSTATE_INDEX(5,41);
          _SFD_ST_SUBSTATE_COUNT(0,5);
          _SFD_ST_SUBSTATE_INDEX(0,0,1);
          _SFD_ST_SUBSTATE_INDEX(0,1,4);
          _SFD_ST_SUBSTATE_INDEX(0,2,13);
          _SFD_ST_SUBSTATE_INDEX(0,3,14);
          _SFD_ST_SUBSTATE_INDEX(0,4,17);
          _SFD_ST_SUBSTATE_COUNT(1,2);
          _SFD_ST_SUBSTATE_INDEX(1,0,2);
          _SFD_ST_SUBSTATE_INDEX(1,1,3);
          _SFD_ST_SUBSTATE_COUNT(2,0);
          _SFD_ST_SUBSTATE_COUNT(3,0);
          _SFD_ST_SUBSTATE_COUNT(4,8);
          _SFD_ST_SUBSTATE_INDEX(4,0,5);
          _SFD_ST_SUBSTATE_INDEX(4,1,6);
          _SFD_ST_SUBSTATE_INDEX(4,2,7);
          _SFD_ST_SUBSTATE_INDEX(4,3,8);
          _SFD_ST_SUBSTATE_INDEX(4,4,9);
          _SFD_ST_SUBSTATE_INDEX(4,5,10);
          _SFD_ST_SUBSTATE_INDEX(4,6,11);
          _SFD_ST_SUBSTATE_INDEX(4,7,12);
          _SFD_ST_SUBSTATE_COUNT(5,0);
          _SFD_ST_SUBSTATE_COUNT(6,0);
          _SFD_ST_SUBSTATE_COUNT(7,0);
          _SFD_ST_SUBSTATE_COUNT(8,0);
          _SFD_ST_SUBSTATE_COUNT(9,0);
          _SFD_ST_SUBSTATE_COUNT(10,0);
          _SFD_ST_SUBSTATE_COUNT(11,0);
          _SFD_ST_SUBSTATE_COUNT(12,0);
          _SFD_ST_SUBSTATE_COUNT(13,0);
          _SFD_ST_SUBSTATE_COUNT(14,2);
          _SFD_ST_SUBSTATE_INDEX(14,0,15);
          _SFD_ST_SUBSTATE_INDEX(14,1,16);
          _SFD_ST_SUBSTATE_COUNT(15,0);
          _SFD_ST_SUBSTATE_COUNT(16,0);
          _SFD_ST_SUBSTATE_COUNT(17,6);
          _SFD_ST_SUBSTATE_INDEX(17,0,18);
          _SFD_ST_SUBSTATE_INDEX(17,1,19);
          _SFD_ST_SUBSTATE_INDEX(17,2,20);
          _SFD_ST_SUBSTATE_INDEX(17,3,21);
          _SFD_ST_SUBSTATE_INDEX(17,4,22);
          _SFD_ST_SUBSTATE_INDEX(17,5,23);
          _SFD_ST_SUBSTATE_COUNT(18,0);
          _SFD_ST_SUBSTATE_COUNT(19,0);
          _SFD_ST_SUBSTATE_COUNT(20,0);
          _SFD_ST_SUBSTATE_COUNT(21,0);
          _SFD_ST_SUBSTATE_COUNT(22,0);
          _SFD_ST_SUBSTATE_COUNT(23,0);
          _SFD_ST_SUBSTATE_COUNT(24,5);
          _SFD_ST_SUBSTATE_INDEX(24,0,25);
          _SFD_ST_SUBSTATE_INDEX(24,1,26);
          _SFD_ST_SUBSTATE_INDEX(24,2,27);
          _SFD_ST_SUBSTATE_INDEX(24,3,28);
          _SFD_ST_SUBSTATE_INDEX(24,4,29);
          _SFD_ST_SUBSTATE_COUNT(25,0);
          _SFD_ST_SUBSTATE_COUNT(26,0);
          _SFD_ST_SUBSTATE_COUNT(27,0);
          _SFD_ST_SUBSTATE_COUNT(28,0);
          _SFD_ST_SUBSTATE_COUNT(29,0);
          _SFD_ST_SUBSTATE_COUNT(30,3);
          _SFD_ST_SUBSTATE_INDEX(30,0,31);
          _SFD_ST_SUBSTATE_INDEX(30,1,32);
          _SFD_ST_SUBSTATE_INDEX(30,2,33);
          _SFD_ST_SUBSTATE_COUNT(31,0);
          _SFD_ST_SUBSTATE_COUNT(32,0);
          _SFD_ST_SUBSTATE_COUNT(33,0);
          _SFD_ST_SUBSTATE_COUNT(34,5);
          _SFD_ST_SUBSTATE_INDEX(34,0,35);
          _SFD_ST_SUBSTATE_INDEX(34,1,36);
          _SFD_ST_SUBSTATE_INDEX(34,2,37);
          _SFD_ST_SUBSTATE_INDEX(34,3,38);
          _SFD_ST_SUBSTATE_INDEX(34,4,39);
          _SFD_ST_SUBSTATE_COUNT(35,0);
          _SFD_ST_SUBSTATE_COUNT(36,0);
          _SFD_ST_SUBSTATE_COUNT(37,0);
          _SFD_ST_SUBSTATE_COUNT(38,0);
          _SFD_ST_SUBSTATE_COUNT(39,0);
          _SFD_ST_SUBSTATE_COUNT(40,0);
          _SFD_ST_SUBSTATE_COUNT(41,0);
        }

        _SFD_CV_INIT_CHART(6,1,0,0);

        {
          _SFD_CV_INIT_STATE(0,5,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(1,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(2,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(3,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(4,8,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(5,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(6,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(7,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(8,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(9,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(10,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(11,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(12,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(13,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(14,2,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(15,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(16,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(17,6,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(18,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(19,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(20,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(21,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(22,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(23,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(24,5,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(25,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(26,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(27,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(28,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(29,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(30,3,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(31,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(32,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(33,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(34,5,1,1,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(35,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(36,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(37,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(38,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(39,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(40,0,0,0,0,0,NULL,NULL);
        }

        {
          _SFD_CV_INIT_STATE(41,0,0,0,0,0,NULL,NULL);
        }

        _SFD_CV_INIT_TRANS(57,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(56,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(31,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(54,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(32,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(52,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(55,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(51,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(50,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(53,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(60,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(58,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(59,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(61,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(64,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(62,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(63,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(27,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(9,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(23,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(8,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(21,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(22,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(26,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(20,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(24,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(28,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(30,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(25,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(29,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(10,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(11,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(12,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(13,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(14,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(15,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(16,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(17,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(3,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(7,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(1,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(0,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(6,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(4,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(5,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(2,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(18,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(19,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(69,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(68,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(66,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(67,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(70,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(71,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(73,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(74,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(65,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(72,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(43,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(38,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(39,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(44,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(42,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(34,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(36,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(33,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(45,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(40,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(47,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(46,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(48,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(49,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(41,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(35,0,NULL,NULL,0,NULL);
        _SFD_CV_INIT_TRANS(37,0,NULL,NULL,0,NULL);

        /* Initialization of MATLAB Function Model Coverage */
        _SFD_CV_INIT_EML(30,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(31,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(32,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(33,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(0,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(17,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(18,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(21,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(23,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(19,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(22,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(20,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(14,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(16,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(15,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(4,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(12,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(5,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(9,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(8,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(6,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(11,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(10,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(7,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(13,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(1,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(3,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(2,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(39,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(37,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(38,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(35,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(36,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(40,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(24,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(29,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(26,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(28,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(25,1,0,0,0,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML(58,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(58,0,0,1,32,1,32);

        {
          static int condStart[] = { 1, 19 };

          static int condEnd[] = { 15, 32 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(58,0,0,1,32,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(58,0,0,1,15,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(58,0,1,19,32,-1,0);
        _SFD_CV_INIT_EML(59,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(59,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(59,0,0,1,14,-1,0);
        _SFD_CV_INIT_EML(61,0,0,0,1,0,0,0,0,0,2,1);
        _SFD_CV_INIT_EML_IF(61,0,0,1,32,1,32);

        {
          static int condStart[] = { 1, 19 };

          static int condEnd[] = { 15, 32 };

          static int pfixExpr[] = { 0, 1, -3 };

          _SFD_CV_INIT_EML_MCDC(61,0,0,1,32,2,0,&(condStart[0]),&(condEnd[0]),3,
                                &(pfixExpr[0]));
        }

        _SFD_CV_INIT_EML_RELATIONAL(61,0,0,1,15,-1,0);
        _SFD_CV_INIT_EML_RELATIONAL(61,0,1,19,32,-1,0);
        _SFD_CV_INIT_EML(64,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(64,0,0,1,15,1,15);
        _SFD_CV_INIT_EML_RELATIONAL(64,0,0,1,15,-1,0);
        _SFD_CV_INIT_EML(62,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(62,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(62,0,0,1,14,-1,0);
        _SFD_CV_INIT_EML(63,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(63,0,0,1,15,1,15);
        _SFD_CV_INIT_EML_RELATIONAL(63,0,0,1,15,-1,0);
        _SFD_CV_INIT_EML(11,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(11,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(11,0,0,1,8,-1,5);
        _SFD_CV_INIT_EML(12,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(12,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(12,0,0,1,10,-1,5);
        _SFD_CV_INIT_EML(13,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(13,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(13,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(14,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(14,0,0,1,12,1,12);
        _SFD_CV_INIT_EML_RELATIONAL(14,0,0,1,12,-1,5);
        _SFD_CV_INIT_EML(15,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(15,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(15,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(17,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(17,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(17,0,0,1,8,-1,5);
        _SFD_CV_INIT_EML(7,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(7,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(7,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(1,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(1,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(1,0,0,1,8,-1,5);
        _SFD_CV_INIT_EML(0,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(0,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(0,0,0,1,10,-1,5);
        _SFD_CV_INIT_EML(6,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(6,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(6,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(4,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(4,0,0,1,12,1,12);
        _SFD_CV_INIT_EML_RELATIONAL(4,0,0,1,12,-1,5);
        _SFD_CV_INIT_EML(5,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(5,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(5,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(2,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(2,0,0,1,14,1,14);
        _SFD_CV_INIT_EML_RELATIONAL(2,0,0,1,14,-1,5);
        _SFD_CV_INIT_EML(19,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(19,0,0,1,21,1,21);
        _SFD_CV_INIT_EML_RELATIONAL(19,0,0,1,21,-1,5);
        _SFD_CV_INIT_EML(27,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(27,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(27,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(9,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(9,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(9,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(23,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(23,0,0,1,6,1,6);
        _SFD_CV_INIT_EML_RELATIONAL(23,0,0,1,6,-1,0);
        _SFD_CV_INIT_EML(21,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(21,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(21,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(22,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(22,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(22,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(26,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(26,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(26,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(20,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(20,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(20,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(24,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(24,0,0,1,9,1,9);
        _SFD_CV_INIT_EML_RELATIONAL(24,0,0,1,9,-1,0);
        _SFD_CV_INIT_EML(28,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(28,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(28,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(30,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(30,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(30,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(25,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(25,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(25,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(29,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(29,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(29,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(69,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(69,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(69,0,0,1,5,-1,0);
        _SFD_CV_INIT_EML(68,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(68,0,0,1,6,1,6);
        _SFD_CV_INIT_EML_RELATIONAL(68,0,0,1,6,-1,0);
        _SFD_CV_INIT_EML(66,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(66,0,0,0,12,0,12);
        _SFD_CV_INIT_EML(67,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(67,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(67,0,0,1,5,-1,0);
        _SFD_CV_INIT_EML(70,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(70,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(70,0,0,1,5,-1,0);
        _SFD_CV_INIT_EML(71,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(71,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(73,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(73,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(65,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(65,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(72,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(72,0,0,0,14,0,14);
        _SFD_CV_INIT_EML(43,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(43,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(43,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(38,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(38,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(38,0,0,1,8,-1,4);
        _SFD_CV_INIT_EML(39,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(39,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(39,0,0,1,5,-1,4);
        _SFD_CV_INIT_EML(44,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(44,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(44,0,0,1,5,-1,4);
        _SFD_CV_INIT_EML(42,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(42,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(42,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(34,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(34,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(34,0,0,1,5,-1,4);
        _SFD_CV_INIT_EML(36,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(36,0,0,1,5,1,5);
        _SFD_CV_INIT_EML_RELATIONAL(36,0,0,1,5,-1,4);
        _SFD_CV_INIT_EML(45,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(45,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(45,0,0,1,8,-1,4);
        _SFD_CV_INIT_EML(40,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(40,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(40,0,0,1,8,-1,4);
        _SFD_CV_INIT_EML(47,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(47,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(47,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(46,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(46,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(46,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(48,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(48,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(48,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(49,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(49,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(49,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(41,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(41,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(41,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(35,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(35,0,0,1,7,1,7);
        _SFD_CV_INIT_EML_RELATIONAL(35,0,0,1,7,-1,4);
        _SFD_CV_INIT_EML(37,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(37,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(37,0,0,1,8,-1,4);
        _SFD_CV_INIT_EML(57,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(57,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(57,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(56,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(56,0,0,1,6,1,6);
        _SFD_CV_INIT_EML_RELATIONAL(56,0,0,1,6,-1,0);
        _SFD_CV_INIT_EML(54,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(54,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(54,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(32,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(32,0,0,1,9,1,9);
        _SFD_CV_INIT_EML_RELATIONAL(32,0,0,1,9,-1,0);
        _SFD_CV_INIT_EML(52,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(52,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(52,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(55,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(55,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(55,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(51,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(51,0,0,1,8,1,8);
        _SFD_CV_INIT_EML_RELATIONAL(51,0,0,1,8,-1,0);
        _SFD_CV_INIT_EML(50,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(50,0,0,1,10,1,10);
        _SFD_CV_INIT_EML_RELATIONAL(50,0,0,1,10,-1,0);
        _SFD_CV_INIT_EML(53,0,0,0,1,0,0,0,0,0,0,0);
        _SFD_CV_INIT_EML_IF(53,0,0,0,15,0,15);
        _SFD_SET_DATA_COMPILED_PROPS(0,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(1,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(2,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(3,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(4,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(5,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(6,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(7,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(8,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(9,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(10,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(11,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(12,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(13,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(14,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(15,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(16,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(17,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(18,SF_UINT8,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_b_sf_marshallOut,(MexInFcnForType)NULL);
        _SFD_SET_DATA_COMPILED_PROPS(19,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(20,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(21,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(22,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(23,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(24,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(25,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(26,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
        _SFD_SET_DATA_COMPILED_PROPS(27,SF_DOUBLE,0,NULL,0,0,0,0.0,1.0,0,0,
          (MexFcnForType)c2_sf_marshallOut,(MexInFcnForType)c2_sf_marshallIn);
      }
    } else {
      sf_debug_reset_current_state_configuration(sfGlobalDebugInstanceStruct,
        _globalfunctionforBigrobotBisTestAudioMachineNumber_,
        chartInstance->chartNumber,chartInstance->instanceNumber);
    }
  }
}

static void chart_debug_initialize_data_addresses(SimStruct *S)
{
  if (!sim_mode_is_rtw_gen(S)) {
    SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance =
      (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
      sf_get_chart_instance_ptr(S);
    if (ssIsFirstInitCond(S)) {
      /* do this only if simulation is starting and after we know the addresses of all data */
      {
        _SFD_SET_DATA_VALUE_PTR(19U, (void *)chartInstance->c2_direction);
        _SFD_SET_DATA_VALUE_PTR(4U, (void *)chartInstance->c2_side);
        _SFD_SET_DATA_VALUE_PTR(5U, (void *)chartInstance->c2_C);
        _SFD_SET_DATA_VALUE_PTR(6U, (void *)chartInstance->c2_d);
        _SFD_SET_DATA_VALUE_PTR(20U, (void *)chartInstance->c2_directionD);
        _SFD_SET_DATA_VALUE_PTR(7U, (void *)chartInstance->c2_Up);
        _SFD_SET_DATA_VALUE_PTR(8U, (void *)chartInstance->c2_Right);
        _SFD_SET_DATA_VALUE_PTR(9U, (void *)chartInstance->c2_Down);
        _SFD_SET_DATA_VALUE_PTR(10U, (void *)chartInstance->c2_Left);
        _SFD_SET_DATA_VALUE_PTR(11U, (void *)chartInstance->c2_Middle);
        _SFD_SET_DATA_VALUE_PTR(21U, (void *)chartInstance->c2_speed);
        _SFD_SET_DATA_VALUE_PTR(22U, (void *)chartInstance->c2_speed2);
        _SFD_SET_DATA_VALUE_PTR(3U, (void *)chartInstance->c2_Stop);
        _SFD_SET_DATA_VALUE_PTR(2U, (void *)chartInstance->c2_Q);
        _SFD_SET_DATA_VALUE_PTR(1U, (void *)chartInstance->c2_P);
        _SFD_SET_DATA_VALUE_PTR(23U, (void *)chartInstance->c2_reset);
        _SFD_SET_DATA_VALUE_PTR(12U, (void *)chartInstance->c2_LeftSensor);
        _SFD_SET_DATA_VALUE_PTR(13U, (void *)chartInstance->c2_RightSensor);
        _SFD_SET_DATA_VALUE_PTR(24U, (void *)chartInstance->c2_Sel1);
        _SFD_SET_DATA_VALUE_PTR(25U, (void *)chartInstance->c2_Sel2);
        _SFD_SET_DATA_VALUE_PTR(26U, (void *)chartInstance->c2_Sel0);
        _SFD_SET_DATA_VALUE_PTR(14U, (void *)chartInstance->c2_NE);
        _SFD_SET_DATA_VALUE_PTR(15U, (void *)chartInstance->c2_E);
        _SFD_SET_DATA_VALUE_PTR(16U, (void *)chartInstance->c2_NO);
        _SFD_SET_DATA_VALUE_PTR(17U, (void *)chartInstance->c2_O);
        _SFD_SET_DATA_VALUE_PTR(18U, (void *)chartInstance->c2_N);
        _SFD_SET_DATA_VALUE_PTR(27U, (void *)chartInstance->c2_sound);
        _SFD_SET_DATA_VALUE_PTR(0U, (void *)chartInstance->c2_counter);
      }
    }
  }
}

static const char* sf_get_instance_specialization(void)
{
  return "srGuuxAXSI3FPCKa05HkA8";
}

static void sf_check_dwork_consistency(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    const uint32_T *sfunDWorkChecksum = sf_get_sfun_dwork_checksum();
    mxArray *infoStruct =
      load_globalfunctionforBigrobotBisTestAudio_optimization_info
      (sim_mode_is_rtw_gen(S), sim_mode_is_modelref_sim(S), sim_mode_is_external
       (S));
    mxArray* mxRTWDWorkChecksum = sf_get_dwork_info_from_mat_file(S,
      sf_get_instance_specialization(), infoStruct, 2, "dworkChecksum");
    if (mxRTWDWorkChecksum != NULL) {
      double *pr = mxGetPr(mxRTWDWorkChecksum);
      if ((uint32_T)pr[0] != sfunDWorkChecksum[0] ||
          (uint32_T)pr[1] != sfunDWorkChecksum[1] ||
          (uint32_T)pr[2] != sfunDWorkChecksum[2] ||
          (uint32_T)pr[3] != sfunDWorkChecksum[3]) {
        sf_mex_error_message("Code generation and simulation targets registered different sets of persistent variables for the block. "
                             "External or Rapid Accelerator mode simulation requires code generation and simulation targets to "
                             "register the same set of persistent variables for this block. "
                             "This discrepancy is typically caused by MATLAB functions that have different code paths for "
                             "simulation and code generation targets where these code paths define different sets of persistent variables. "
                             "Please identify these code paths in the offending block and rewrite the MATLAB code so that "
                             "the set of persistent variables is the same between simulation and code generation.");
      }
    }
  }
}

static void sf_opaque_initialize_c2_globalfunctionforBigrobotBisTestAudio(void
  *chartInstanceVar)
{
  sf_check_dwork_consistency
    (((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
      chartInstanceVar)->S);
  chart_debug_initialization
    (((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
      chartInstanceVar)->S,0);
  initialize_params_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
  initialize_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

static void sf_opaque_enable_c2_globalfunctionforBigrobotBisTestAudio(void
  *chartInstanceVar)
{
  enable_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

static void sf_opaque_disable_c2_globalfunctionforBigrobotBisTestAudio(void
  *chartInstanceVar)
{
  disable_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

static void sf_opaque_gateway_c2_globalfunctionforBigrobotBisTestAudio(void
  *chartInstanceVar)
{
  sf_gateway_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

static void sf_opaque_ext_mode_exec_c2_globalfunctionforBigrobotBisTestAudio
  (void *chartInstanceVar)
{
  ext_mode_exec_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

static const mxArray*
  sf_opaque_get_sim_state_c2_globalfunctionforBigrobotBisTestAudio(SimStruct* S)
{
  return get_sim_state_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
     sf_get_chart_instance_ptr(S));    /* raw sim ctx */
}

static void sf_opaque_set_sim_state_c2_globalfunctionforBigrobotBisTestAudio
  (SimStruct* S, const mxArray *st)
{
  set_sim_state_c2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     sf_get_chart_instance_ptr(S), st);
}

static void sf_opaque_terminate_c2_globalfunctionforBigrobotBisTestAudio(void
  *chartInstanceVar)
{
  if (chartInstanceVar!=NULL) {
    SimStruct *S = ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
                    chartInstanceVar)->S;
    if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
      sf_clear_rtw_identifier(S);
      unload_globalfunctionforBigrobotBisTestAudio_optimization_info();
    }

    finalize_c2_globalfunctionforBigrobotBisTestAudio
      ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
       chartInstanceVar);
    utFree(chartInstanceVar);
    if (ssGetUserData(S)!= NULL) {
      sf_free_ChartRunTimeInfo(S);
    }

    ssSetUserData(S,NULL);
  }
}

static void sf_opaque_init_subchart_simstructs(void *chartInstanceVar)
{
  initSimStructsc2_globalfunctionforBigrobotBisTestAudio
    ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
     chartInstanceVar);
}

extern unsigned int sf_machine_global_initializer_called(void);
static void mdlProcessParameters_c2_globalfunctionforBigrobotBisTestAudio
  (SimStruct *S)
{
  int i;
  for (i=0;i<ssGetNumRunTimeParams(S);i++) {
    if (ssGetSFcnParamTunable(S,i)) {
      ssUpdateDlgParamAsRunTimeParam(S,i);
    }
  }

  if (sf_machine_global_initializer_called()) {
    initialize_params_c2_globalfunctionforBigrobotBisTestAudio
      ((SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct*)
       sf_get_chart_instance_ptr(S));
  }
}

mxArray *sf_c2_globalfunctionforBigrobotBisTestAudio_get_testpoint_info(void)
{
  const char *infoEncStr[] = {
    "100 S1x10'varName','path'{{T\"is_active_c2_globalfunctionforBigrobotBisTestAudio\",T\"is_active_c2_globalfunctionforBigrobotBisTestAudio\"},{T\"is_c2_globalfunctionforBigrobotBisTestAudio\",T\"is_c2_globalfunctionforBigrobotBisTestAudio\"},{T\"is_LINE_FOLLOWER_IN\",T\"LINE_FOLLOWER_IN.is_LINE_FOLLOWER_IN\"},{T\"is_GeometricalShape\",T\"GeometricalShape.is_GeometricalShape\"},{T\"is_Triangle1\",T\"GeometricalShape.Triangle1.is_Triangle1\"},{T\"is_StraightLine\",T\"GeometricalShape.StraightLine.is_StraightLine\"},{T\"is_Square\",T\"GeometricalShape.Square.is_Square\"},{T\"is_Circle\",T\"GeometricalShape.Circle.is_Circle\"},{T\"is_ObstacleAvoidance\",T\"ObstacleAvoidance.is_ObstacleAvoidance\"},{T\"is_Joystick\",T\"Joystick.is_Joystick\"}}"
  };

  mxArray *mxTpInfo = sf_mex_decode_encoded_mx_struct_array(infoEncStr, 10, 10);
  return mxTpInfo;
}

static void sf_set_sfun_dwork_info(SimStruct *S)
{
  const char *dworkEncStr[] = {
    "100 S1x10'type','isSigned','wordLength','bias','slope','exponent','isScaledDouble','isComplex','size','local_queue_dwork_idx','local_queue_id'{{T\"int32\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]}}",
    "100 S1x6'type','isSigned','wordLength','bias','slope','exponent','isScaledDouble','isComplex','size','local_queue_dwork_idx','local_queue_id'{{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"double\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"double\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"double\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"double\",,,,,,,M[0],M[],M[-1],M[-1]},{T\"uint8\",,,,,,,M[0],M[],M[-1],M[-1]}}"
  };

  sf_set_encoded_dwork_info(S, dworkEncStr, 16, 10);
}

static uint32_T* sf_get_sfun_dwork_checksum()
{
  static uint32_T checksum[4] = { 1195615598U, 187701355U, 4237396602U,
    4169340767U };

  return checksum;
}

static void mdlSetWorkWidths_c2_globalfunctionforBigrobotBisTestAudio(SimStruct *
  S)
{
  ssSetStatesModifiedOnlyInUpdate(S, 0);
  ssMdlUpdateIsEmpty(S, 1);
  ssSetModelReferenceSampleTimeDisallowInheritance(S);
  if (sim_mode_is_rtw_gen(S) || sim_mode_is_external(S)) {
    mxArray *infoStruct =
      load_globalfunctionforBigrobotBisTestAudio_optimization_info
      (sim_mode_is_rtw_gen(S), sim_mode_is_modelref_sim(S), sim_mode_is_external
       (S));
    int_T chartIsInlinable =
      (int_T)sf_is_chart_inlinable(sf_get_instance_specialization(),infoStruct,2);
    ssSetStateflowIsInlinable(S,chartIsInlinable);
    ssSetRTWCG(S,1);
    ssSetNotMultipleInlinable(S,sf_rtw_info_uint_prop
      (sf_get_instance_specialization(),infoStruct,2,
       "gatewayCannotBeInlinedMultipleTimes"));
    sf_set_chart_accesses_machine_info(S, sf_get_instance_specialization(),
      infoStruct, 2);
    sf_update_buildInfo(S, sf_get_instance_specialization(),infoStruct,2);
    if (chartIsInlinable) {
      ssSetInputPortOptimOpts(S, 0, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 1, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 2, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 3, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 4, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 5, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 6, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 7, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 8, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 9, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 10, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 11, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 12, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 13, SS_REUSABLE_AND_LOCAL);
      ssSetInputPortOptimOpts(S, 14, SS_REUSABLE_AND_LOCAL);
      sf_mark_chart_expressionable_inputs(S,sf_get_instance_specialization(),
        infoStruct,2,15);
      sf_mark_chart_reusable_outputs(S,sf_get_instance_specialization(),
        infoStruct,2,9);
    }

    {
      unsigned int outPortIdx;
      for (outPortIdx=1; outPortIdx<=9; ++outPortIdx) {
        ssSetOutputPortOptimizeInIR(S, outPortIdx, 1U);
      }
    }

    {
      unsigned int inPortIdx;
      for (inPortIdx=0; inPortIdx < 15; ++inPortIdx) {
        ssSetInputPortOptimizeInIR(S, inPortIdx, 1U);
      }
    }

    sf_set_rtw_dwork_info(S,sf_get_instance_specialization(),infoStruct,2);
    sf_register_codegen_names_for_scoped_functions_defined_by_chart(S);
    ssSetHasSubFunctions(S,!(chartIsInlinable));
  } else {
    sf_set_sfun_dwork_info(S);
  }

  ssSetOptions(S,ssGetOptions(S)|SS_OPTION_WORKS_WITH_CODE_REUSE);
  ssSetChecksum0(S,(3119439881U));
  ssSetChecksum1(S,(1240828758U));
  ssSetChecksum2(S,(2046447780U));
  ssSetChecksum3(S,(4291594904U));
  ssSetmdlDerivatives(S, NULL);
  ssSetExplicitFCSSCtrl(S,1);
  ssSetStateSemanticsClassicAndSynchronous(S, true);
  ssSupportsMultipleExecInstances(S,1);
}

static void mdlRTW_c2_globalfunctionforBigrobotBisTestAudio(SimStruct *S)
{
  if (sim_mode_is_rtw_gen(S)) {
    ssWriteRTWStrParam(S, "StateflowChartType", "Stateflow");
  }
}

static void mdlStart_c2_globalfunctionforBigrobotBisTestAudio(SimStruct *S)
{
  SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *chartInstance;
  chartInstance = (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct *)
    utMalloc(sizeof(SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct));
  if (chartInstance==NULL) {
    sf_mex_error_message("Could not allocate memory for chart instance.");
  }

  memset(chartInstance, 0, sizeof
         (SFc2_globalfunctionforBigrobotBisTestAudioInstanceStruct));
  chartInstance->chartInfo.chartInstance = chartInstance;
  chartInstance->chartInfo.isEMLChart = 0;
  chartInstance->chartInfo.chartInitialized = 0;
  chartInstance->chartInfo.sFunctionGateway =
    sf_opaque_gateway_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.initializeChart =
    sf_opaque_initialize_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.terminateChart =
    sf_opaque_terminate_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.enableChart =
    sf_opaque_enable_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.disableChart =
    sf_opaque_disable_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.getSimState =
    sf_opaque_get_sim_state_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.setSimState =
    sf_opaque_set_sim_state_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.getSimStateInfo =
    sf_get_sim_state_info_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.zeroCrossings = NULL;
  chartInstance->chartInfo.outputs = NULL;
  chartInstance->chartInfo.derivatives = NULL;
  chartInstance->chartInfo.mdlRTW =
    mdlRTW_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.mdlStart =
    mdlStart_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.mdlSetWorkWidths =
    mdlSetWorkWidths_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.callGetHoverDataForMsg =
    sf_opaque_get_hover_data_for_msg;
  chartInstance->chartInfo.extModeExec =
    sf_opaque_ext_mode_exec_c2_globalfunctionforBigrobotBisTestAudio;
  chartInstance->chartInfo.restoreLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.restoreBeforeLastMajorStepConfiguration = NULL;
  chartInstance->chartInfo.storeCurrentConfiguration = NULL;
  chartInstance->chartInfo.callAtomicSubchartUserFcn = NULL;
  chartInstance->chartInfo.callAtomicSubchartAutoFcn = NULL;
  chartInstance->chartInfo.debugInstance = sfGlobalDebugInstanceStruct;
  chartInstance->S = S;
  sf_init_ChartRunTimeInfo(S, &(chartInstance->chartInfo), false, 0);
  init_dsm_address_info(chartInstance);
  init_simulink_io_address(chartInstance);
  if (!sim_mode_is_rtw_gen(S)) {
  }

  chart_debug_initialization(S,1);
  mdl_start_c2_globalfunctionforBigrobotBisTestAudio(chartInstance);
}

void c2_globalfunctionforBigrobotBisTestAudio_method_dispatcher(SimStruct *S,
  int_T method, void *data)
{
  switch (method) {
   case SS_CALL_MDL_START:
    mdlStart_c2_globalfunctionforBigrobotBisTestAudio(S);
    break;

   case SS_CALL_MDL_SET_WORK_WIDTHS:
    mdlSetWorkWidths_c2_globalfunctionforBigrobotBisTestAudio(S);
    break;

   case SS_CALL_MDL_PROCESS_PARAMETERS:
    mdlProcessParameters_c2_globalfunctionforBigrobotBisTestAudio(S);
    break;

   default:
    /* Unhandled method */
    sf_mex_error_message("Stateflow Internal Error:\n"
                         "Error calling c2_globalfunctionforBigrobotBisTestAudio_method_dispatcher.\n"
                         "Can't handle method %d.\n", method);
    break;
  }
}
