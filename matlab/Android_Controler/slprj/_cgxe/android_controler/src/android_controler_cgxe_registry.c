#define S_FUNCTION_LEVEL               2
#define S_FUNCTION_NAME                android_controler_cgxe
#include "simstruc.h"
#include "android_controler_cgxe.h"
#define MDL_START

static void mdlStart(SimStruct* S)
{
  unsigned int success;
  success = cgxe_android_controler_method_dispatcher(S, SS_CALL_MDL_START, NULL);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }
}

#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlInitializeConditions method directly.\n");
}

#define MDL_UPDATE

static void mdlUpdate(SimStruct *S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlUpdate method directly.\n");
}

static void mdlOutputs(SimStruct* S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlOutputs method directly.\n");
}

static void mdlTerminate(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlTerminate method directly.\n");
}

static void mdlInitializeSizes(SimStruct *S)
{
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
}

static mxArray* cgxe_get_supported_modules(void)
{
  mxArray* mxModules = mxCreateCellMatrix(6, 1);
  mxArray* mxChksum = NULL;
  uint32_T* checksumData = NULL;
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 133849349;
  checksumData[1] = 2411175902;
  checksumData[2] = 728459727;
  checksumData[3] = 2065912730;
  mxSetCell(mxModules, 0, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 1799478551;
  checksumData[1] = 115921845;
  checksumData[2] = 2516272101;
  checksumData[3] = 3321476869;
  mxSetCell(mxModules, 1, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 2284856100;
  checksumData[1] = 2597573358;
  checksumData[2] = 298573034;
  checksumData[3] = 1337758597;
  mxSetCell(mxModules, 2, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3142023313;
  checksumData[1] = 3387156537;
  checksumData[2] = 545059890;
  checksumData[3] = 2654554250;
  mxSetCell(mxModules, 3, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3470754072;
  checksumData[1] = 2096815322;
  checksumData[2] = 477920432;
  checksumData[3] = 1894057529;
  mxSetCell(mxModules, 4, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3802960954;
  checksumData[1] = 2005578432;
  checksumData[2] = 3167107951;
  checksumData[3] = 1425291663;
  mxSetCell(mxModules, 5, mxChksum);
  return mxModules;
}

static int cgxe_process_get_checksums(int nlhs, mxArray* plhs[], int nrhs, const
  mxArray* prhs[])
{
  const char* checksumFields[] = { "modules", "model", "makefile", "target",
    "overall" };

  mxArray* mxChecksum = mxCreateStructMatrix(1, 1, 5, checksumFields);
  mxSetField(mxChecksum, 0, "modules", cgxe_get_supported_modules());

  {
    mxArray* mxModelChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxModelChksum);
    checksumData[0] = 2613279898;
    checksumData[1] = 1942028192;
    checksumData[2] = 2596023399;
    checksumData[3] = 2410470996;
    mxSetField(mxChecksum, 0, "model", mxModelChksum);
  }

  {
    mxArray* mxMakefileChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxMakefileChksum);
    checksumData[0] = 2618114212;
    checksumData[1] = 3407785403;
    checksumData[2] = 2516109712;
    checksumData[3] = 815147294;
    mxSetField(mxChecksum, 0, "makefile", mxMakefileChksum);
  }

  {
    mxArray* mxTargetChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxTargetChksum);
    checksumData[0] = 0;
    checksumData[1] = 0;
    checksumData[2] = 0;
    checksumData[3] = 0;
    mxSetField(mxChecksum, 0, "target", mxTargetChksum);
  }

  {
    mxArray* mxOverallChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxOverallChksum);
    checksumData[0] = 763467112;
    checksumData[1] = 2349089555;
    checksumData[2] = 1847733792;
    checksumData[3] = 767178135;
    mxSetField(mxChecksum, 0, "overall", mxOverallChksum);
  }

  plhs[0] = mxChecksum;
  return 1;
}

static int cgxe_mex_unlock_call(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  while (mexIsLocked()) {
    mexUnlock();
  }

  return 1;
}

static SimStruct* cgxe_unpack_simstruct(const mxArray *mxS)
{
  uint32_T *uintPtr = (uint32_T*)malloc(sizeof(SimStruct*));
  int nEl = sizeof(SimStruct*)/sizeof(uint32_T);
  uint32_T *uintDataPtr = (uint32_T *)mxGetData(mxS);
  int el;
  SimStruct *S;
  for (el=0; el < nEl; el++) {
    uintPtr[el] = uintDataPtr[el];
  }

  memcpy(&S,uintPtr,sizeof(SimStruct*));
  free(uintPtr);
  return S;
}

static int cgxe_get_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_android_controler_method_dispatcher(S,
    SS_CALL_MDL_GET_SIM_STATE, (void *) (plhs));
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_set_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_android_controler_method_dispatcher(S,
    SS_CALL_MDL_SET_SIM_STATE, (void *) prhs[2]);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_get_BuildInfoUpdate(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "gx4DvIBXThemebGNxxGrOG") == 0) {
    extern mxArray *cgxe_gx4DvIBXThemebGNxxGrOG_BuildInfoUpdate(void);
    plhs[0] = cgxe_gx4DvIBXThemebGNxxGrOG_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "D4jOH49czg0MK6QXrPXUn") == 0) {
    extern mxArray *cgxe_D4jOH49czg0MK6QXrPXUn_BuildInfoUpdate(void);
    plhs[0] = cgxe_D4jOH49czg0MK6QXrPXUn_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "Cs37LiUCU1lWCW2PvRhHpC") == 0) {
    extern mxArray *cgxe_Cs37LiUCU1lWCW2PvRhHpC_BuildInfoUpdate(void);
    plhs[0] = cgxe_Cs37LiUCU1lWCW2PvRhHpC_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "8A23tTWE2VYETOdIIZQpR") == 0) {
    extern mxArray *cgxe_8A23tTWE2VYETOdIIZQpR_BuildInfoUpdate(void);
    plhs[0] = cgxe_8A23tTWE2VYETOdIIZQpR_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "eYAiqQ2JmQLVI2GrziYjQH") == 0) {
    extern mxArray *cgxe_eYAiqQ2JmQLVI2GrziYjQH_BuildInfoUpdate(void);
    plhs[0] = cgxe_eYAiqQ2JmQLVI2GrziYjQH_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "XfXcbEA2QBlsAj1biKycCE") == 0) {
    extern mxArray *cgxe_XfXcbEA2QBlsAj1biKycCE_BuildInfoUpdate(void);
    plhs[0] = cgxe_XfXcbEA2QBlsAj1biKycCE_BuildInfoUpdate();
    return 1;
  }

  return 0;
}

static int cgxe_get_fallback_info(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "gx4DvIBXThemebGNxxGrOG") == 0) {
    extern mxArray *cgxe_gx4DvIBXThemebGNxxGrOG_fallback_info(void);
    plhs[0] = cgxe_gx4DvIBXThemebGNxxGrOG_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "D4jOH49czg0MK6QXrPXUn") == 0) {
    extern mxArray *cgxe_D4jOH49czg0MK6QXrPXUn_fallback_info(void);
    plhs[0] = cgxe_D4jOH49czg0MK6QXrPXUn_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "Cs37LiUCU1lWCW2PvRhHpC") == 0) {
    extern mxArray *cgxe_Cs37LiUCU1lWCW2PvRhHpC_fallback_info(void);
    plhs[0] = cgxe_Cs37LiUCU1lWCW2PvRhHpC_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "8A23tTWE2VYETOdIIZQpR") == 0) {
    extern mxArray *cgxe_8A23tTWE2VYETOdIIZQpR_fallback_info(void);
    plhs[0] = cgxe_8A23tTWE2VYETOdIIZQpR_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "eYAiqQ2JmQLVI2GrziYjQH") == 0) {
    extern mxArray *cgxe_eYAiqQ2JmQLVI2GrziYjQH_fallback_info(void);
    plhs[0] = cgxe_eYAiqQ2JmQLVI2GrziYjQH_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "XfXcbEA2QBlsAj1biKycCE") == 0) {
    extern mxArray *cgxe_XfXcbEA2QBlsAj1biKycCE_fallback_info(void);
    plhs[0] = cgxe_XfXcbEA2QBlsAj1biKycCE_fallback_info();
    return 1;
  }

  return 0;
}

#define PROCESS_MEX_SFUNCTION_CMD_LINE_CALL

static int ProcessMexSfunctionCmdLineCall(int nlhs, mxArray* plhs[], int nrhs,
  const mxArray* prhs[])
{
  char commandName[64];
  if (nrhs < 1 || !mxIsChar(prhs[0]))
    return 0;
  mxGetString(prhs[0], commandName, sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName, "get_checksums") == 0) {
    return cgxe_process_get_checksums(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "mex_unlock") == 0) {
    return cgxe_mex_unlock_call(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_sim_state") == 0) {
    return cgxe_get_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "set_sim_state") == 0) {
    return cgxe_set_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_BuildInfoUpdate") == 0) {
    return cgxe_get_BuildInfoUpdate(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_fallback_info") == 0) {
    return cgxe_get_fallback_info(nlhs, plhs, nrhs, prhs);
  }

  return 0;
}

#include "simulink.c"
