#ifndef __Cs37LiUCU1lWCW2PvRhHpC_h__
#define __Cs37LiUCU1lWCW2PvRhHpC_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "slexec_vm_zc_functions.h"

/* Type Definitions */
#ifndef typedef_codertarget_internal_androidButton
#define typedef_codertarget_internal_androidButton

typedef struct {
  int32_T isInitialized;
  boolean_T TunablePropsChanged;
  real_T SampleTime;
} codertarget_internal_androidButton;

#endif                                 /*typedef_codertarget_internal_androidButton*/

#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
  char_T f1[7];
} struct_T;

#endif                                 /*typedef_struct_T*/

#ifndef typedef_b_struct_T
#define typedef_b_struct_T

typedef struct {
  char_T f1[6];
  char_T f2[6];
} b_struct_T;

#endif                                 /*typedef_b_struct_T*/

#ifndef typedef_InstanceStruct_Cs37LiUCU1lWCW2PvRhHpC
#define typedef_InstanceStruct_Cs37LiUCU1lWCW2PvRhHpC

typedef struct {
  SimStruct *S;
  codertarget_internal_androidButton sysobj;
  boolean_T sysobj_not_empty;
  void *emlrtRootTLSGlobal;
  boolean_T *b_y0;
} InstanceStruct_Cs37LiUCU1lWCW2PvRhHpC;

#endif                                 /*typedef_InstanceStruct_Cs37LiUCU1lWCW2PvRhHpC*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_Cs37LiUCU1lWCW2PvRhHpC(SimStruct *S, int_T method,
  void* data);

#endif
