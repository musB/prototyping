#ifndef __eYAiqQ2JmQLVI2GrziYjQH_h__
#define __eYAiqQ2JmQLVI2GrziYjQH_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "slexec_vm_zc_functions.h"

/* Type Definitions */
#ifndef typedef_codertarget_internal_androidAccelerometer
#define typedef_codertarget_internal_androidAccelerometer

typedef struct {
  int32_T isInitialized;
  boolean_T TunablePropsChanged;
  real_T SampleTime;
} codertarget_internal_androidAccelerometer;

#endif                                 /*typedef_codertarget_internal_androidAccelerometer*/

#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
  char_T f1[7];
} struct_T;

#endif                                 /*typedef_struct_T*/

#ifndef typedef_b_struct_T
#define typedef_b_struct_T

typedef struct {
  char_T f1[6];
  char_T f2[6];
} b_struct_T;

#endif                                 /*typedef_b_struct_T*/

#ifndef typedef_InstanceStruct_eYAiqQ2JmQLVI2GrziYjQH
#define typedef_InstanceStruct_eYAiqQ2JmQLVI2GrziYjQH

typedef struct {
  SimStruct *S;
  codertarget_internal_androidAccelerometer sysobj;
  boolean_T sysobj_not_empty;
  void *emlrtRootTLSGlobal;
  real32_T *b_y0;
  real32_T *b_y1;
  real32_T *y2;
} InstanceStruct_eYAiqQ2JmQLVI2GrziYjQH;

#endif                                 /*typedef_InstanceStruct_eYAiqQ2JmQLVI2GrziYjQH*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_eYAiqQ2JmQLVI2GrziYjQH(SimStruct *S, int_T method,
  void* data);

#endif
