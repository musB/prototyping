/* Include files */

#include "android_controler_cgxe.h"
#include "m_gx4DvIBXThemebGNxxGrOG.h"
#include "m_D4jOH49czg0MK6QXrPXUn.h"
#include "m_Cs37LiUCU1lWCW2PvRhHpC.h"
#include "m_8A23tTWE2VYETOdIIZQpR.h"
#include "m_eYAiqQ2JmQLVI2GrziYjQH.h"
#include "m_XfXcbEA2QBlsAj1biKycCE.h"

unsigned int cgxe_android_controler_method_dispatcher(SimStruct* S, int_T method,
  void* data)
{
  if (ssGetChecksum0(S) == 133849349 &&
      ssGetChecksum1(S) == 2411175902 &&
      ssGetChecksum2(S) == 728459727 &&
      ssGetChecksum3(S) == 2065912730) {
    method_dispatcher_gx4DvIBXThemebGNxxGrOG(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1799478551 &&
      ssGetChecksum1(S) == 115921845 &&
      ssGetChecksum2(S) == 2516272101 &&
      ssGetChecksum3(S) == 3321476869) {
    method_dispatcher_D4jOH49czg0MK6QXrPXUn(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2284856100 &&
      ssGetChecksum1(S) == 2597573358 &&
      ssGetChecksum2(S) == 298573034 &&
      ssGetChecksum3(S) == 1337758597) {
    method_dispatcher_Cs37LiUCU1lWCW2PvRhHpC(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3142023313 &&
      ssGetChecksum1(S) == 3387156537 &&
      ssGetChecksum2(S) == 545059890 &&
      ssGetChecksum3(S) == 2654554250) {
    method_dispatcher_8A23tTWE2VYETOdIIZQpR(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3470754072 &&
      ssGetChecksum1(S) == 2096815322 &&
      ssGetChecksum2(S) == 477920432 &&
      ssGetChecksum3(S) == 1894057529) {
    method_dispatcher_eYAiqQ2JmQLVI2GrziYjQH(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3802960954 &&
      ssGetChecksum1(S) == 2005578432 &&
      ssGetChecksum2(S) == 3167107951 &&
      ssGetChecksum3(S) == 1425291663) {
    method_dispatcher_XfXcbEA2QBlsAj1biKycCE(S, method, data);
    return 1;
  }

  return 0;
}
