/* Include files */

#include "androidraspberrypigettingstarted_modified_cgxe.h"
#include "m_FFVSNh0hU8SS1dDTo2slb.h"
#include "m_0Z4bnQeLe7QLBMhA1dWTXG.h"
#include "m_aFfYymxN9Bo4l29GU5uBmD.h"
#include "m_ZmEX9J1ISRstSvdZzeVDbD.h"
#include "m_LBmnDCF3dfCAW3dMI1X0yF.h"
#include "m_ek59v9i5mGAQS5Z6sZFEjC.h"
#include "m_a7KjRdpnfNhS6W01dwRlJ.h"

unsigned int cgxe_androidraspberrypigettingstarted_modified_method_dispatcher
  (SimStruct* S, int_T method, void* data)
{
  if (ssGetChecksum0(S) == 949777052 &&
      ssGetChecksum1(S) == 4053039914 &&
      ssGetChecksum2(S) == 3498017432 &&
      ssGetChecksum3(S) == 123257519) {
    method_dispatcher_FFVSNh0hU8SS1dDTo2slb(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1021880361 &&
      ssGetChecksum1(S) == 528812753 &&
      ssGetChecksum2(S) == 467971849 &&
      ssGetChecksum3(S) == 1937819282) {
    method_dispatcher_0Z4bnQeLe7QLBMhA1dWTXG(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1248455707 &&
      ssGetChecksum1(S) == 3079680566 &&
      ssGetChecksum2(S) == 974305018 &&
      ssGetChecksum3(S) == 456001185) {
    method_dispatcher_aFfYymxN9Bo4l29GU5uBmD(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2137107623 &&
      ssGetChecksum1(S) == 2530767341 &&
      ssGetChecksum2(S) == 2333401926 &&
      ssGetChecksum3(S) == 1672591295) {
    method_dispatcher_ZmEX9J1ISRstSvdZzeVDbD(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2207660012 &&
      ssGetChecksum1(S) == 3710883166 &&
      ssGetChecksum2(S) == 590695163 &&
      ssGetChecksum3(S) == 2045547192) {
    method_dispatcher_LBmnDCF3dfCAW3dMI1X0yF(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2391338435 &&
      ssGetChecksum1(S) == 621487689 &&
      ssGetChecksum2(S) == 2619530436 &&
      ssGetChecksum3(S) == 296589572) {
    method_dispatcher_ek59v9i5mGAQS5Z6sZFEjC(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2523228347 &&
      ssGetChecksum1(S) == 1327149588 &&
      ssGetChecksum2(S) == 489770006 &&
      ssGetChecksum3(S) == 1337564717) {
    method_dispatcher_a7KjRdpnfNhS6W01dwRlJ(S, method, data);
    return 1;
  }

  return 0;
}
