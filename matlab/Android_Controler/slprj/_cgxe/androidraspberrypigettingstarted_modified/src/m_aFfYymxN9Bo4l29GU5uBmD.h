#ifndef __aFfYymxN9Bo4l29GU5uBmD_h__
#define __aFfYymxN9Bo4l29GU5uBmD_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "slexec_vm_zc_functions.h"

/* Type Definitions */
#ifndef typedef_codertarget_internal_androidButton
#define typedef_codertarget_internal_androidButton

typedef struct {
  int32_T isInitialized;
  boolean_T TunablePropsChanged;
  real_T SampleTime;
} codertarget_internal_androidButton;

#endif                                 /*typedef_codertarget_internal_androidButton*/

#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
  char_T f1[7];
} struct_T;

#endif                                 /*typedef_struct_T*/

#ifndef typedef_b_struct_T
#define typedef_b_struct_T

typedef struct {
  char_T f1[6];
  char_T f2[6];
} b_struct_T;

#endif                                 /*typedef_b_struct_T*/

#ifndef typedef_InstanceStruct_aFfYymxN9Bo4l29GU5uBmD
#define typedef_InstanceStruct_aFfYymxN9Bo4l29GU5uBmD

typedef struct {
  SimStruct *S;
  codertarget_internal_androidButton sysobj;
  boolean_T sysobj_not_empty;
  void *emlrtRootTLSGlobal;
  boolean_T *b_y0;
} InstanceStruct_aFfYymxN9Bo4l29GU5uBmD;

#endif                                 /*typedef_InstanceStruct_aFfYymxN9Bo4l29GU5uBmD*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_aFfYymxN9Bo4l29GU5uBmD(SimStruct *S, int_T method,
  void* data);

#endif
