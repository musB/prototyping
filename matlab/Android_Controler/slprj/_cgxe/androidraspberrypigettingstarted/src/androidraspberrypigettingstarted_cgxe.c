/* Include files */

#include "androidraspberrypigettingstarted_cgxe.h"
#include "m_3UntM6jJN52ZmeDK7WTFBE.h"
#include "m_FFVSNh0hU8SS1dDTo2slb.h"
#include "m_RLBJjozCobS4OZ23DV07rF.h"
#include "m_zaYlmNRsixk3IT2lG3A6Z.h"
#include "m_SjDCmGVwyafm3BzdH5K0NE.h"
#include "m_5QtI1gQQWBDKA6E4G3ELdD.h"
#include "m_bXnLlaKP2YUlI9DPmmVY5C.h"

unsigned int cgxe_androidraspberrypigettingstarted_method_dispatcher(SimStruct*
  S, int_T method, void* data)
{
  if (ssGetChecksum0(S) == 835672199 &&
      ssGetChecksum1(S) == 2862951994 &&
      ssGetChecksum2(S) == 3258638003 &&
      ssGetChecksum3(S) == 4284627330) {
    method_dispatcher_3UntM6jJN52ZmeDK7WTFBE(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 949777052 &&
      ssGetChecksum1(S) == 4053039914 &&
      ssGetChecksum2(S) == 3498017432 &&
      ssGetChecksum3(S) == 123257519) {
    method_dispatcher_FFVSNh0hU8SS1dDTo2slb(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1246300184 &&
      ssGetChecksum1(S) == 2363092915 &&
      ssGetChecksum2(S) == 3608496565 &&
      ssGetChecksum3(S) == 3474097778) {
    method_dispatcher_RLBJjozCobS4OZ23DV07rF(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3035732635 &&
      ssGetChecksum1(S) == 336602131 &&
      ssGetChecksum2(S) == 3044632446 &&
      ssGetChecksum3(S) == 1692546428) {
    method_dispatcher_zaYlmNRsixk3IT2lG3A6Z(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3874701261 &&
      ssGetChecksum1(S) == 3425157023 &&
      ssGetChecksum2(S) == 3691158483 &&
      ssGetChecksum3(S) == 2188704928) {
    method_dispatcher_SjDCmGVwyafm3BzdH5K0NE(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 3914951824 &&
      ssGetChecksum1(S) == 1482075132 &&
      ssGetChecksum2(S) == 3817966044 &&
      ssGetChecksum3(S) == 3833896349) {
    method_dispatcher_5QtI1gQQWBDKA6E4G3ELdD(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 4191512756 &&
      ssGetChecksum1(S) == 3537260502 &&
      ssGetChecksum2(S) == 102024872 &&
      ssGetChecksum3(S) == 2561842367) {
    method_dispatcher_bXnLlaKP2YUlI9DPmmVY5C(S, method, data);
    return 1;
  }

  return 0;
}
