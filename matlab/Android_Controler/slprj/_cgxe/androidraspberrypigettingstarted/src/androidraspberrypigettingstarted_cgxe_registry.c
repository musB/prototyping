#define S_FUNCTION_LEVEL               2
#define S_FUNCTION_NAME                androidraspberrypigettingstarted_cgxe
#include "simstruc.h"
#include "androidraspberrypigettingstarted_cgxe.h"
#define MDL_START

static void mdlStart(SimStruct* S)
{
  unsigned int success;
  success = cgxe_androidraspberrypigettingstarted_method_dispatcher(S,
    SS_CALL_MDL_START, NULL);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }
}

#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlInitializeConditions method directly.\n");
}

#define MDL_UPDATE

static void mdlUpdate(SimStruct *S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlUpdate method directly.\n");
}

static void mdlOutputs(SimStruct* S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlOutputs method directly.\n");
}

static void mdlTerminate(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlTerminate method directly.\n");
}

static void mdlInitializeSizes(SimStruct *S)
{
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
}

static mxArray* cgxe_get_supported_modules(void)
{
  mxArray* mxModules = mxCreateCellMatrix(7, 1);
  mxArray* mxChksum = NULL;
  uint32_T* checksumData = NULL;
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 835672199;
  checksumData[1] = 2862951994;
  checksumData[2] = 3258638003;
  checksumData[3] = 4284627330;
  mxSetCell(mxModules, 0, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 949777052;
  checksumData[1] = 4053039914;
  checksumData[2] = 3498017432;
  checksumData[3] = 123257519;
  mxSetCell(mxModules, 1, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 1246300184;
  checksumData[1] = 2363092915;
  checksumData[2] = 3608496565;
  checksumData[3] = 3474097778;
  mxSetCell(mxModules, 2, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3035732635;
  checksumData[1] = 336602131;
  checksumData[2] = 3044632446;
  checksumData[3] = 1692546428;
  mxSetCell(mxModules, 3, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3874701261;
  checksumData[1] = 3425157023;
  checksumData[2] = 3691158483;
  checksumData[3] = 2188704928;
  mxSetCell(mxModules, 4, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 3914951824;
  checksumData[1] = 1482075132;
  checksumData[2] = 3817966044;
  checksumData[3] = 3833896349;
  mxSetCell(mxModules, 5, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 4191512756;
  checksumData[1] = 3537260502;
  checksumData[2] = 102024872;
  checksumData[3] = 2561842367;
  mxSetCell(mxModules, 6, mxChksum);
  return mxModules;
}

static int cgxe_process_get_checksums(int nlhs, mxArray* plhs[], int nrhs, const
  mxArray* prhs[])
{
  const char* checksumFields[] = { "modules", "model", "makefile", "target",
    "overall" };

  mxArray* mxChecksum = mxCreateStructMatrix(1, 1, 5, checksumFields);
  mxSetField(mxChecksum, 0, "modules", cgxe_get_supported_modules());

  {
    mxArray* mxModelChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxModelChksum);
    checksumData[0] = 1497325917;
    checksumData[1] = 2598738338;
    checksumData[2] = 3593971402;
    checksumData[3] = 2269448612;
    mxSetField(mxChecksum, 0, "model", mxModelChksum);
  }

  {
    mxArray* mxMakefileChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxMakefileChksum);
    checksumData[0] = 1296770532;
    checksumData[1] = 1699480216;
    checksumData[2] = 4157805599;
    checksumData[3] = 1036426483;
    mxSetField(mxChecksum, 0, "makefile", mxMakefileChksum);
  }

  {
    mxArray* mxTargetChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxTargetChksum);
    checksumData[0] = 0;
    checksumData[1] = 0;
    checksumData[2] = 0;
    checksumData[3] = 0;
    mxSetField(mxChecksum, 0, "target", mxTargetChksum);
  }

  {
    mxArray* mxOverallChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxOverallChksum);
    checksumData[0] = 763047163;
    checksumData[1] = 815405157;
    checksumData[2] = 1533389920;
    checksumData[3] = 2215538;
    mxSetField(mxChecksum, 0, "overall", mxOverallChksum);
  }

  plhs[0] = mxChecksum;
  return 1;
}

static int cgxe_mex_unlock_call(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  while (mexIsLocked()) {
    mexUnlock();
  }

  return 1;
}

static SimStruct* cgxe_unpack_simstruct(const mxArray *mxS)
{
  uint32_T *uintPtr = (uint32_T*)malloc(sizeof(SimStruct*));
  int nEl = sizeof(SimStruct*)/sizeof(uint32_T);
  uint32_T *uintDataPtr = (uint32_T *)mxGetData(mxS);
  int el;
  SimStruct *S;
  for (el=0; el < nEl; el++) {
    uintPtr[el] = uintDataPtr[el];
  }

  memcpy(&S,uintPtr,sizeof(SimStruct*));
  free(uintPtr);
  return S;
}

static int cgxe_get_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_androidraspberrypigettingstarted_method_dispatcher(S,
    SS_CALL_MDL_GET_SIM_STATE, (void *) (plhs));
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_set_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_androidraspberrypigettingstarted_method_dispatcher(S,
    SS_CALL_MDL_SET_SIM_STATE, (void *) prhs[2]);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_get_BuildInfoUpdate(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "3UntM6jJN52ZmeDK7WTFBE") == 0) {
    extern mxArray *cgxe_3UntM6jJN52ZmeDK7WTFBE_BuildInfoUpdate(void);
    plhs[0] = cgxe_3UntM6jJN52ZmeDK7WTFBE_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "FFVSNh0hU8SS1dDTo2slb") == 0) {
    extern mxArray *cgxe_FFVSNh0hU8SS1dDTo2slb_BuildInfoUpdate(void);
    plhs[0] = cgxe_FFVSNh0hU8SS1dDTo2slb_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "RLBJjozCobS4OZ23DV07rF") == 0) {
    extern mxArray *cgxe_RLBJjozCobS4OZ23DV07rF_BuildInfoUpdate(void);
    plhs[0] = cgxe_RLBJjozCobS4OZ23DV07rF_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "zaYlmNRsixk3IT2lG3A6Z") == 0) {
    extern mxArray *cgxe_zaYlmNRsixk3IT2lG3A6Z_BuildInfoUpdate(void);
    plhs[0] = cgxe_zaYlmNRsixk3IT2lG3A6Z_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "SjDCmGVwyafm3BzdH5K0NE") == 0) {
    extern mxArray *cgxe_SjDCmGVwyafm3BzdH5K0NE_BuildInfoUpdate(void);
    plhs[0] = cgxe_SjDCmGVwyafm3BzdH5K0NE_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "5QtI1gQQWBDKA6E4G3ELdD") == 0) {
    extern mxArray *cgxe_5QtI1gQQWBDKA6E4G3ELdD_BuildInfoUpdate(void);
    plhs[0] = cgxe_5QtI1gQQWBDKA6E4G3ELdD_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "bXnLlaKP2YUlI9DPmmVY5C") == 0) {
    extern mxArray *cgxe_bXnLlaKP2YUlI9DPmmVY5C_BuildInfoUpdate(void);
    plhs[0] = cgxe_bXnLlaKP2YUlI9DPmmVY5C_BuildInfoUpdate();
    return 1;
  }

  return 0;
}

static int cgxe_get_fallback_info(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "3UntM6jJN52ZmeDK7WTFBE") == 0) {
    extern mxArray *cgxe_3UntM6jJN52ZmeDK7WTFBE_fallback_info(void);
    plhs[0] = cgxe_3UntM6jJN52ZmeDK7WTFBE_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "FFVSNh0hU8SS1dDTo2slb") == 0) {
    extern mxArray *cgxe_FFVSNh0hU8SS1dDTo2slb_fallback_info(void);
    plhs[0] = cgxe_FFVSNh0hU8SS1dDTo2slb_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "RLBJjozCobS4OZ23DV07rF") == 0) {
    extern mxArray *cgxe_RLBJjozCobS4OZ23DV07rF_fallback_info(void);
    plhs[0] = cgxe_RLBJjozCobS4OZ23DV07rF_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "zaYlmNRsixk3IT2lG3A6Z") == 0) {
    extern mxArray *cgxe_zaYlmNRsixk3IT2lG3A6Z_fallback_info(void);
    plhs[0] = cgxe_zaYlmNRsixk3IT2lG3A6Z_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "SjDCmGVwyafm3BzdH5K0NE") == 0) {
    extern mxArray *cgxe_SjDCmGVwyafm3BzdH5K0NE_fallback_info(void);
    plhs[0] = cgxe_SjDCmGVwyafm3BzdH5K0NE_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "5QtI1gQQWBDKA6E4G3ELdD") == 0) {
    extern mxArray *cgxe_5QtI1gQQWBDKA6E4G3ELdD_fallback_info(void);
    plhs[0] = cgxe_5QtI1gQQWBDKA6E4G3ELdD_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "bXnLlaKP2YUlI9DPmmVY5C") == 0) {
    extern mxArray *cgxe_bXnLlaKP2YUlI9DPmmVY5C_fallback_info(void);
    plhs[0] = cgxe_bXnLlaKP2YUlI9DPmmVY5C_fallback_info();
    return 1;
  }

  return 0;
}

#define PROCESS_MEX_SFUNCTION_CMD_LINE_CALL

static int ProcessMexSfunctionCmdLineCall(int nlhs, mxArray* plhs[], int nrhs,
  const mxArray* prhs[])
{
  char commandName[64];
  if (nrhs < 1 || !mxIsChar(prhs[0]))
    return 0;
  mxGetString(prhs[0], commandName, sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName, "get_checksums") == 0) {
    return cgxe_process_get_checksums(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "mex_unlock") == 0) {
    return cgxe_mex_unlock_call(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_sim_state") == 0) {
    return cgxe_get_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "set_sim_state") == 0) {
    return cgxe_set_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_BuildInfoUpdate") == 0) {
    return cgxe_get_BuildInfoUpdate(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_fallback_info") == 0) {
    return cgxe_get_fallback_info(nlhs, plhs, nrhs, prhs);
  }

  return 0;
}

#include "simulink.c"
