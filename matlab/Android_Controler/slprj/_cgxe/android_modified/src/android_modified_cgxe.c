/* Include files */

#include "android_modified_cgxe.h"
#include "m_FFVSNh0hU8SS1dDTo2slb.h"
#include "m_u0DIBHfFhcV1WgmSxQFwS.h"
#include "m_MN3IDc1LL6mBlvYZc2HWAF.h"
#include "m_GRJKJ2OZWJCXJYsBdpU5mE.h"
#include "m_fSlLc30OyNegrTBfRwLOVC.h"
#include "m_E4Q1TJe3hvZhvtftIBalj.h"

unsigned int cgxe_android_modified_method_dispatcher(SimStruct* S, int_T method,
  void* data)
{
  if (ssGetChecksum0(S) == 949777052 &&
      ssGetChecksum1(S) == 4053039914 &&
      ssGetChecksum2(S) == 3498017432 &&
      ssGetChecksum3(S) == 123257519) {
    method_dispatcher_FFVSNh0hU8SS1dDTo2slb(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1143345484 &&
      ssGetChecksum1(S) == 1265167807 &&
      ssGetChecksum2(S) == 851172388 &&
      ssGetChecksum3(S) == 2724269868) {
    method_dispatcher_u0DIBHfFhcV1WgmSxQFwS(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1418417532 &&
      ssGetChecksum1(S) == 2093972633 &&
      ssGetChecksum2(S) == 626643125 &&
      ssGetChecksum3(S) == 2138192714) {
    method_dispatcher_MN3IDc1LL6mBlvYZc2HWAF(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 1430046264 &&
      ssGetChecksum1(S) == 522299103 &&
      ssGetChecksum2(S) == 1663967119 &&
      ssGetChecksum3(S) == 2954190398) {
    method_dispatcher_GRJKJ2OZWJCXJYsBdpU5mE(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2428617805 &&
      ssGetChecksum1(S) == 1827871851 &&
      ssGetChecksum2(S) == 2757645923 &&
      ssGetChecksum3(S) == 3585749116) {
    method_dispatcher_fSlLc30OyNegrTBfRwLOVC(S, method, data);
    return 1;
  }

  if (ssGetChecksum0(S) == 2481099013 &&
      ssGetChecksum1(S) == 1341269695 &&
      ssGetChecksum2(S) == 3491001280 &&
      ssGetChecksum3(S) == 2740770416) {
    method_dispatcher_E4Q1TJe3hvZhvtftIBalj(S, method, data);
    return 1;
  }

  return 0;
}
