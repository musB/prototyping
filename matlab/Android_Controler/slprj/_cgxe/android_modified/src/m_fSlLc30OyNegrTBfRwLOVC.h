#ifndef __fSlLc30OyNegrTBfRwLOVC_h__
#define __fSlLc30OyNegrTBfRwLOVC_h__

/* Include files */
#include "simstruc.h"
#include "rtwtypes.h"
#include "multiword_types.h"
#include "slexec_vm_zc_functions.h"

/* Type Definitions */
#ifndef typedef_codertarget_internal_androidButton
#define typedef_codertarget_internal_androidButton

typedef struct {
  int32_T isInitialized;
  boolean_T TunablePropsChanged;
  real_T SampleTime;
} codertarget_internal_androidButton;

#endif                                 /*typedef_codertarget_internal_androidButton*/

#ifndef typedef_struct_T
#define typedef_struct_T

typedef struct {
  char_T f1[7];
} struct_T;

#endif                                 /*typedef_struct_T*/

#ifndef typedef_b_struct_T
#define typedef_b_struct_T

typedef struct {
  char_T f1[6];
  char_T f2[6];
} b_struct_T;

#endif                                 /*typedef_b_struct_T*/

#ifndef typedef_InstanceStruct_fSlLc30OyNegrTBfRwLOVC
#define typedef_InstanceStruct_fSlLc30OyNegrTBfRwLOVC

typedef struct {
  SimStruct *S;
  codertarget_internal_androidButton sysobj;
  boolean_T sysobj_not_empty;
  void *emlrtRootTLSGlobal;
  boolean_T *b_y0;
} InstanceStruct_fSlLc30OyNegrTBfRwLOVC;

#endif                                 /*typedef_InstanceStruct_fSlLc30OyNegrTBfRwLOVC*/

/* Named Constants */

/* Variable Declarations */

/* Variable Definitions */

/* Function Declarations */

/* Function Definitions */
extern void method_dispatcher_fSlLc30OyNegrTBfRwLOVC(SimStruct *S, int_T method,
  void* data);

#endif
