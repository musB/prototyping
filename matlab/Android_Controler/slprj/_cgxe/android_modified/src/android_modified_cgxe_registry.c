#define S_FUNCTION_LEVEL               2
#define S_FUNCTION_NAME                android_modified_cgxe
#include "simstruc.h"
#include "android_modified_cgxe.h"
#define MDL_START

static void mdlStart(SimStruct* S)
{
  unsigned int success;
  success = cgxe_android_modified_method_dispatcher(S, SS_CALL_MDL_START, NULL);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }
}

#define MDL_INITIALIZE_CONDITIONS

static void mdlInitializeConditions(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlInitializeConditions method directly.\n");
}

#define MDL_UPDATE

static void mdlUpdate(SimStruct *S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlUpdate method directly.\n");
}

static void mdlOutputs(SimStruct* S, int_T tid)
{
  mexPrintf("ERROR: Calling model mdlOutputs method directly.\n");
}

static void mdlTerminate(SimStruct *S)
{
  mexPrintf("ERROR: Calling model mdlTerminate method directly.\n");
}

static void mdlInitializeSizes(SimStruct *S)
{
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
}

static mxArray* cgxe_get_supported_modules(void)
{
  mxArray* mxModules = mxCreateCellMatrix(6, 1);
  mxArray* mxChksum = NULL;
  uint32_T* checksumData = NULL;
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 949777052;
  checksumData[1] = 4053039914;
  checksumData[2] = 3498017432;
  checksumData[3] = 123257519;
  mxSetCell(mxModules, 0, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 1143345484;
  checksumData[1] = 1265167807;
  checksumData[2] = 851172388;
  checksumData[3] = 2724269868;
  mxSetCell(mxModules, 1, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 1418417532;
  checksumData[1] = 2093972633;
  checksumData[2] = 626643125;
  checksumData[3] = 2138192714;
  mxSetCell(mxModules, 2, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 1430046264;
  checksumData[1] = 522299103;
  checksumData[2] = 1663967119;
  checksumData[3] = 2954190398;
  mxSetCell(mxModules, 3, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 2428617805;
  checksumData[1] = 1827871851;
  checksumData[2] = 2757645923;
  checksumData[3] = 3585749116;
  mxSetCell(mxModules, 4, mxChksum);
  mxChksum = mxCreateNumericMatrix(1, 4, mxUINT32_CLASS, mxREAL);
  checksumData = (uint32_T*) mxGetData(mxChksum);
  checksumData[0] = 2481099013;
  checksumData[1] = 1341269695;
  checksumData[2] = 3491001280;
  checksumData[3] = 2740770416;
  mxSetCell(mxModules, 5, mxChksum);
  return mxModules;
}

static int cgxe_process_get_checksums(int nlhs, mxArray* plhs[], int nrhs, const
  mxArray* prhs[])
{
  const char* checksumFields[] = { "modules", "model", "makefile", "target",
    "overall" };

  mxArray* mxChecksum = mxCreateStructMatrix(1, 1, 5, checksumFields);
  mxSetField(mxChecksum, 0, "modules", cgxe_get_supported_modules());

  {
    mxArray* mxModelChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxModelChksum);
    checksumData[0] = 2481097282;
    checksumData[1] = 3838902468;
    checksumData[2] = 2816388947;
    checksumData[3] = 1848332712;
    mxSetField(mxChecksum, 0, "model", mxModelChksum);
  }

  {
    mxArray* mxMakefileChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxMakefileChksum);
    checksumData[0] = 1574908121;
    checksumData[1] = 1677948485;
    checksumData[2] = 2303277462;
    checksumData[3] = 723788377;
    mxSetField(mxChecksum, 0, "makefile", mxMakefileChksum);
  }

  {
    mxArray* mxTargetChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxTargetChksum);
    checksumData[0] = 0;
    checksumData[1] = 0;
    checksumData[2] = 0;
    checksumData[3] = 0;
    mxSetField(mxChecksum, 0, "target", mxTargetChksum);
  }

  {
    mxArray* mxOverallChksum = mxCreateDoubleMatrix(1, 4, mxREAL);
    double* checksumData = (double*) mxGetData(mxOverallChksum);
    checksumData[0] = 487168700;
    checksumData[1] = 1153900269;
    checksumData[2] = 289008855;
    checksumData[3] = 310846866;
    mxSetField(mxChecksum, 0, "overall", mxOverallChksum);
  }

  plhs[0] = mxChecksum;
  return 1;
}

static int cgxe_mex_unlock_call(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  while (mexIsLocked()) {
    mexUnlock();
  }

  return 1;
}

static SimStruct* cgxe_unpack_simstruct(const mxArray *mxS)
{
  uint32_T *uintPtr = (uint32_T*)malloc(sizeof(SimStruct*));
  int nEl = sizeof(SimStruct*)/sizeof(uint32_T);
  uint32_T *uintDataPtr = (uint32_T *)mxGetData(mxS);
  int el;
  SimStruct *S;
  for (el=0; el < nEl; el++) {
    uintPtr[el] = uintDataPtr[el];
  }

  memcpy(&S,uintPtr,sizeof(SimStruct*));
  free(uintPtr);
  return S;
}

static int cgxe_get_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_android_modified_method_dispatcher(S, SS_CALL_MDL_GET_SIM_STATE,
    (void *) (plhs));
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_set_sim_state(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  unsigned int success;
  SimStruct *S = cgxe_unpack_simstruct(prhs[1]);
  success = cgxe_android_modified_method_dispatcher(S, SS_CALL_MDL_SET_SIM_STATE,
    (void *) prhs[2]);
  if (!success) {
    /* error */
    mexPrintf("ERROR: Failed to dispatch s-function method!\n");
  }

  return 1;
}

static int cgxe_get_BuildInfoUpdate(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "FFVSNh0hU8SS1dDTo2slb") == 0) {
    extern mxArray *cgxe_FFVSNh0hU8SS1dDTo2slb_BuildInfoUpdate(void);
    plhs[0] = cgxe_FFVSNh0hU8SS1dDTo2slb_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "u0DIBHfFhcV1WgmSxQFwS") == 0) {
    extern mxArray *cgxe_u0DIBHfFhcV1WgmSxQFwS_BuildInfoUpdate(void);
    plhs[0] = cgxe_u0DIBHfFhcV1WgmSxQFwS_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "MN3IDc1LL6mBlvYZc2HWAF") == 0) {
    extern mxArray *cgxe_MN3IDc1LL6mBlvYZc2HWAF_BuildInfoUpdate(void);
    plhs[0] = cgxe_MN3IDc1LL6mBlvYZc2HWAF_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "GRJKJ2OZWJCXJYsBdpU5mE") == 0) {
    extern mxArray *cgxe_GRJKJ2OZWJCXJYsBdpU5mE_BuildInfoUpdate(void);
    plhs[0] = cgxe_GRJKJ2OZWJCXJYsBdpU5mE_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "fSlLc30OyNegrTBfRwLOVC") == 0) {
    extern mxArray *cgxe_fSlLc30OyNegrTBfRwLOVC_BuildInfoUpdate(void);
    plhs[0] = cgxe_fSlLc30OyNegrTBfRwLOVC_BuildInfoUpdate();
    return 1;
  }

  if (strcmp(tpChksum, "E4Q1TJe3hvZhvtftIBalj") == 0) {
    extern mxArray *cgxe_E4Q1TJe3hvZhvtftIBalj_BuildInfoUpdate(void);
    plhs[0] = cgxe_E4Q1TJe3hvZhvtftIBalj_BuildInfoUpdate();
    return 1;
  }

  return 0;
}

static int cgxe_get_fallback_info(int nlhs, mxArray * plhs[], int nrhs, const
  mxArray * prhs[])
{
  char tpChksum[64];
  mxGetString(prhs[1], tpChksum,sizeof(tpChksum)/sizeof(char));
  tpChksum[(sizeof(tpChksum)/sizeof(char)-1)] = '\0';
  if (strcmp(tpChksum, "FFVSNh0hU8SS1dDTo2slb") == 0) {
    extern mxArray *cgxe_FFVSNh0hU8SS1dDTo2slb_fallback_info(void);
    plhs[0] = cgxe_FFVSNh0hU8SS1dDTo2slb_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "u0DIBHfFhcV1WgmSxQFwS") == 0) {
    extern mxArray *cgxe_u0DIBHfFhcV1WgmSxQFwS_fallback_info(void);
    plhs[0] = cgxe_u0DIBHfFhcV1WgmSxQFwS_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "MN3IDc1LL6mBlvYZc2HWAF") == 0) {
    extern mxArray *cgxe_MN3IDc1LL6mBlvYZc2HWAF_fallback_info(void);
    plhs[0] = cgxe_MN3IDc1LL6mBlvYZc2HWAF_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "GRJKJ2OZWJCXJYsBdpU5mE") == 0) {
    extern mxArray *cgxe_GRJKJ2OZWJCXJYsBdpU5mE_fallback_info(void);
    plhs[0] = cgxe_GRJKJ2OZWJCXJYsBdpU5mE_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "fSlLc30OyNegrTBfRwLOVC") == 0) {
    extern mxArray *cgxe_fSlLc30OyNegrTBfRwLOVC_fallback_info(void);
    plhs[0] = cgxe_fSlLc30OyNegrTBfRwLOVC_fallback_info();
    return 1;
  }

  if (strcmp(tpChksum, "E4Q1TJe3hvZhvtftIBalj") == 0) {
    extern mxArray *cgxe_E4Q1TJe3hvZhvtftIBalj_fallback_info(void);
    plhs[0] = cgxe_E4Q1TJe3hvZhvtftIBalj_fallback_info();
    return 1;
  }

  return 0;
}

#define PROCESS_MEX_SFUNCTION_CMD_LINE_CALL

static int ProcessMexSfunctionCmdLineCall(int nlhs, mxArray* plhs[], int nrhs,
  const mxArray* prhs[])
{
  char commandName[64];
  if (nrhs < 1 || !mxIsChar(prhs[0]))
    return 0;
  mxGetString(prhs[0], commandName, sizeof(commandName)/sizeof(char));
  commandName[(sizeof(commandName)/sizeof(char)-1)] = '\0';
  if (strcmp(commandName, "get_checksums") == 0) {
    return cgxe_process_get_checksums(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "mex_unlock") == 0) {
    return cgxe_mex_unlock_call(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_sim_state") == 0) {
    return cgxe_get_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "set_sim_state") == 0) {
    return cgxe_set_sim_state(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_BuildInfoUpdate") == 0) {
    return cgxe_get_BuildInfoUpdate(nlhs, plhs, nrhs, prhs);
  }

  if (strcmp(commandName, "get_fallback_info") == 0) {
    return cgxe_get_fallback_info(nlhs, plhs, nrhs, prhs);
  }

  return 0;
}

#include "simulink.c"
