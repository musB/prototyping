/*
 * globalfunctionforBigrobotBisTestAudio_dt.h
 *
 * Code generation for model "globalfunctionforBigrobotBisTestAudio".
 *
 * Model version              : 1.7
 * Simulink Coder version : 8.12 (R2017a) 16-Feb-2017
 * C source code generated on : Mon Dec 18 10:55:01 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(codertarget_linux_blocks_Digi_T),
  sizeof(raspi_internal_PWMBlock_globa_T),
  sizeof(codertarget_linux_blocks_Di_o_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "codertarget_linux_blocks_Digi_T",
  "raspi_internal_PWMBlock_globa_T",
  "codertarget_linux_blocks_Di_o_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&globalfunctionforBigrobotBisT_B.Constant), 0, 0, 19 },

  { (char_T *)(&globalfunctionforBigrobotBisT_B.FromWorkspace), 4, 0, 4 },

  { (char_T *)(&globalfunctionforBigrobotBisT_B.DigitalRead), 8, 0, 7 }
  ,

  { (char_T *)(&globalfunctionforBigrobotBis_DW.obj), 14, 0, 9 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.RateTransition3_Buffer0), 0, 0,
    6 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.obj_nv), 15, 0, 2 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.FromWorkspace_PWORK.TimePtr), 11,
    0, 20 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.Buffer_inBufPtrIdx), 6, 0, 8 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.obj_f), 16, 0, 5 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.FromWorkspace_IWORK.PrevIndex),
    10, 0, 4 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.Buffer_CircBuf[0]), 4, 0, 176400
  },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.is_active_c2_globalfunctionforB),
    3, 0, 11 },

  { (char_T *)(&globalfunctionforBigrobotBis_DW.p_not_empty), 8, 0, 2 }
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  13U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&globalfunctionforBigrobotBisT_P.Switch_Threshold), 0, 0, 23 },

  { (char_T *)(&globalfunctionforBigrobotBisT_P.Gain_Gain_m), 1, 0, 4 },

  { (char_T *)(&globalfunctionforBigrobotBisT_P.Buffer_ic[0]), 4, 0, 88200 }
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  3U,
  rtPTransitions
};

/* [EOF] globalfunctionforBigrobotBisTestAudio_dt.h */
