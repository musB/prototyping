/*
 * File: globalfunctionforBigrobotBisTestAudio_types.h
 *
 * Code generated for Simulink model 'globalfunctionforBigrobotBisTestAudio'.
 *
 * Model version                  : 1.7
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Dec 18 10:55:01 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_globalfunctionforBigrobotBisTestAudio_types_h_
#define RTW_HEADER_globalfunctionforBigrobotBisTestAudio_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"
#ifndef typedef_raspi_internal_PWMBlock_globa_T
#define typedef_raspi_internal_PWMBlock_globa_T

typedef struct {
  int32_T isInitialized;
  uint32_T PinNumber;
} raspi_internal_PWMBlock_globa_T;

#endif                                 /*typedef_raspi_internal_PWMBlock_globa_T*/

#ifndef typedef_codertarget_linux_blocks_Digi_T
#define typedef_codertarget_linux_blocks_Digi_T

typedef struct {
  int32_T isInitialized;
  real_T SampleTime;
} codertarget_linux_blocks_Digi_T;

#endif                                 /*typedef_codertarget_linux_blocks_Digi_T*/

#ifndef typedef_codertarget_linux_blocks_Di_o_T
#define typedef_codertarget_linux_blocks_Di_o_T

typedef struct {
  int32_T isInitialized;
} codertarget_linux_blocks_Di_o_T;

#endif                                 /*typedef_codertarget_linux_blocks_Di_o_T*/

#ifndef typedef_struct_T_globalfunctionforBig_T
#define typedef_struct_T_globalfunctionforBig_T

typedef struct {
  real_T f1[2];
} struct_T_globalfunctionforBig_T;

#endif                                 /*typedef_struct_T_globalfunctionforBig_T*/

#ifndef typedef_struct_T_globalfunctionforB_o_T
#define typedef_struct_T_globalfunctionforB_o_T

typedef struct {
  char_T f1[7];
} struct_T_globalfunctionforB_o_T;

#endif                                 /*typedef_struct_T_globalfunctionforB_o_T*/

#ifndef typedef_struct_T_globalfunctionfor_oc_T
#define typedef_struct_T_globalfunctionfor_oc_T

typedef struct {
  char_T f1[6];
  char_T f2[2];
  real_T f3;
  char_T f4[2];
  real_T f5;
} struct_T_globalfunctionfor_oc_T;

#endif                                 /*typedef_struct_T_globalfunctionfor_oc_T*/

#ifndef typedef_struct_T_globalfunctionfo_oc2_T
#define typedef_struct_T_globalfunctionfo_oc2_T

typedef struct {
  char_T f1[4];
  char_T f2[11];
  char_T f3[7];
  char_T f4[6];
} struct_T_globalfunctionfo_oc2_T;

#endif                                 /*typedef_struct_T_globalfunctionfo_oc2_T*/

/* Parameters (auto storage) */
typedef struct P_globalfunctionforBigrobotBi_T_ P_globalfunctionforBigrobotBi_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_globalfunctionforBigr_T RT_MODEL_globalfunctionforBig_T;

#endif                                 /* RTW_HEADER_globalfunctionforBigrobotBisTestAudio_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
