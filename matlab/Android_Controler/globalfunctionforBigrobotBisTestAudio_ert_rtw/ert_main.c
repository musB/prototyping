/*
 * File: ert_main.c
 *
 * Code generated for Simulink model 'globalfunctionforBigrobotBisTestAudio'.
 *
 * Model version                  : 1.7
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Dec 18 10:55:01 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include <stdio.h>
#include <stdlib.h>
#include "globalfunctionforBigrobotBisTestAudio.h"
#include "globalfunctionforBigrobotBisTestAudio_private.h"
#include "rtwtypes.h"
#include "limits.h"
#include "rt_nonfinite.h"
#include "linuxinitialize.h"
#define UNUSED(x)                      x = x

/* Function prototype declaration*/
void exitFcn(int sig);
void *terminateTask(void *arg);
void *baseRateTask(void *arg);
void *subrateTask(void *arg);
volatile boolean_T runModel = true;
sem_t stopSem;
sem_t baserateTaskSem;
sem_t subrateTaskSem[4];
int taskId[4];
pthread_t schedulerThread;
pthread_t baseRateThread;
unsigned long threadJoinStatus[8];
int terminatingmodel = 0;
pthread_t subRateThread[4];
int subratePriority[4];
void *subrateTask(void *arg)
{
  int tid = *((int *) arg);
  int subRateId;
  subRateId = tid + 1;
  while (runModel) {
    sem_wait(&subrateTaskSem[tid]);
    if (terminatingmodel)
      break;

#ifdef MW_RTOS_DEBUG

    printf(" -subrate task %d semaphore received\n", subRateId);

#endif

    globalfunctionforBigrobotBisTestAudio_step(subRateId);

    /* Get model outputs here */
  }

  pthread_exit((void *)0);
  return NULL;
}

void *baseRateTask(void *arg)
{
  int_T i;
  runModel = (rtmGetErrorStatus(globalfunctionforBigrobotBis_M) == (NULL)) &&
    !rtmGetStopRequested(globalfunctionforBigrobotBis_M);
  while (runModel) {
    sem_wait(&baserateTaskSem);

#ifdef MW_RTOS_DEBUG

    printf("*base rate task semaphore received\n");
    fflush(stdout);

#endif

    for (i = 1
         ; i <= 4; i++) {
      if (rtmStepTask(globalfunctionforBigrobotBis_M, i)
          ) {
        sem_post(&subrateTaskSem[ i - 1
                 ]);
      }
    }

    /* External mode */
    {
      boolean_T rtmStopReq = false;
      rtExtModePauseIfNeeded(globalfunctionforBigrobotBis_M->extModeInfo, 5,
        &rtmStopReq);
      if (rtmStopReq) {
        rtmSetStopRequested(globalfunctionforBigrobotBis_M, true);
      }

      if (rtmGetStopRequested(globalfunctionforBigrobotBis_M) == true) {
        rtmSetErrorStatus(globalfunctionforBigrobotBis_M, "Simulation finished");
        break;
      }
    }

    /* External mode */
    {
      boolean_T rtmStopReq = false;
      rtExtModeOneStep(globalfunctionforBigrobotBis_M->extModeInfo, 5,
                       &rtmStopReq);
      if (rtmStopReq) {
        rtmSetStopRequested(globalfunctionforBigrobotBis_M, true);
      }
    }

    globalfunctionforBigrobotBisTestAudio_step(0);

    /* Get model outputs here */
    rtExtModeCheckEndTrigger();
    runModel = (rtmGetErrorStatus(globalfunctionforBigrobotBis_M) == (NULL)) &&
      !rtmGetStopRequested(globalfunctionforBigrobotBis_M);
  }

  terminateTask(arg);
  pthread_exit((void *)0);
  return NULL;
}

void exitFcn(int sig)
{
  UNUSED(sig);
  rtmSetErrorStatus(globalfunctionforBigrobotBis_M, "stopping the model");
}

void *terminateTask(void *arg)
{
  UNUSED(arg);
  terminatingmodel = 1;

  {
    int i;

    /* Signal all periodic tasks to complete */
    for (i=0; i<4; i++) {
      CHECK_STATUS(sem_post(&subrateTaskSem[i]), 0, "sem_post");
      CHECK_STATUS(sem_destroy(&subrateTaskSem[i]), 0, "sem_destroy");
    }

    /* Wait for all periodic tasks to complete */
    for (i=0; i<4; i++) {
      CHECK_STATUS(pthread_join(subRateThread[i],(void *)&threadJoinStatus), 0,
                   "pthread_join");
    }

    runModel = 0;
  }

  rtExtModeShutdown(5);

  /* Disable rt_OneStep() here */

  /* Terminate model */
  globalfunctionforBigrobotBisTestAudio_terminate();
  sem_post(&stopSem);
  return NULL;
}

int main(int argc, char **argv)
{
  UNUSED(argc);
  UNUSED(argv);
  subratePriority[0] = 39;
  subratePriority[1] = 38;
  subratePriority[2] = 37;
  subratePriority[3] = 36;
  rtmSetErrorStatus(globalfunctionforBigrobotBis_M, 0);
  rtExtModeParseArgs(argc, (const char_T **)argv, NULL);

  /* Initialize model */
  globalfunctionforBigrobotBisTestAudio_initialize();

  /* External mode */
  rtSetTFinalForExtMode(&rtmGetTFinal(globalfunctionforBigrobotBis_M));
  rtExtModeCheckInit(5);

  {
    boolean_T rtmStopReq = false;
    rtExtModeWaitForStartPkt(globalfunctionforBigrobotBis_M->extModeInfo, 5,
      &rtmStopReq);
    if (rtmStopReq) {
      rtmSetStopRequested(globalfunctionforBigrobotBis_M, true);
    }
  }

  rtERTExtModeStartMsg();

  /* Call RTOS Initialization function */
  myRTOSInit(2.2675736961451247E-6, 4);

  /* Wait for stop semaphore */
  sem_wait(&stopSem);
  return 0;
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
