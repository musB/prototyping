/*
 * File: globalfunctionforBigrobotBisTestAudio.h
 *
 * Code generated for Simulink model 'globalfunctionforBigrobotBisTestAudio'.
 *
 * Model version                  : 1.7
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Dec 18 10:55:01 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_globalfunctionforBigrobotBisTestAudio_h_
#define RTW_HEADER_globalfunctionforBigrobotBisTestAudio_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef globalfunctionforBigrobotBisTestAudio_COMMON_INCLUDES_
# define globalfunctionforBigrobotBisTestAudio_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "MW_pigs.h"
#include "MW_gpio.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* globalfunctionforBigrobotBisTestAudio_COMMON_INCLUDES_ */

#include "globalfunctionforBigrobotBisTestAudio_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_zcfcn.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

#define globalfunctionforBigrobotBisTestAudio_M (globalfunctionforBigrobotBis_M)

/* Block signals (auto storage) */
typedef struct {
  real32_T MatrixConcatenate_m[44100]; /* '<S36>/Matrix Concatenate' */
  int16_T Switch[44100];               /* '<Root>/Switch' */
  int16_T Buffer[22050];               /* '<S25>/Buffer' */
  int16_T Buffer_h[22050];             /* '<S27>/Buffer' */
  int16_T Buffer_o[22050];             /* '<S28>/Buffer' */
  int16_T Buffer_k[22050];             /* '<S26>/Buffer' */
  char_T cv0[45];
  real_T Constant;                     /* '<Root>/Constant' */
  real_T Diametredurobot;              /* '<Root>/Diametre du robot' */
  real_T RateTransition3;              /* '<Root>/Rate Transition3' */
  real_T RateTransition;               /* '<Root>/Rate Transition' */
  real_T ButtonUp;                     /* '<Root>/Button Up' */
  real_T ButtonRight;                  /* '<Root>/Button Right' */
  real_T ButtonDown;                   /* '<Root>/Button Down' */
  real_T ButtonLeft;                   /* '<Root>/Button Left' */
  real_T ButtonMiddle;                 /* '<Root>/Button Middle' */
  real_T RateTransition1;              /* '<Root>/Rate Transition1' */
  real_T direction;                    /* '<Root>/GeometricalShape' */
  real_T directionD;                   /* '<Root>/GeometricalShape' */
  real_T speed;                        /* '<Root>/GeometricalShape' */
  real_T speed2;                       /* '<Root>/GeometricalShape' */
  real_T reset;                        /* '<Root>/GeometricalShape' */
  real_T Sel1;                         /* '<Root>/GeometricalShape' */
  real_T Sel2;                         /* '<Root>/GeometricalShape' */
  real_T Sel0;                         /* '<Root>/GeometricalShape' */
  real_T sound;                        /* '<Root>/GeometricalShape' */
  real_T sampleTime;
  real_T sampleTime_m;
  int16_T FromWorkspace;               /* '<S25>/From Workspace' */
  int16_T FromWorkspace_l;             /* '<S27>/From Workspace' */
  int16_T FromWorkspace_h;             /* '<S28>/From Workspace' */
  int16_T FromWorkspace_m;             /* '<S26>/From Workspace' */
  boolean_T DigitalRead;               /* '<S23>/Digital Read' */
  boolean_T DigitalRead_j;             /* '<S19>/Digital Read' */
  boolean_T DigitalRead_jw;            /* '<S18>/Digital Read' */
  boolean_T DigitalRead_g;             /* '<S17>/Digital Read' */
  boolean_T DigitalRead_h;             /* '<S16>/Digital Read' */
  boolean_T DigitalRead_f;             /* '<S11>/Digital Read' */
  boolean_T DigitalRead_hi;            /* '<S2>/Digital Read' */
} B_globalfunctionforBigrobotBi_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  codertarget_linux_blocks_Digi_T obj; /* '<S23>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_g;/* '<S19>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_a;/* '<S18>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_n;/* '<S17>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_b;/* '<S16>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_o;/* '<S11>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_k;/* '<S4>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_bf;/* '<S3>/Digital Read' */
  codertarget_linux_blocks_Digi_T obj_af;/* '<S2>/Digital Read' */
  real_T RateTransition3_Buffer0;      /* '<Root>/Rate Transition3' */
  real_T p;                            /* '<S20>/Counter' */
  real_T Stop;                         /* '<Root>/GeometricalShape' */
  real_T Q;                            /* '<Root>/GeometricalShape' */
  real_T P;                            /* '<Root>/GeometricalShape' */
  real_T counter;                      /* '<Root>/GeometricalShape' */
  raspi_internal_PWMBlock_globa_T obj_nv;/* '<Root>/Enable Left' */
  raspi_internal_PWMBlock_globa_T obj_l;/* '<Root>/Enable Right' */
  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK;               /* '<S25>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_j;             /* '<S27>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_g;             /* '<S28>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_c;             /* '<S26>/From Workspace' */

  void *DigitalRead_PWORK;             /* '<S23>/Digital Read' */
  void *DigitalWrite_PWORK;            /* '<S22>/Digital Write' */
  void *DigitalRead_PWORK_o;           /* '<S19>/Digital Read' */
  void *DigitalRead_PWORK_a;           /* '<S18>/Digital Read' */
  void *DigitalRead_PWORK_p;           /* '<S17>/Digital Read' */
  void *DigitalRead_PWORK_l;           /* '<S16>/Digital Read' */
  void *DigitalRead_PWORK_n;           /* '<S11>/Digital Read' */
  void *DigitalWrite_PWORK_j;          /* '<S10>/Digital Write' */
  void *DigitalWrite_PWORK_f;          /* '<S7>/Digital Write' */
  void *DigitalWrite_PWORK_c;          /* '<S6>/Digital Write' */
  void *DigitalWrite_PWORK_p;          /* '<S5>/Digital Write' */
  void *DigitalRead_PWORK_f;           /* '<S4>/Digital Read' */
  void *DigitalRead_PWORK_b;           /* '<S3>/Digital Read' */
  void *DigitalRead_PWORK_pm;          /* '<S2>/Digital Read' */
  void *EnableLeft_PWORK;              /* '<Root>/Enable Left' */
  void *EnableRight_PWORK;             /* '<Root>/Enable Right' */
  int32_T Buffer_inBufPtrIdx;          /* '<S25>/Buffer' */
  int32_T Buffer_outBufPtrIdx;         /* '<S25>/Buffer' */
  int32_T Buffer_inBufPtrIdx_p;        /* '<S27>/Buffer' */
  int32_T Buffer_outBufPtrIdx_j;       /* '<S27>/Buffer' */
  int32_T Buffer_inBufPtrIdx_g;        /* '<S28>/Buffer' */
  int32_T Buffer_outBufPtrIdx_d;       /* '<S28>/Buffer' */
  int32_T Buffer_inBufPtrIdx_k;        /* '<S26>/Buffer' */
  int32_T Buffer_outBufPtrIdx_o;       /* '<S26>/Buffer' */
  codertarget_linux_blocks_Di_o_T obj_f;/* '<S22>/Digital Write' */
  codertarget_linux_blocks_Di_o_T obj_le;/* '<S10>/Digital Write' */
  codertarget_linux_blocks_Di_o_T obj_i;/* '<S7>/Digital Write' */
  codertarget_linux_blocks_Di_o_T obj_h;/* '<S6>/Digital Write' */
  codertarget_linux_blocks_Di_o_T obj_hq;/* '<S5>/Digital Write' */
  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK;               /* '<S25>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_n;             /* '<S27>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_h;             /* '<S28>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_e;             /* '<S26>/From Workspace' */

  int16_T Buffer_CircBuf[44100];       /* '<S25>/Buffer' */
  int16_T Buffer_CircBuf_m[44100];     /* '<S27>/Buffer' */
  int16_T Buffer_CircBuf_p[44100];     /* '<S28>/Buffer' */
  int16_T Buffer_CircBuf_h[44100];     /* '<S26>/Buffer' */
  uint8_T is_active_c2_globalfunctionforB;/* '<Root>/GeometricalShape' */
  uint8_T is_c2_globalfunctionforBigrobot;/* '<Root>/GeometricalShape' */
  uint8_T is_LINE_FOLLOWER_IN;         /* '<Root>/GeometricalShape' */
  uint8_T is_GeometricalShape;         /* '<Root>/GeometricalShape' */
  uint8_T is_Triangle1;                /* '<Root>/GeometricalShape' */
  uint8_T is_StraightLine;             /* '<Root>/GeometricalShape' */
  uint8_T is_Square;                   /* '<Root>/GeometricalShape' */
  uint8_T is_Circle;                   /* '<Root>/GeometricalShape' */
  uint8_T is_ObstacleAvoidance;        /* '<Root>/GeometricalShape' */
  uint8_T is_Joystick;                 /* '<Root>/GeometricalShape' */
  uint8_T temporalCounter_i1;          /* '<Root>/GeometricalShape' */
  boolean_T p_not_empty;               /* '<S20>/Counter' */
  boolean_T AA;                        /* '<S20>/Counter' */
} DW_globalfunctionforBigrobotB_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState ResettableSubsystem_Reset_ZCE;/* '<Root>/Resettable Subsystem' */
} PrevZCX_globalfunctionforBigr_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: device
   * Referenced by: '<Root>/ALSA Audio Playback8'
   */
  uint8_T ALSAAudioPlayback8_p1[8];
} ConstP_globalfunctionforBigro_T;

/* Parameters (auto storage) */
struct P_globalfunctionforBigrobotBi_T_ {
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<Root>/Switch'
                                        */
  real_T Switch1_Threshold;            /* Expression: 1
                                        * Referenced by: '<Root>/Switch1'
                                        */
  real_T DigitalRead_SampleTime;       /* Expression: sampleTime
                                        * Referenced by: '<S2>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_n;     /* Expression: sampleTime
                                        * Referenced by: '<S3>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_f;     /* Expression: sampleTime
                                        * Referenced by: '<S4>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_e;     /* Expression: sampleTime
                                        * Referenced by: '<S11>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_et;    /* Expression: sampleTime
                                        * Referenced by: '<S16>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_k;     /* Expression: sampleTime
                                        * Referenced by: '<S17>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_p;     /* Expression: sampleTime
                                        * Referenced by: '<S18>/Digital Read'
                                        */
  real_T DigitalRead_SampleTime_kv;    /* Expression: sampleTime
                                        * Referenced by: '<S19>/Digital Read'
                                        */
  real_T Gain_Gain;                    /* Expression: 2*pi
                                        * Referenced by: '<S20>/Gain'
                                        */
  real_T DigitalRead_SampleTime_j;     /* Expression: sampleTime
                                        * Referenced by: '<S23>/Digital Read'
                                        */
  real_T Constant_Value;               /* Expression: 0.5
                                        * Referenced by: '<Root>/Constant'
                                        */
  real_T Diametredurobot_Value;        /* Expression: 0.18
                                        * Referenced by: '<Root>/Diametre du robot'
                                        */
  real_T MototEncoderRatio_Value;      /* Expression: 159
                                        * Referenced by: '<Root>/Motot+Encoder Ratio'
                                        */
  real_T Wheelradius_Value;            /* Expression: 3.5/100
                                        * Referenced by: '<Root>/Wheel radius'
                                        */
  real_T RateTransition3_X0;           /* Expression: 0
                                        * Referenced by: '<Root>/Rate Transition3'
                                        */
  real_T ButtonUp_Value;               /* Expression: 0
                                        * Referenced by: '<Root>/Button Up'
                                        */
  real_T ButtonRight_Value;            /* Expression: 0
                                        * Referenced by: '<Root>/Button Right'
                                        */
  real_T ButtonDown_Value;             /* Expression: 0
                                        * Referenced by: '<Root>/Button Down'
                                        */
  real_T ButtonLeft_Value;             /* Expression: 0
                                        * Referenced by: '<Root>/Button Left'
                                        */
  real_T ButtonMiddle_Value;           /* Expression: 0
                                        * Referenced by: '<Root>/Button Middle'
                                        */
  real_T Switch2_Threshold;            /* Expression: 2
                                        * Referenced by: '<Root>/Switch2'
                                        */
  real32_T Gain_Gain_m;                /* Computed Parameter: Gain_Gain_m
                                        * Referenced by: '<S29>/Gain'
                                        */
  real32_T Gain_Gain_j;                /* Computed Parameter: Gain_Gain_j
                                        * Referenced by: '<S30>/Gain'
                                        */
  real32_T Gain_Gain_jh;               /* Computed Parameter: Gain_Gain_jh
                                        * Referenced by: '<S31>/Gain'
                                        */
  real32_T Gain_Gain_g;                /* Computed Parameter: Gain_Gain_g
                                        * Referenced by: '<S32>/Gain'
                                        */
  int16_T Buffer_ic[22050];            /* Computed Parameter: Buffer_ic
                                        * Referenced by: '<S25>/Buffer'
                                        */
  int16_T Buffer_ic_f[22050];          /* Computed Parameter: Buffer_ic_f
                                        * Referenced by: '<S27>/Buffer'
                                        */
  int16_T Buffer_ic_a[22050];          /* Computed Parameter: Buffer_ic_a
                                        * Referenced by: '<S28>/Buffer'
                                        */
  int16_T Buffer_ic_e[22050];          /* Computed Parameter: Buffer_ic_e
                                        * Referenced by: '<S26>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_globalfunctionforBigr_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTick2;
    uint32_T clockTick3;
    uint32_T clockTick4;
    struct {
      uint32_T TID[5];
    } TaskCounters;

    struct {
      boolean_T TID2_3;
      boolean_T TID3_4;
    } RateInteraction;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_globalfunctionforBigrobotBi_T globalfunctionforBigrobotBisT_P;

/* Block signals (auto storage) */
extern B_globalfunctionforBigrobotBi_T globalfunctionforBigrobotBisT_B;

/* Block states (auto storage) */
extern DW_globalfunctionforBigrobotB_T globalfunctionforBigrobotBis_DW;

/* External data declarations for dependent source files */

/* Zero-crossing (trigger) state */
extern PrevZCX_globalfunctionforBigr_T globalfunctionforBigrob_PrevZCX;

/* Constant parameters (auto storage) */
extern const ConstP_globalfunctionforBigro_T globalfunctionforBigrobo_ConstP;

/* External function called from main */
extern void globalfunctionforBigrobotBisTestAudio_SetEventsForThisBaseStep
  (boolean_T *eventFlags);

/* Model entry point functions */
extern void globalfunctionforBigrobotBisTestAudio_SetEventsForThisBaseStep
  (boolean_T *eventFlags);
extern void globalfunctionforBigrobotBisTestAudio_initialize(void);
extern void globalfunctionforBigrobotBisTestAudio_step(int_T tid);
extern void globalfunctionforBigrobotBisTestAudio_terminate(void);

/* Real-time Model object */
extern RT_MODEL_globalfunctionforBig_T *const globalfunctionforBigrobotBis_M;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<Root>/ReferenceSpeed' : Unused code path elimination
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'globalfunctionforBigrobotBisTestAudio'
 * '<S1>'   : 'globalfunctionforBigrobotBisTestAudio/Down'
 * '<S2>'   : 'globalfunctionforBigrobotBisTestAudio/East'
 * '<S3>'   : 'globalfunctionforBigrobotBisTestAudio/Encoder Right Motor'
 * '<S4>'   : 'globalfunctionforBigrobotBisTestAudio/Encoder left Motor'
 * '<S5>'   : 'globalfunctionforBigrobotBisTestAudio/GPIO Write'
 * '<S6>'   : 'globalfunctionforBigrobotBisTestAudio/GPIO Write1'
 * '<S7>'   : 'globalfunctionforBigrobotBisTestAudio/GPIO Write2'
 * '<S8>'   : 'globalfunctionforBigrobotBisTestAudio/GeometricalShape'
 * '<S9>'   : 'globalfunctionforBigrobotBisTestAudio/Left'
 * '<S10>'  : 'globalfunctionforBigrobotBisTestAudio/Left motor direction'
 * '<S11>'  : 'globalfunctionforBigrobotBisTestAudio/Left_Sensor'
 * '<S12>'  : 'globalfunctionforBigrobotBisTestAudio/Middle'
 * '<S13>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control'
 * '<S14>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control1'
 * '<S15>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control2'
 * '<S16>'  : 'globalfunctionforBigrobotBisTestAudio/North'
 * '<S17>'  : 'globalfunctionforBigrobotBisTestAudio/North-West'
 * '<S18>'  : 'globalfunctionforBigrobotBisTestAudio/North-east'
 * '<S19>'  : 'globalfunctionforBigrobotBisTestAudio/Ouest'
 * '<S20>'  : 'globalfunctionforBigrobotBisTestAudio/Resettable Subsystem'
 * '<S21>'  : 'globalfunctionforBigrobotBisTestAudio/Right'
 * '<S22>'  : 'globalfunctionforBigrobotBisTestAudio/Right motor Direction'
 * '<S23>'  : 'globalfunctionforBigrobotBisTestAudio/Right_Sensor 1'
 * '<S24>'  : 'globalfunctionforBigrobotBisTestAudio/SDL Video Display'
 * '<S25>'  : 'globalfunctionforBigrobotBisTestAudio/Signal From Workspace1'
 * '<S26>'  : 'globalfunctionforBigrobotBisTestAudio/Signal From Workspace10'
 * '<S27>'  : 'globalfunctionforBigrobotBisTestAudio/Signal From Workspace12'
 * '<S28>'  : 'globalfunctionforBigrobotBisTestAudio/Signal From Workspace9'
 * '<S29>'  : 'globalfunctionforBigrobotBisTestAudio/Sum left & right channels  and to single1'
 * '<S30>'  : 'globalfunctionforBigrobotBisTestAudio/Sum left & right channels  and to single3'
 * '<S31>'  : 'globalfunctionforBigrobotBisTestAudio/Sum left & right channels  and to single5'
 * '<S32>'  : 'globalfunctionforBigrobotBisTestAudio/Sum left & right channels  and to single6'
 * '<S33>'  : 'globalfunctionforBigrobotBisTestAudio/Up'
 * '<S34>'  : 'globalfunctionforBigrobotBisTestAudio/Write to DAC1'
 * '<S35>'  : 'globalfunctionforBigrobotBisTestAudio/Write to DAC3'
 * '<S36>'  : 'globalfunctionforBigrobotBisTestAudio/Write to DAC4'
 * '<S37>'  : 'globalfunctionforBigrobotBisTestAudio/Write to DAC5'
 * '<S38>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control/Frequency Estimator'
 * '<S39>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control/Speed Controller'
 * '<S40>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control/Speed Estimator'
 * '<S41>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control/Frequency Estimator/Triggered Subsystem'
 * '<S42>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control1/Frequency Estimator'
 * '<S43>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control1/Speed Controller'
 * '<S44>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control1/Speed Estimator'
 * '<S45>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control1/Frequency Estimator/Triggered Subsystem'
 * '<S46>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control2/Frequency Estimator'
 * '<S47>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control2/Speed Controller'
 * '<S48>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control2/Speed Estimator'
 * '<S49>'  : 'globalfunctionforBigrobotBisTestAudio/Motor Speed Control2/Frequency Estimator/Triggered Subsystem'
 * '<S50>'  : 'globalfunctionforBigrobotBisTestAudio/Resettable Subsystem/Counter'
 */
#endif                                 /* RTW_HEADER_globalfunctionforBigrobotBisTestAudio_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
