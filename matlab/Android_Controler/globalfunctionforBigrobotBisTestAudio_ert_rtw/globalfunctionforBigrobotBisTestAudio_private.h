/*
 * File: globalfunctionforBigrobotBisTestAudio_private.h
 *
 * Code generated for Simulink model 'globalfunctionforBigrobotBisTestAudio'.
 *
 * Model version                  : 1.7
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Mon Dec 18 10:55:01 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_globalfunctionforBigrobotBisTestAudio_private_h_
#define RTW_HEADER_globalfunctionforBigrobotBisTestAudio_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"
#include "globalfunctionforBigrobotBisTestAudio.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

/* Used by FromWorkspace Block: '<S25>/From Workspace' */
#ifndef rtInterpolate
# define rtInterpolate(v1,v2,f1,f2)    (((v1)==(v2))?((double)(v1)): (((f1)*((double)(v1)))+((f2)*((double)(v2)))))
#endif

#ifndef rtRound
# define rtRound(v)                    ( ((v) >= 0) ? floor((v) + 0.5) : ceil((v) - 0.5) )
#endif

extern real_T rt_roundd_snf(real_T u);
extern void globalfunctionforBigrobotBisTestAudio_step0(void);
extern void globalfunctionforBigrobotBisTestAudio_step1(void);
extern void globalfunctionforBigrobotBisTestAudio_step2(void);
extern void globalfunctionforBigrobotBisTestAudio_step3(void);
extern void globalfunctionforBigrobotBisTestAudio_step4(void);

#endif                                 /* RTW_HEADER_globalfunctionforBigrobotBisTestAudio_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
