  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 3;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (globalfunctionforBigrobotBisT_P)
    ;%
      section.nData     = 23;
      section.data(23)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_P.Switch_Threshold
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_P.Switch1_Threshold
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_n
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_f
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_e
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_et
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_k
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_p
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_kv
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% globalfunctionforBigrobotBisT_P.Gain_Gain
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% globalfunctionforBigrobotBisT_P.DigitalRead_SampleTime_j
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% globalfunctionforBigrobotBisT_P.Constant_Value
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% globalfunctionforBigrobotBisT_P.Diametredurobot_Value
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% globalfunctionforBigrobotBisT_P.MototEncoderRatio_Value
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 14;
	
	  ;% globalfunctionforBigrobotBisT_P.Wheelradius_Value
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 15;
	
	  ;% globalfunctionforBigrobotBisT_P.RateTransition3_X0
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 16;
	
	  ;% globalfunctionforBigrobotBisT_P.ButtonUp_Value
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 17;
	
	  ;% globalfunctionforBigrobotBisT_P.ButtonRight_Value
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 18;
	
	  ;% globalfunctionforBigrobotBisT_P.ButtonDown_Value
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 19;
	
	  ;% globalfunctionforBigrobotBisT_P.ButtonLeft_Value
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 20;
	
	  ;% globalfunctionforBigrobotBisT_P.ButtonMiddle_Value
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 21;
	
	  ;% globalfunctionforBigrobotBisT_P.Switch2_Threshold
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 22;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_P.Gain_Gain_m
	  section.data(1).logicalSrcIdx = 23;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_P.Gain_Gain_j
	  section.data(2).logicalSrcIdx = 24;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBisT_P.Gain_Gain_jh
	  section.data(3).logicalSrcIdx = 25;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBisT_P.Gain_Gain_g
	  section.data(4).logicalSrcIdx = 26;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_P.Buffer_ic
	  section.data(1).logicalSrcIdx = 27;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_P.Buffer_ic_f
	  section.data(2).logicalSrcIdx = 28;
	  section.data(2).dtTransOffset = 22050;
	
	  ;% globalfunctionforBigrobotBisT_P.Buffer_ic_a
	  section.data(3).logicalSrcIdx = 29;
	  section.data(3).dtTransOffset = 44100;
	
	  ;% globalfunctionforBigrobotBisT_P.Buffer_ic_e
	  section.data(4).logicalSrcIdx = 30;
	  section.data(4).dtTransOffset = 66150;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 3;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (globalfunctionforBigrobotBisT_B)
    ;%
      section.nData     = 19;
      section.data(19)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_B.Constant
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_B.Diametredurobot
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBisT_B.RateTransition3
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBisT_B.RateTransition
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBisT_B.ButtonUp
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBisT_B.ButtonRight
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBisT_B.ButtonDown
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBisT_B.ButtonLeft
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% globalfunctionforBigrobotBisT_B.ButtonMiddle
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% globalfunctionforBigrobotBisT_B.RateTransition1
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% globalfunctionforBigrobotBisT_B.direction
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% globalfunctionforBigrobotBisT_B.directionD
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% globalfunctionforBigrobotBisT_B.speed
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% globalfunctionforBigrobotBisT_B.speed2
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% globalfunctionforBigrobotBisT_B.reset
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 14;
	
	  ;% globalfunctionforBigrobotBisT_B.Sel1
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 15;
	
	  ;% globalfunctionforBigrobotBisT_B.Sel2
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 16;
	
	  ;% globalfunctionforBigrobotBisT_B.Sel0
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 17;
	
	  ;% globalfunctionforBigrobotBisT_B.sound
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 18;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_B.FromWorkspace
	  section.data(1).logicalSrcIdx = 19;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_B.FromWorkspace_l
	  section.data(2).logicalSrcIdx = 20;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBisT_B.FromWorkspace_h
	  section.data(3).logicalSrcIdx = 21;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBisT_B.FromWorkspace_m
	  section.data(4).logicalSrcIdx = 22;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead
	  section.data(1).logicalSrcIdx = 23;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_j
	  section.data(2).logicalSrcIdx = 24;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_jw
	  section.data(3).logicalSrcIdx = 25;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_g
	  section.data(4).logicalSrcIdx = 26;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_h
	  section.data(5).logicalSrcIdx = 27;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_f
	  section.data(6).logicalSrcIdx = 28;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBisT_B.DigitalRead_hi
	  section.data(7).logicalSrcIdx = 29;
	  section.data(7).dtTransOffset = 6;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 10;
    sectIdxOffset = 3;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (globalfunctionforBigrobotBis_DW)
    ;%
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.obj
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_g
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_a
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_n
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_b
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_o
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_k
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_bf
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_af
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.RateTransition3_Buffer0
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.p
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.Stop
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.Q
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.P
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBis_DW.counter
	  section.data(6).logicalSrcIdx = 14;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.obj_nv
	  section.data(1).logicalSrcIdx = 15;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_l
	  section.data(2).logicalSrcIdx = 16;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 20;
      section.data(20)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_PWORK.TimePtr
	  section.data(1).logicalSrcIdx = 17;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_PWORK_j.TimePtr
	  section.data(2).logicalSrcIdx = 18;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_PWORK_g.TimePtr
	  section.data(3).logicalSrcIdx = 19;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_PWORK_c.TimePtr
	  section.data(4).logicalSrcIdx = 20;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK
	  section.data(5).logicalSrcIdx = 21;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalWrite_PWORK
	  section.data(6).logicalSrcIdx = 22;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_o
	  section.data(7).logicalSrcIdx = 23;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_a
	  section.data(8).logicalSrcIdx = 24;
	  section.data(8).dtTransOffset = 7;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_p
	  section.data(9).logicalSrcIdx = 25;
	  section.data(9).dtTransOffset = 8;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_l
	  section.data(10).logicalSrcIdx = 26;
	  section.data(10).dtTransOffset = 9;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_n
	  section.data(11).logicalSrcIdx = 27;
	  section.data(11).dtTransOffset = 10;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalWrite_PWORK_j
	  section.data(12).logicalSrcIdx = 28;
	  section.data(12).dtTransOffset = 11;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalWrite_PWORK_f
	  section.data(13).logicalSrcIdx = 29;
	  section.data(13).dtTransOffset = 12;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalWrite_PWORK_c
	  section.data(14).logicalSrcIdx = 30;
	  section.data(14).dtTransOffset = 13;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalWrite_PWORK_p
	  section.data(15).logicalSrcIdx = 31;
	  section.data(15).dtTransOffset = 14;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_f
	  section.data(16).logicalSrcIdx = 32;
	  section.data(16).dtTransOffset = 15;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_b
	  section.data(17).logicalSrcIdx = 33;
	  section.data(17).dtTransOffset = 16;
	
	  ;% globalfunctionforBigrobotBis_DW.DigitalRead_PWORK_pm
	  section.data(18).logicalSrcIdx = 34;
	  section.data(18).dtTransOffset = 17;
	
	  ;% globalfunctionforBigrobotBis_DW.EnableLeft_PWORK
	  section.data(19).logicalSrcIdx = 35;
	  section.data(19).dtTransOffset = 18;
	
	  ;% globalfunctionforBigrobotBis_DW.EnableRight_PWORK
	  section.data(20).logicalSrcIdx = 36;
	  section.data(20).dtTransOffset = 19;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.Buffer_inBufPtrIdx
	  section.data(1).logicalSrcIdx = 37;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_outBufPtrIdx
	  section.data(2).logicalSrcIdx = 38;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_inBufPtrIdx_p
	  section.data(3).logicalSrcIdx = 40;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_outBufPtrIdx_j
	  section.data(4).logicalSrcIdx = 41;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_inBufPtrIdx_g
	  section.data(5).logicalSrcIdx = 43;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_outBufPtrIdx_d
	  section.data(6).logicalSrcIdx = 44;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_inBufPtrIdx_k
	  section.data(7).logicalSrcIdx = 46;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_outBufPtrIdx_o
	  section.data(8).logicalSrcIdx = 47;
	  section.data(8).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.obj_f
	  section.data(1).logicalSrcIdx = 50;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_le
	  section.data(2).logicalSrcIdx = 51;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_i
	  section.data(3).logicalSrcIdx = 52;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_h
	  section.data(4).logicalSrcIdx = 53;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.obj_hq
	  section.data(5).logicalSrcIdx = 54;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_IWORK.PrevIndex
	  section.data(1).logicalSrcIdx = 55;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_IWORK_n.PrevIndex
	  section.data(2).logicalSrcIdx = 56;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_IWORK_h.PrevIndex
	  section.data(3).logicalSrcIdx = 57;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.FromWorkspace_IWORK_e.PrevIndex
	  section.data(4).logicalSrcIdx = 58;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.Buffer_CircBuf
	  section.data(1).logicalSrcIdx = 59;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_CircBuf_m
	  section.data(2).logicalSrcIdx = 60;
	  section.data(2).dtTransOffset = 44100;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_CircBuf_p
	  section.data(3).logicalSrcIdx = 61;
	  section.data(3).dtTransOffset = 88200;
	
	  ;% globalfunctionforBigrobotBis_DW.Buffer_CircBuf_h
	  section.data(4).logicalSrcIdx = 62;
	  section.data(4).dtTransOffset = 132300;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 11;
      section.data(11)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.is_active_c2_globalfunctionforB
	  section.data(1).logicalSrcIdx = 63;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.is_c2_globalfunctionforBigrobot
	  section.data(2).logicalSrcIdx = 64;
	  section.data(2).dtTransOffset = 1;
	
	  ;% globalfunctionforBigrobotBis_DW.is_LINE_FOLLOWER_IN
	  section.data(3).logicalSrcIdx = 65;
	  section.data(3).dtTransOffset = 2;
	
	  ;% globalfunctionforBigrobotBis_DW.is_GeometricalShape
	  section.data(4).logicalSrcIdx = 66;
	  section.data(4).dtTransOffset = 3;
	
	  ;% globalfunctionforBigrobotBis_DW.is_Triangle1
	  section.data(5).logicalSrcIdx = 67;
	  section.data(5).dtTransOffset = 4;
	
	  ;% globalfunctionforBigrobotBis_DW.is_StraightLine
	  section.data(6).logicalSrcIdx = 68;
	  section.data(6).dtTransOffset = 5;
	
	  ;% globalfunctionforBigrobotBis_DW.is_Square
	  section.data(7).logicalSrcIdx = 69;
	  section.data(7).dtTransOffset = 6;
	
	  ;% globalfunctionforBigrobotBis_DW.is_Circle
	  section.data(8).logicalSrcIdx = 70;
	  section.data(8).dtTransOffset = 7;
	
	  ;% globalfunctionforBigrobotBis_DW.is_ObstacleAvoidance
	  section.data(9).logicalSrcIdx = 71;
	  section.data(9).dtTransOffset = 8;
	
	  ;% globalfunctionforBigrobotBis_DW.is_Joystick
	  section.data(10).logicalSrcIdx = 72;
	  section.data(10).dtTransOffset = 9;
	
	  ;% globalfunctionforBigrobotBis_DW.temporalCounter_i1
	  section.data(11).logicalSrcIdx = 73;
	  section.data(11).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% globalfunctionforBigrobotBis_DW.p_not_empty
	  section.data(1).logicalSrcIdx = 76;
	  section.data(1).dtTransOffset = 0;
	
	  ;% globalfunctionforBigrobotBis_DW.AA
	  section.data(2).logicalSrcIdx = 77;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 3391929878;
  targMap.checksum1 = 1642553410;
  targMap.checksum2 = 695334666;
  targMap.checksum3 = 1314981449;

