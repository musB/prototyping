package com.example.android_modified;


interface OnFragmentInteractionListener {
   void onFragmentCreate (String name);
   void onFragmentStart  (String name);
   void onFragmentPause  (String name);
   void onFragmentResume (String name);
}
