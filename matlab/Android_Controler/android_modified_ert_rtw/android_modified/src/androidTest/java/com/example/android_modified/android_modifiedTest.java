package com.example.android_modified;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class com.example.android_modified.android_modifiedTest \
 * com.example.android_modified.tests/android.test.InstrumentationTestRunner
 */
public class android_modifiedTest extends ActivityInstrumentationTestCase2<android_modified> {

    public android_modifiedTest() {
        super("com.example.android_modified", android_modified.class);
    }

}
