/*
 * File: untitled1_types.h
 *
 * Code generated for Simulink model 'untitled1'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Thu Dec 14 14:37:28 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_untitled1_types_h_
#define RTW_HEADER_untitled1_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_untitled1_T_ P_untitled1_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_untitled1_T RT_MODEL_untitled1_T;

#endif                                 /* RTW_HEADER_untitled1_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
