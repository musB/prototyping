function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/ALSA Audio Playback8 */
	this.urlHashMap["AudioWorkingCode_1:1958"] = "AudioWorkingCode_1.c:563,50173,50211&AudioWorkingCode_1.h:157&AudioWorkingCode_1_data.c:6714";
	/* <Root>/Constant5 */
	this.urlHashMap["AudioWorkingCode_1:1983"] = "AudioWorkingCode_1.c:418&AudioWorkingCode_1.h:171&AudioWorkingCode_1_data.c:28";
	/* <Root>/Constant6 */
	this.urlHashMap["AudioWorkingCode_1:1984"] = "AudioWorkingCode_1.c:411&AudioWorkingCode_1.h:165&AudioWorkingCode_1_data.c:22";
	/* <Root>/Constant7 */
	this.urlHashMap["AudioWorkingCode_1:2011"] = "AudioWorkingCode_1.c:412&AudioWorkingCode_1.h:177&AudioWorkingCode_1_data.c:34";
	/* <Root>/Signal From
Workspace10 */
	this.urlHashMap["AudioWorkingCode_1:1971"] = "msg=&block=AudioWorkingCode_1:1971";
	/* <Root>/Signal From
Workspace11 */
	this.urlHashMap["AudioWorkingCode_1:1985"] = "msg=&block=AudioWorkingCode_1:1985";
	/* <Root>/Signal From
Workspace12 */
	this.urlHashMap["AudioWorkingCode_1:1986"] = "msg=&block=AudioWorkingCode_1:1986";
	/* <Root>/Signal From
Workspace9 */
	this.urlHashMap["AudioWorkingCode_1:1959"] = "msg=&block=AudioWorkingCode_1:1959";
	/* <Root>/Sum left & right channels 
and to single3 */
	this.urlHashMap["AudioWorkingCode_1:1960"] = "msg=&block=AudioWorkingCode_1:1960";
	/* <Root>/Sum left & right channels 
and to single4 */
	this.urlHashMap["AudioWorkingCode_1:1972"] = "msg=&block=AudioWorkingCode_1:1972";
	/* <Root>/Sum left & right channels 
and to single5 */
	this.urlHashMap["AudioWorkingCode_1:1987"] = "msg=&block=AudioWorkingCode_1:1987";
	/* <Root>/Sum left & right channels 
and to single6 */
	this.urlHashMap["AudioWorkingCode_1:1993"] = "msg=&block=AudioWorkingCode_1:1993";
	/* <Root>/Switch */
	this.urlHashMap["AudioWorkingCode_1:1957"] = "AudioWorkingCode_1.c:417,483&AudioWorkingCode_1.h:87,174&AudioWorkingCode_1_data.c:31";
	/* <Root>/Switch1 */
	this.urlHashMap["AudioWorkingCode_1:1999"] = "AudioWorkingCode_1.c:413,490,496,501,506,527,533,538,543&AudioWorkingCode_1.h:168&AudioWorkingCode_1_data.c:25";
	/* <Root>/Switch2 */
	this.urlHashMap["AudioWorkingCode_1:2010"] = "AudioWorkingCode_1.c:410,561&AudioWorkingCode_1.h:180&AudioWorkingCode_1_data.c:37";
	/* <Root>/Write to
DAC3 */
	this.urlHashMap["AudioWorkingCode_1:1966"] = "msg=&block=AudioWorkingCode_1:1966";
	/* <Root>/Write to
DAC4 */
	this.urlHashMap["AudioWorkingCode_1:1978"] = "msg=&block=AudioWorkingCode_1:1978";
	/* <Root>/Write to
DAC5 */
	this.urlHashMap["AudioWorkingCode_1:2000"] = "msg=&block=AudioWorkingCode_1:2000";
	/* <Root>/Write to
DAC6 */
	this.urlHashMap["AudioWorkingCode_1:2005"] = "msg=&block=AudioWorkingCode_1:2005";
	/* <S1>/Buffer */
	this.urlHashMap["AudioWorkingCode_1:1971:10"] = "AudioWorkingCode_1.c:140,163,323,350,50181,50193&AudioWorkingCode_1.h:89,126,127,149,198&AudioWorkingCode_1_data.c:1738";
	/* <S1>/Frame Status */
	this.urlHashMap["AudioWorkingCode_1:1971:1"] = "msg=&block=AudioWorkingCode_1:1971:1";
	/* <S1>/From Workspace */
	this.urlHashMap["AudioWorkingCode_1:1971:2"] = "AudioWorkingCode_1.c:120,16693&AudioWorkingCode_1.h:93,110,138";
	/* <S1>/Outport */
	this.urlHashMap["AudioWorkingCode_1:1971:3"] = "msg=&block=AudioWorkingCode_1:1971:3";
	/* <S2>/Buffer */
	this.urlHashMap["AudioWorkingCode_1:1985:10"] = "AudioWorkingCode_1.c:230,253,381,408,50201&AudioWorkingCode_1.h:91,130,131,151,204&AudioWorkingCode_1_data.c:5040";
	/* <S2>/Frame Status */
	this.urlHashMap["AudioWorkingCode_1:1985:1"] = "msg=&block=AudioWorkingCode_1:1985:1";
	/* <S2>/From Workspace */
	this.urlHashMap["AudioWorkingCode_1:1985:2"] = "AudioWorkingCode_1.c:210,42989&AudioWorkingCode_1.h:95,122,146";
	/* <S2>/Outport */
	this.urlHashMap["AudioWorkingCode_1:1985:3"] = "msg=&block=AudioWorkingCode_1:1985:3";
	/* <S3>/Buffer */
	this.urlHashMap["AudioWorkingCode_1:1986:10"] = "AudioWorkingCode_1.c:185,208,352,379,50185,50197&AudioWorkingCode_1.h:90,128,129,150,201&AudioWorkingCode_1_data.c:3470";
	/* <S3>/Frame Status */
	this.urlHashMap["AudioWorkingCode_1:1986:1"] = "msg=&block=AudioWorkingCode_1:1986:1";
	/* <S3>/From Workspace */
	this.urlHashMap["AudioWorkingCode_1:1986:2"] = "AudioWorkingCode_1.c:165,24856&AudioWorkingCode_1.h:94,116,142";
	/* <S3>/Outport */
	this.urlHashMap["AudioWorkingCode_1:1986:3"] = "msg=&block=AudioWorkingCode_1:1986:3";
	/* <S4>/Buffer */
	this.urlHashMap["AudioWorkingCode_1:1959:10"] = "AudioWorkingCode_1.c:95,118,294,321,50177,50189&AudioWorkingCode_1.h:88,124,125,148,195&AudioWorkingCode_1_data.c:53";
	/* <S4>/Frame Status */
	this.urlHashMap["AudioWorkingCode_1:1959:1"] = "msg=&block=AudioWorkingCode_1:1959:1";
	/* <S4>/From Workspace */
	this.urlHashMap["AudioWorkingCode_1:1959:2"] = "AudioWorkingCode_1.c:75,660&AudioWorkingCode_1.h:92,104,134&AudioWorkingCode_1_private.h:31";
	/* <S4>/Outport */
	this.urlHashMap["AudioWorkingCode_1:1959:3"] = "msg=&block=AudioWorkingCode_1:1959:3";
	/* <S5>/In */
	this.urlHashMap["AudioWorkingCode_1:1961"] = "msg=&block=AudioWorkingCode_1:1961";
	/* <S5>/ 1 */
	this.urlHashMap["AudioWorkingCode_1:1962"] = "AudioWorkingCode_1.c:426";
	/* <S5>/Gain */
	this.urlHashMap["AudioWorkingCode_1:1963"] = "AudioWorkingCode_1.c:425&AudioWorkingCode_1.h:192&AudioWorkingCode_1_data.c:49";
	/* <S5>/Matrix
Sum */
	this.urlHashMap["AudioWorkingCode_1:1964"] = "AudioWorkingCode_1.c:427";
	/* <S5>/Out */
	this.urlHashMap["AudioWorkingCode_1:1965"] = "msg=&block=AudioWorkingCode_1:1965";
	/* <S6>/In */
	this.urlHashMap["AudioWorkingCode_1:1973"] = "msg=&block=AudioWorkingCode_1:1973";
	/* <S6>/ 1 */
	this.urlHashMap["AudioWorkingCode_1:1974"] = "AudioWorkingCode_1.c:455";
	/* <S6>/Gain */
	this.urlHashMap["AudioWorkingCode_1:1975"] = "AudioWorkingCode_1.c:454&AudioWorkingCode_1.h:189&AudioWorkingCode_1_data.c:46";
	/* <S6>/Matrix
Sum */
	this.urlHashMap["AudioWorkingCode_1:1976"] = "AudioWorkingCode_1.c:456";
	/* <S6>/Out */
	this.urlHashMap["AudioWorkingCode_1:1977"] = "msg=&block=AudioWorkingCode_1:1977";
	/* <S7>/In */
	this.urlHashMap["AudioWorkingCode_1:1988"] = "msg=&block=AudioWorkingCode_1:1988";
	/* <S7>/ 1 */
	this.urlHashMap["AudioWorkingCode_1:1989"] = "AudioWorkingCode_1.c:488";
	/* <S7>/Gain */
	this.urlHashMap["AudioWorkingCode_1:1990"] = "AudioWorkingCode_1.c:487&AudioWorkingCode_1.h:186&AudioWorkingCode_1_data.c:43";
	/* <S7>/Matrix
Sum */
	this.urlHashMap["AudioWorkingCode_1:1991"] = "AudioWorkingCode_1.c:489";
	/* <S7>/Out */
	this.urlHashMap["AudioWorkingCode_1:1992"] = "msg=&block=AudioWorkingCode_1:1992";
	/* <S8>/In */
	this.urlHashMap["AudioWorkingCode_1:1994"] = "msg=&block=AudioWorkingCode_1:1994";
	/* <S8>/ 1 */
	this.urlHashMap["AudioWorkingCode_1:1995"] = "AudioWorkingCode_1.c:525";
	/* <S8>/Gain */
	this.urlHashMap["AudioWorkingCode_1:1996"] = "AudioWorkingCode_1.c:524&AudioWorkingCode_1.h:183&AudioWorkingCode_1_data.c:40";
	/* <S8>/Matrix
Sum */
	this.urlHashMap["AudioWorkingCode_1:1997"] = "AudioWorkingCode_1.c:526";
	/* <S8>/Out */
	this.urlHashMap["AudioWorkingCode_1:1998"] = "msg=&block=AudioWorkingCode_1:1998";
	/* <S9>/In */
	this.urlHashMap["AudioWorkingCode_1:1967"] = "msg=&block=AudioWorkingCode_1:1967";
	/* <S9>/ 5 */
	this.urlHashMap["AudioWorkingCode_1:1968"] = "AudioWorkingCode_1.c:420,440";
	/* <S9>/Matrix
Concatenate */
	this.urlHashMap["AudioWorkingCode_1:1969"] = "msg=&block=AudioWorkingCode_1:1969";
	/* <S9>/Out */
	this.urlHashMap["AudioWorkingCode_1:1970"] = "msg=&block=AudioWorkingCode_1:1970";
	/* <S10>/In */
	this.urlHashMap["AudioWorkingCode_1:1979"] = "msg=&block=AudioWorkingCode_1:1979";
	/* <S10>/ 5 */
	this.urlHashMap["AudioWorkingCode_1:1980"] = "AudioWorkingCode_1.c:419,469";
	/* <S10>/Matrix
Concatenate */
	this.urlHashMap["AudioWorkingCode_1:1981"] = "msg=&block=AudioWorkingCode_1:1981";
	/* <S10>/Out */
	this.urlHashMap["AudioWorkingCode_1:1982"] = "msg=&block=AudioWorkingCode_1:1982";
	/* <S11>/In */
	this.urlHashMap["AudioWorkingCode_1:2001"] = "msg=&block=AudioWorkingCode_1:2001";
	/* <S11>/ 5 */
	this.urlHashMap["AudioWorkingCode_1:2002"] = "AudioWorkingCode_1.c:507,510";
	/* <S11>/Matrix
Concatenate */
	this.urlHashMap["AudioWorkingCode_1:2003"] = "msg=&block=AudioWorkingCode_1:2003";
	/* <S11>/Out */
	this.urlHashMap["AudioWorkingCode_1:2004"] = "msg=&block=AudioWorkingCode_1:2004";
	/* <S12>/In */
	this.urlHashMap["AudioWorkingCode_1:2006"] = "msg=&block=AudioWorkingCode_1:2006";
	/* <S12>/ 5 */
	this.urlHashMap["AudioWorkingCode_1:2007"] = "AudioWorkingCode_1.c:544,547";
	/* <S12>/Matrix
Concatenate */
	this.urlHashMap["AudioWorkingCode_1:2008"] = "AudioWorkingCode_1.h:86";
	/* <S12>/Out */
	this.urlHashMap["AudioWorkingCode_1:2009"] = "msg=&block=AudioWorkingCode_1:2009";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "AudioWorkingCode_1"};
	this.sidHashMap["AudioWorkingCode_1"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "AudioWorkingCode_1:1971"};
	this.sidHashMap["AudioWorkingCode_1:1971"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "AudioWorkingCode_1:1985"};
	this.sidHashMap["AudioWorkingCode_1:1985"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "AudioWorkingCode_1:1986"};
	this.sidHashMap["AudioWorkingCode_1:1986"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "AudioWorkingCode_1:1959"};
	this.sidHashMap["AudioWorkingCode_1:1959"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "AudioWorkingCode_1:1960"};
	this.sidHashMap["AudioWorkingCode_1:1960"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "AudioWorkingCode_1:1972"};
	this.sidHashMap["AudioWorkingCode_1:1972"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "AudioWorkingCode_1:1987"};
	this.sidHashMap["AudioWorkingCode_1:1987"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<S8>"] = {sid: "AudioWorkingCode_1:1993"};
	this.sidHashMap["AudioWorkingCode_1:1993"] = {rtwname: "<S8>"};
	this.rtwnameHashMap["<S9>"] = {sid: "AudioWorkingCode_1:1966"};
	this.sidHashMap["AudioWorkingCode_1:1966"] = {rtwname: "<S9>"};
	this.rtwnameHashMap["<S10>"] = {sid: "AudioWorkingCode_1:1978"};
	this.sidHashMap["AudioWorkingCode_1:1978"] = {rtwname: "<S10>"};
	this.rtwnameHashMap["<S11>"] = {sid: "AudioWorkingCode_1:2000"};
	this.sidHashMap["AudioWorkingCode_1:2000"] = {rtwname: "<S11>"};
	this.rtwnameHashMap["<S12>"] = {sid: "AudioWorkingCode_1:2005"};
	this.sidHashMap["AudioWorkingCode_1:2005"] = {rtwname: "<S12>"};
	this.rtwnameHashMap["<Root>/ALSA Audio Playback8"] = {sid: "AudioWorkingCode_1:1958"};
	this.sidHashMap["AudioWorkingCode_1:1958"] = {rtwname: "<Root>/ALSA Audio Playback8"};
	this.rtwnameHashMap["<Root>/Constant5"] = {sid: "AudioWorkingCode_1:1983"};
	this.sidHashMap["AudioWorkingCode_1:1983"] = {rtwname: "<Root>/Constant5"};
	this.rtwnameHashMap["<Root>/Constant6"] = {sid: "AudioWorkingCode_1:1984"};
	this.sidHashMap["AudioWorkingCode_1:1984"] = {rtwname: "<Root>/Constant6"};
	this.rtwnameHashMap["<Root>/Constant7"] = {sid: "AudioWorkingCode_1:2011"};
	this.sidHashMap["AudioWorkingCode_1:2011"] = {rtwname: "<Root>/Constant7"};
	this.rtwnameHashMap["<Root>/Signal From Workspace10"] = {sid: "AudioWorkingCode_1:1971"};
	this.sidHashMap["AudioWorkingCode_1:1971"] = {rtwname: "<Root>/Signal From Workspace10"};
	this.rtwnameHashMap["<Root>/Signal From Workspace11"] = {sid: "AudioWorkingCode_1:1985"};
	this.sidHashMap["AudioWorkingCode_1:1985"] = {rtwname: "<Root>/Signal From Workspace11"};
	this.rtwnameHashMap["<Root>/Signal From Workspace12"] = {sid: "AudioWorkingCode_1:1986"};
	this.sidHashMap["AudioWorkingCode_1:1986"] = {rtwname: "<Root>/Signal From Workspace12"};
	this.rtwnameHashMap["<Root>/Signal From Workspace9"] = {sid: "AudioWorkingCode_1:1959"};
	this.sidHashMap["AudioWorkingCode_1:1959"] = {rtwname: "<Root>/Signal From Workspace9"};
	this.rtwnameHashMap["<Root>/Sum left & right channels  and to single3"] = {sid: "AudioWorkingCode_1:1960"};
	this.sidHashMap["AudioWorkingCode_1:1960"] = {rtwname: "<Root>/Sum left & right channels  and to single3"};
	this.rtwnameHashMap["<Root>/Sum left & right channels  and to single4"] = {sid: "AudioWorkingCode_1:1972"};
	this.sidHashMap["AudioWorkingCode_1:1972"] = {rtwname: "<Root>/Sum left & right channels  and to single4"};
	this.rtwnameHashMap["<Root>/Sum left & right channels  and to single5"] = {sid: "AudioWorkingCode_1:1987"};
	this.sidHashMap["AudioWorkingCode_1:1987"] = {rtwname: "<Root>/Sum left & right channels  and to single5"};
	this.rtwnameHashMap["<Root>/Sum left & right channels  and to single6"] = {sid: "AudioWorkingCode_1:1993"};
	this.sidHashMap["AudioWorkingCode_1:1993"] = {rtwname: "<Root>/Sum left & right channels  and to single6"};
	this.rtwnameHashMap["<Root>/Switch"] = {sid: "AudioWorkingCode_1:1957"};
	this.sidHashMap["AudioWorkingCode_1:1957"] = {rtwname: "<Root>/Switch"};
	this.rtwnameHashMap["<Root>/Switch1"] = {sid: "AudioWorkingCode_1:1999"};
	this.sidHashMap["AudioWorkingCode_1:1999"] = {rtwname: "<Root>/Switch1"};
	this.rtwnameHashMap["<Root>/Switch2"] = {sid: "AudioWorkingCode_1:2010"};
	this.sidHashMap["AudioWorkingCode_1:2010"] = {rtwname: "<Root>/Switch2"};
	this.rtwnameHashMap["<Root>/Write to DAC3"] = {sid: "AudioWorkingCode_1:1966"};
	this.sidHashMap["AudioWorkingCode_1:1966"] = {rtwname: "<Root>/Write to DAC3"};
	this.rtwnameHashMap["<Root>/Write to DAC4"] = {sid: "AudioWorkingCode_1:1978"};
	this.sidHashMap["AudioWorkingCode_1:1978"] = {rtwname: "<Root>/Write to DAC4"};
	this.rtwnameHashMap["<Root>/Write to DAC5"] = {sid: "AudioWorkingCode_1:2000"};
	this.sidHashMap["AudioWorkingCode_1:2000"] = {rtwname: "<Root>/Write to DAC5"};
	this.rtwnameHashMap["<Root>/Write to DAC6"] = {sid: "AudioWorkingCode_1:2005"};
	this.sidHashMap["AudioWorkingCode_1:2005"] = {rtwname: "<Root>/Write to DAC6"};
	this.rtwnameHashMap["<S1>/Buffer"] = {sid: "AudioWorkingCode_1:1971:10"};
	this.sidHashMap["AudioWorkingCode_1:1971:10"] = {rtwname: "<S1>/Buffer"};
	this.rtwnameHashMap["<S1>/Frame Status"] = {sid: "AudioWorkingCode_1:1971:1"};
	this.sidHashMap["AudioWorkingCode_1:1971:1"] = {rtwname: "<S1>/Frame Status"};
	this.rtwnameHashMap["<S1>/From Workspace"] = {sid: "AudioWorkingCode_1:1971:2"};
	this.sidHashMap["AudioWorkingCode_1:1971:2"] = {rtwname: "<S1>/From Workspace"};
	this.rtwnameHashMap["<S1>/Outport"] = {sid: "AudioWorkingCode_1:1971:3"};
	this.sidHashMap["AudioWorkingCode_1:1971:3"] = {rtwname: "<S1>/Outport"};
	this.rtwnameHashMap["<S2>/Buffer"] = {sid: "AudioWorkingCode_1:1985:10"};
	this.sidHashMap["AudioWorkingCode_1:1985:10"] = {rtwname: "<S2>/Buffer"};
	this.rtwnameHashMap["<S2>/Frame Status"] = {sid: "AudioWorkingCode_1:1985:1"};
	this.sidHashMap["AudioWorkingCode_1:1985:1"] = {rtwname: "<S2>/Frame Status"};
	this.rtwnameHashMap["<S2>/From Workspace"] = {sid: "AudioWorkingCode_1:1985:2"};
	this.sidHashMap["AudioWorkingCode_1:1985:2"] = {rtwname: "<S2>/From Workspace"};
	this.rtwnameHashMap["<S2>/Outport"] = {sid: "AudioWorkingCode_1:1985:3"};
	this.sidHashMap["AudioWorkingCode_1:1985:3"] = {rtwname: "<S2>/Outport"};
	this.rtwnameHashMap["<S3>/Buffer"] = {sid: "AudioWorkingCode_1:1986:10"};
	this.sidHashMap["AudioWorkingCode_1:1986:10"] = {rtwname: "<S3>/Buffer"};
	this.rtwnameHashMap["<S3>/Frame Status"] = {sid: "AudioWorkingCode_1:1986:1"};
	this.sidHashMap["AudioWorkingCode_1:1986:1"] = {rtwname: "<S3>/Frame Status"};
	this.rtwnameHashMap["<S3>/From Workspace"] = {sid: "AudioWorkingCode_1:1986:2"};
	this.sidHashMap["AudioWorkingCode_1:1986:2"] = {rtwname: "<S3>/From Workspace"};
	this.rtwnameHashMap["<S3>/Outport"] = {sid: "AudioWorkingCode_1:1986:3"};
	this.sidHashMap["AudioWorkingCode_1:1986:3"] = {rtwname: "<S3>/Outport"};
	this.rtwnameHashMap["<S4>/Buffer"] = {sid: "AudioWorkingCode_1:1959:10"};
	this.sidHashMap["AudioWorkingCode_1:1959:10"] = {rtwname: "<S4>/Buffer"};
	this.rtwnameHashMap["<S4>/Frame Status"] = {sid: "AudioWorkingCode_1:1959:1"};
	this.sidHashMap["AudioWorkingCode_1:1959:1"] = {rtwname: "<S4>/Frame Status"};
	this.rtwnameHashMap["<S4>/From Workspace"] = {sid: "AudioWorkingCode_1:1959:2"};
	this.sidHashMap["AudioWorkingCode_1:1959:2"] = {rtwname: "<S4>/From Workspace"};
	this.rtwnameHashMap["<S4>/Outport"] = {sid: "AudioWorkingCode_1:1959:3"};
	this.sidHashMap["AudioWorkingCode_1:1959:3"] = {rtwname: "<S4>/Outport"};
	this.rtwnameHashMap["<S5>/In"] = {sid: "AudioWorkingCode_1:1961"};
	this.sidHashMap["AudioWorkingCode_1:1961"] = {rtwname: "<S5>/In"};
	this.rtwnameHashMap["<S5>/ 1"] = {sid: "AudioWorkingCode_1:1962"};
	this.sidHashMap["AudioWorkingCode_1:1962"] = {rtwname: "<S5>/ 1"};
	this.rtwnameHashMap["<S5>/Gain"] = {sid: "AudioWorkingCode_1:1963"};
	this.sidHashMap["AudioWorkingCode_1:1963"] = {rtwname: "<S5>/Gain"};
	this.rtwnameHashMap["<S5>/Matrix Sum"] = {sid: "AudioWorkingCode_1:1964"};
	this.sidHashMap["AudioWorkingCode_1:1964"] = {rtwname: "<S5>/Matrix Sum"};
	this.rtwnameHashMap["<S5>/Out"] = {sid: "AudioWorkingCode_1:1965"};
	this.sidHashMap["AudioWorkingCode_1:1965"] = {rtwname: "<S5>/Out"};
	this.rtwnameHashMap["<S6>/In"] = {sid: "AudioWorkingCode_1:1973"};
	this.sidHashMap["AudioWorkingCode_1:1973"] = {rtwname: "<S6>/In"};
	this.rtwnameHashMap["<S6>/ 1"] = {sid: "AudioWorkingCode_1:1974"};
	this.sidHashMap["AudioWorkingCode_1:1974"] = {rtwname: "<S6>/ 1"};
	this.rtwnameHashMap["<S6>/Gain"] = {sid: "AudioWorkingCode_1:1975"};
	this.sidHashMap["AudioWorkingCode_1:1975"] = {rtwname: "<S6>/Gain"};
	this.rtwnameHashMap["<S6>/Matrix Sum"] = {sid: "AudioWorkingCode_1:1976"};
	this.sidHashMap["AudioWorkingCode_1:1976"] = {rtwname: "<S6>/Matrix Sum"};
	this.rtwnameHashMap["<S6>/Out"] = {sid: "AudioWorkingCode_1:1977"};
	this.sidHashMap["AudioWorkingCode_1:1977"] = {rtwname: "<S6>/Out"};
	this.rtwnameHashMap["<S7>/In"] = {sid: "AudioWorkingCode_1:1988"};
	this.sidHashMap["AudioWorkingCode_1:1988"] = {rtwname: "<S7>/In"};
	this.rtwnameHashMap["<S7>/ 1"] = {sid: "AudioWorkingCode_1:1989"};
	this.sidHashMap["AudioWorkingCode_1:1989"] = {rtwname: "<S7>/ 1"};
	this.rtwnameHashMap["<S7>/Gain"] = {sid: "AudioWorkingCode_1:1990"};
	this.sidHashMap["AudioWorkingCode_1:1990"] = {rtwname: "<S7>/Gain"};
	this.rtwnameHashMap["<S7>/Matrix Sum"] = {sid: "AudioWorkingCode_1:1991"};
	this.sidHashMap["AudioWorkingCode_1:1991"] = {rtwname: "<S7>/Matrix Sum"};
	this.rtwnameHashMap["<S7>/Out"] = {sid: "AudioWorkingCode_1:1992"};
	this.sidHashMap["AudioWorkingCode_1:1992"] = {rtwname: "<S7>/Out"};
	this.rtwnameHashMap["<S8>/In"] = {sid: "AudioWorkingCode_1:1994"};
	this.sidHashMap["AudioWorkingCode_1:1994"] = {rtwname: "<S8>/In"};
	this.rtwnameHashMap["<S8>/ 1"] = {sid: "AudioWorkingCode_1:1995"};
	this.sidHashMap["AudioWorkingCode_1:1995"] = {rtwname: "<S8>/ 1"};
	this.rtwnameHashMap["<S8>/Gain"] = {sid: "AudioWorkingCode_1:1996"};
	this.sidHashMap["AudioWorkingCode_1:1996"] = {rtwname: "<S8>/Gain"};
	this.rtwnameHashMap["<S8>/Matrix Sum"] = {sid: "AudioWorkingCode_1:1997"};
	this.sidHashMap["AudioWorkingCode_1:1997"] = {rtwname: "<S8>/Matrix Sum"};
	this.rtwnameHashMap["<S8>/Out"] = {sid: "AudioWorkingCode_1:1998"};
	this.sidHashMap["AudioWorkingCode_1:1998"] = {rtwname: "<S8>/Out"};
	this.rtwnameHashMap["<S9>/In"] = {sid: "AudioWorkingCode_1:1967"};
	this.sidHashMap["AudioWorkingCode_1:1967"] = {rtwname: "<S9>/In"};
	this.rtwnameHashMap["<S9>/ 5"] = {sid: "AudioWorkingCode_1:1968"};
	this.sidHashMap["AudioWorkingCode_1:1968"] = {rtwname: "<S9>/ 5"};
	this.rtwnameHashMap["<S9>/Matrix Concatenate"] = {sid: "AudioWorkingCode_1:1969"};
	this.sidHashMap["AudioWorkingCode_1:1969"] = {rtwname: "<S9>/Matrix Concatenate"};
	this.rtwnameHashMap["<S9>/Out"] = {sid: "AudioWorkingCode_1:1970"};
	this.sidHashMap["AudioWorkingCode_1:1970"] = {rtwname: "<S9>/Out"};
	this.rtwnameHashMap["<S10>/In"] = {sid: "AudioWorkingCode_1:1979"};
	this.sidHashMap["AudioWorkingCode_1:1979"] = {rtwname: "<S10>/In"};
	this.rtwnameHashMap["<S10>/ 5"] = {sid: "AudioWorkingCode_1:1980"};
	this.sidHashMap["AudioWorkingCode_1:1980"] = {rtwname: "<S10>/ 5"};
	this.rtwnameHashMap["<S10>/Matrix Concatenate"] = {sid: "AudioWorkingCode_1:1981"};
	this.sidHashMap["AudioWorkingCode_1:1981"] = {rtwname: "<S10>/Matrix Concatenate"};
	this.rtwnameHashMap["<S10>/Out"] = {sid: "AudioWorkingCode_1:1982"};
	this.sidHashMap["AudioWorkingCode_1:1982"] = {rtwname: "<S10>/Out"};
	this.rtwnameHashMap["<S11>/In"] = {sid: "AudioWorkingCode_1:2001"};
	this.sidHashMap["AudioWorkingCode_1:2001"] = {rtwname: "<S11>/In"};
	this.rtwnameHashMap["<S11>/ 5"] = {sid: "AudioWorkingCode_1:2002"};
	this.sidHashMap["AudioWorkingCode_1:2002"] = {rtwname: "<S11>/ 5"};
	this.rtwnameHashMap["<S11>/Matrix Concatenate"] = {sid: "AudioWorkingCode_1:2003"};
	this.sidHashMap["AudioWorkingCode_1:2003"] = {rtwname: "<S11>/Matrix Concatenate"};
	this.rtwnameHashMap["<S11>/Out"] = {sid: "AudioWorkingCode_1:2004"};
	this.sidHashMap["AudioWorkingCode_1:2004"] = {rtwname: "<S11>/Out"};
	this.rtwnameHashMap["<S12>/In"] = {sid: "AudioWorkingCode_1:2006"};
	this.sidHashMap["AudioWorkingCode_1:2006"] = {rtwname: "<S12>/In"};
	this.rtwnameHashMap["<S12>/ 5"] = {sid: "AudioWorkingCode_1:2007"};
	this.sidHashMap["AudioWorkingCode_1:2007"] = {rtwname: "<S12>/ 5"};
	this.rtwnameHashMap["<S12>/Matrix Concatenate"] = {sid: "AudioWorkingCode_1:2008"};
	this.sidHashMap["AudioWorkingCode_1:2008"] = {rtwname: "<S12>/Matrix Concatenate"};
	this.rtwnameHashMap["<S12>/Out"] = {sid: "AudioWorkingCode_1:2009"};
	this.sidHashMap["AudioWorkingCode_1:2009"] = {rtwname: "<S12>/Out"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
