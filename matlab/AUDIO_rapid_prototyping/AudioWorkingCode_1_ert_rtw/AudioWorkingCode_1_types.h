/*
 * File: AudioWorkingCode_1_types.h
 *
 * Code generated for Simulink model 'AudioWorkingCode_1'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Thu Dec 14 15:31:26 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_AudioWorkingCode_1_types_h_
#define RTW_HEADER_AudioWorkingCode_1_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

/* Parameters (auto storage) */
typedef struct P_AudioWorkingCode_1_T_ P_AudioWorkingCode_1_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_AudioWorkingCode_1_T RT_MODEL_AudioWorkingCode_1_T;

#endif                                 /* RTW_HEADER_AudioWorkingCode_1_types_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
