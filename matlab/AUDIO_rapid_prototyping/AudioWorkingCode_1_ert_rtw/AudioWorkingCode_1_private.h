/*
 * File: AudioWorkingCode_1_private.h
 *
 * Code generated for Simulink model 'AudioWorkingCode_1'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Thu Dec 14 15:31:26 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_AudioWorkingCode_1_private_h_
#define RTW_HEADER_AudioWorkingCode_1_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "AudioWorkingCode_1.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

/* Used by FromWorkspace Block: '<S4>/From Workspace' */
#ifndef rtInterpolate
# define rtInterpolate(v1,v2,f1,f2)    (((v1)==(v2))?((double)(v1)): (((f1)*((double)(v1)))+((f2)*((double)(v2)))))
#endif

#ifndef rtRound
# define rtRound(v)                    ( ((v) >= 0) ? floor((v) + 0.5) : ceil((v) - 0.5) )
#endif

extern void AudioWorkingCode_1_step0(void);
extern void AudioWorkingCode_1_step1(void);

#endif                                 /* RTW_HEADER_AudioWorkingCode_1_private_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
