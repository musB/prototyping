function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <Root>/ALSA Audio Playback1 */
	this.urlHashMap["untitled:1"] = "untitled.c:414,16540,16572&untitled.h:123&untitled_data.c:1752";
	/* <Root>/Equalizer1 */
	this.urlHashMap["untitled:2"] = "untitled.c:373,390,16549,16566";
	/* <S1>/Band 1 
DF1 */
	this.urlHashMap["untitled:4"] = "untitled.c:63,76,375,378,16551,16554";
	/* <S1>/Band 2 
DF2 */
	this.urlHashMap["untitled:24"] = "untitled.c:126,139,380,383,16556,16559";
	/* <S1>/Band 3 
DF2T */
	this.urlHashMap["untitled:44"] = "untitled.c:188,201,385,388,16561,16564";
	/* <S1>/Coeffs for Band1 */
	this.urlHashMap["untitled:64"] = "untitled.c:85&untitled.h:132&untitled_data.c:23";
	/* <S1>/Coeffs for Band2 */
	this.urlHashMap["untitled:65"] = "untitled.c:149&untitled.h:137&untitled_data.c:30";
	/* <S1>/Coeffs for Band3 */
	this.urlHashMap["untitled:66"] = "untitled.c:210&untitled.h:142&untitled_data.c:37";
	/* <S2>/Buffer */
	this.urlHashMap["untitled:70:10"] = "untitled.c:277,299,338,363,16543&untitled.h:92,111,112,117,167&untitled_data.c:64";
	/* <S2>/From Workspace */
	this.urlHashMap["untitled:70:2"] = "untitled.c:258,507&untitled.h:93,102,115&untitled_private.h:31";
	/* <S3>/ 1 */
	this.urlHashMap["untitled:73"] = "untitled.c:366";
	/* <S3>/Gain */
	this.urlHashMap["untitled:74"] = "untitled.c:365&untitled.h:88,164&untitled_data.c:60";
	/* <S3>/Matrix
Sum */
	this.urlHashMap["untitled:75"] = "untitled.c:367";
	/* <S4>/ 5 */
	this.urlHashMap["untitled:79"] = "untitled.c:399,412";
	/* <S4>/Matrix
Concatenate */
	this.urlHashMap["untitled:80"] = "untitled.h:86";
	/* <S5>/Biquad Filter */
	this.urlHashMap["untitled:7"] = "untitled.c:98,116&untitled.h:91,109,110";
	/* <S6>/Biquad Filter */
	this.urlHashMap["untitled:27"] = "untitled.c:162,178&untitled.h:90,107";
	/* <S7>/Biquad Filter */
	this.urlHashMap["untitled:47"] = "untitled.c:223,237&untitled.h:89,105";
	/* <S8>/Add */
	this.urlHashMap["untitled:10"] = "untitled.c:84,96";
	/* <S8>/Unit Delay */
	this.urlHashMap["untitled:11"] = "untitled.c:68,73,88,118,123&untitled.h:108,133&untitled_data.c:24";
	/* <S8>/a1 */
	this.urlHashMap["untitled:12"] = "untitled.c:86&untitled.h:155&untitled_data.c:51";
	/* <S8>/b0 */
	this.urlHashMap["untitled:13"] = "untitled.c:87&untitled.h:146&untitled_data.c:42";
	/* <S10>/Add */
	this.urlHashMap["untitled:30"] = "untitled.c:148,160";
	/* <S10>/Unit Delay */
	this.urlHashMap["untitled:31"] = "untitled.c:131,136,152,180,185&untitled.h:106,138&untitled_data.c:31";
	/* <S10>/a1 */
	this.urlHashMap["untitled:32"] = "untitled.c:150&untitled.h:158&untitled_data.c:54";
	/* <S10>/b0 */
	this.urlHashMap["untitled:33"] = "untitled.c:151&untitled.h:149&untitled_data.c:45";
	/* <S12>/Add */
	this.urlHashMap["untitled:50"] = "untitled.c:209,221";
	/* <S12>/Unit Delay */
	this.urlHashMap["untitled:51"] = "untitled.c:193,198,213,239,244&untitled.h:104,143&untitled_data.c:38";
	/* <S12>/a1 */
	this.urlHashMap["untitled:52"] = "untitled.c:211&untitled.h:161&untitled_data.c:57";
	/* <S12>/b0 */
	this.urlHashMap["untitled:53"] = "untitled.c:212&untitled.h:152&untitled_data.c:48";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "untitled"};
	this.sidHashMap["untitled"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>"] = {sid: "untitled:2"};
	this.sidHashMap["untitled:2"] = {rtwname: "<S1>"};
	this.rtwnameHashMap["<S2>"] = {sid: "untitled:70"};
	this.sidHashMap["untitled:70"] = {rtwname: "<S2>"};
	this.rtwnameHashMap["<S3>"] = {sid: "untitled:71"};
	this.sidHashMap["untitled:71"] = {rtwname: "<S3>"};
	this.rtwnameHashMap["<S4>"] = {sid: "untitled:77"};
	this.sidHashMap["untitled:77"] = {rtwname: "<S4>"};
	this.rtwnameHashMap["<S5>"] = {sid: "untitled:4"};
	this.sidHashMap["untitled:4"] = {rtwname: "<S5>"};
	this.rtwnameHashMap["<S6>"] = {sid: "untitled:24"};
	this.sidHashMap["untitled:24"] = {rtwname: "<S6>"};
	this.rtwnameHashMap["<S7>"] = {sid: "untitled:44"};
	this.sidHashMap["untitled:44"] = {rtwname: "<S7>"};
	this.rtwnameHashMap["<S8>"] = {sid: "untitled:8"};
	this.sidHashMap["untitled:8"] = {rtwname: "<S8>"};
	this.rtwnameHashMap["<S9>"] = {sid: "untitled:16"};
	this.sidHashMap["untitled:16"] = {rtwname: "<S9>"};
	this.rtwnameHashMap["<S10>"] = {sid: "untitled:28"};
	this.sidHashMap["untitled:28"] = {rtwname: "<S10>"};
	this.rtwnameHashMap["<S11>"] = {sid: "untitled:36"};
	this.sidHashMap["untitled:36"] = {rtwname: "<S11>"};
	this.rtwnameHashMap["<S12>"] = {sid: "untitled:48"};
	this.sidHashMap["untitled:48"] = {rtwname: "<S12>"};
	this.rtwnameHashMap["<S13>"] = {sid: "untitled:56"};
	this.sidHashMap["untitled:56"] = {rtwname: "<S13>"};
	this.rtwnameHashMap["<Root>/ALSA Audio Playback1"] = {sid: "untitled:1"};
	this.sidHashMap["untitled:1"] = {rtwname: "<Root>/ALSA Audio Playback1"};
	this.rtwnameHashMap["<Root>/Equalizer1"] = {sid: "untitled:2"};
	this.sidHashMap["untitled:2"] = {rtwname: "<Root>/Equalizer1"};
	this.rtwnameHashMap["<Root>/Signal From Workspace1"] = {sid: "untitled:70"};
	this.sidHashMap["untitled:70"] = {rtwname: "<Root>/Signal From Workspace1"};
	this.rtwnameHashMap["<Root>/Sum left & right channels  and to single1"] = {sid: "untitled:71"};
	this.sidHashMap["untitled:71"] = {rtwname: "<Root>/Sum left & right channels  and to single1"};
	this.rtwnameHashMap["<Root>/Write to DAC1"] = {sid: "untitled:77"};
	this.sidHashMap["untitled:77"] = {rtwname: "<Root>/Write to DAC1"};
	this.rtwnameHashMap["<S1>/In"] = {sid: "untitled:3"};
	this.sidHashMap["untitled:3"] = {rtwname: "<S1>/In"};
	this.rtwnameHashMap["<S1>/Band 1  DF1"] = {sid: "untitled:4"};
	this.sidHashMap["untitled:4"] = {rtwname: "<S1>/Band 1  DF1"};
	this.rtwnameHashMap["<S1>/Band 2  DF2"] = {sid: "untitled:24"};
	this.sidHashMap["untitled:24"] = {rtwname: "<S1>/Band 2  DF2"};
	this.rtwnameHashMap["<S1>/Band 3  DF2T"] = {sid: "untitled:44"};
	this.sidHashMap["untitled:44"] = {rtwname: "<S1>/Band 3  DF2T"};
	this.rtwnameHashMap["<S1>/Coeffs for Band1"] = {sid: "untitled:64"};
	this.sidHashMap["untitled:64"] = {rtwname: "<S1>/Coeffs for Band1"};
	this.rtwnameHashMap["<S1>/Coeffs for Band2"] = {sid: "untitled:65"};
	this.sidHashMap["untitled:65"] = {rtwname: "<S1>/Coeffs for Band2"};
	this.rtwnameHashMap["<S1>/Coeffs for Band3"] = {sid: "untitled:66"};
	this.sidHashMap["untitled:66"] = {rtwname: "<S1>/Coeffs for Band3"};
	this.rtwnameHashMap["<S1>/Out"] = {sid: "untitled:67"};
	this.sidHashMap["untitled:67"] = {rtwname: "<S1>/Out"};
	this.rtwnameHashMap["<S2>/Buffer"] = {sid: "untitled:70:10"};
	this.sidHashMap["untitled:70:10"] = {rtwname: "<S2>/Buffer"};
	this.rtwnameHashMap["<S2>/Frame Status"] = {sid: "untitled:70:1"};
	this.sidHashMap["untitled:70:1"] = {rtwname: "<S2>/Frame Status"};
	this.rtwnameHashMap["<S2>/From Workspace"] = {sid: "untitled:70:2"};
	this.sidHashMap["untitled:70:2"] = {rtwname: "<S2>/From Workspace"};
	this.rtwnameHashMap["<S2>/Outport"] = {sid: "untitled:70:3"};
	this.sidHashMap["untitled:70:3"] = {rtwname: "<S2>/Outport"};
	this.rtwnameHashMap["<S3>/In"] = {sid: "untitled:72"};
	this.sidHashMap["untitled:72"] = {rtwname: "<S3>/In"};
	this.rtwnameHashMap["<S3>/ 1"] = {sid: "untitled:73"};
	this.sidHashMap["untitled:73"] = {rtwname: "<S3>/ 1"};
	this.rtwnameHashMap["<S3>/Gain"] = {sid: "untitled:74"};
	this.sidHashMap["untitled:74"] = {rtwname: "<S3>/Gain"};
	this.rtwnameHashMap["<S3>/Matrix Sum"] = {sid: "untitled:75"};
	this.sidHashMap["untitled:75"] = {rtwname: "<S3>/Matrix Sum"};
	this.rtwnameHashMap["<S3>/Out"] = {sid: "untitled:76"};
	this.sidHashMap["untitled:76"] = {rtwname: "<S3>/Out"};
	this.rtwnameHashMap["<S4>/In"] = {sid: "untitled:78"};
	this.sidHashMap["untitled:78"] = {rtwname: "<S4>/In"};
	this.rtwnameHashMap["<S4>/ 5"] = {sid: "untitled:79"};
	this.sidHashMap["untitled:79"] = {rtwname: "<S4>/ 5"};
	this.rtwnameHashMap["<S4>/Matrix Concatenate"] = {sid: "untitled:80"};
	this.sidHashMap["untitled:80"] = {rtwname: "<S4>/Matrix Concatenate"};
	this.rtwnameHashMap["<S4>/Out"] = {sid: "untitled:81"};
	this.sidHashMap["untitled:81"] = {rtwname: "<S4>/Out"};
	this.rtwnameHashMap["<S5>/In"] = {sid: "untitled:5"};
	this.sidHashMap["untitled:5"] = {rtwname: "<S5>/In"};
	this.rtwnameHashMap["<S5>/Coeffs"] = {sid: "untitled:6"};
	this.sidHashMap["untitled:6"] = {rtwname: "<S5>/Coeffs"};
	this.rtwnameHashMap["<S5>/Biquad Filter"] = {sid: "untitled:7"};
	this.sidHashMap["untitled:7"] = {rtwname: "<S5>/Biquad Filter"};
	this.rtwnameHashMap["<S5>/Smooth Coefficients (Leaky Integrator)"] = {sid: "untitled:8"};
	this.sidHashMap["untitled:8"] = {rtwname: "<S5>/Smooth Coefficients (Leaky Integrator)"};
	this.rtwnameHashMap["<S5>/Split Coefficients"] = {sid: "untitled:16"};
	this.sidHashMap["untitled:16"] = {rtwname: "<S5>/Split Coefficients"};
	this.rtwnameHashMap["<S5>/Out"] = {sid: "untitled:23"};
	this.sidHashMap["untitled:23"] = {rtwname: "<S5>/Out"};
	this.rtwnameHashMap["<S6>/In"] = {sid: "untitled:25"};
	this.sidHashMap["untitled:25"] = {rtwname: "<S6>/In"};
	this.rtwnameHashMap["<S6>/Coeffs"] = {sid: "untitled:26"};
	this.sidHashMap["untitled:26"] = {rtwname: "<S6>/Coeffs"};
	this.rtwnameHashMap["<S6>/Biquad Filter"] = {sid: "untitled:27"};
	this.sidHashMap["untitled:27"] = {rtwname: "<S6>/Biquad Filter"};
	this.rtwnameHashMap["<S6>/Smooth Coefficients (Leaky Integrator)"] = {sid: "untitled:28"};
	this.sidHashMap["untitled:28"] = {rtwname: "<S6>/Smooth Coefficients (Leaky Integrator)"};
	this.rtwnameHashMap["<S6>/Split Coefficients"] = {sid: "untitled:36"};
	this.sidHashMap["untitled:36"] = {rtwname: "<S6>/Split Coefficients"};
	this.rtwnameHashMap["<S6>/Out"] = {sid: "untitled:43"};
	this.sidHashMap["untitled:43"] = {rtwname: "<S6>/Out"};
	this.rtwnameHashMap["<S7>/In"] = {sid: "untitled:45"};
	this.sidHashMap["untitled:45"] = {rtwname: "<S7>/In"};
	this.rtwnameHashMap["<S7>/Coeffs"] = {sid: "untitled:46"};
	this.sidHashMap["untitled:46"] = {rtwname: "<S7>/Coeffs"};
	this.rtwnameHashMap["<S7>/Biquad Filter"] = {sid: "untitled:47"};
	this.sidHashMap["untitled:47"] = {rtwname: "<S7>/Biquad Filter"};
	this.rtwnameHashMap["<S7>/Smooth Coefficients (Leaky Integrator)"] = {sid: "untitled:48"};
	this.sidHashMap["untitled:48"] = {rtwname: "<S7>/Smooth Coefficients (Leaky Integrator)"};
	this.rtwnameHashMap["<S7>/Split Coefficients"] = {sid: "untitled:56"};
	this.sidHashMap["untitled:56"] = {rtwname: "<S7>/Split Coefficients"};
	this.rtwnameHashMap["<S7>/Out"] = {sid: "untitled:63"};
	this.sidHashMap["untitled:63"] = {rtwname: "<S7>/Out"};
	this.rtwnameHashMap["<S8>/In"] = {sid: "untitled:9"};
	this.sidHashMap["untitled:9"] = {rtwname: "<S8>/In"};
	this.rtwnameHashMap["<S8>/Add"] = {sid: "untitled:10"};
	this.sidHashMap["untitled:10"] = {rtwname: "<S8>/Add"};
	this.rtwnameHashMap["<S8>/Unit Delay"] = {sid: "untitled:11"};
	this.sidHashMap["untitled:11"] = {rtwname: "<S8>/Unit Delay"};
	this.rtwnameHashMap["<S8>/a1"] = {sid: "untitled:12"};
	this.sidHashMap["untitled:12"] = {rtwname: "<S8>/a1"};
	this.rtwnameHashMap["<S8>/b0"] = {sid: "untitled:13"};
	this.sidHashMap["untitled:13"] = {rtwname: "<S8>/b0"};
	this.rtwnameHashMap["<S8>/Out"] = {sid: "untitled:14"};
	this.sidHashMap["untitled:14"] = {rtwname: "<S8>/Out"};
	this.rtwnameHashMap["<S9>/In"] = {sid: "untitled:17"};
	this.sidHashMap["untitled:17"] = {rtwname: "<S9>/In"};
	this.rtwnameHashMap["<S9>/Den Selector"] = {sid: "untitled:18"};
	this.sidHashMap["untitled:18"] = {rtwname: "<S9>/Den Selector"};
	this.rtwnameHashMap["<S9>/Num Selector"] = {sid: "untitled:19"};
	this.sidHashMap["untitled:19"] = {rtwname: "<S9>/Num Selector"};
	this.rtwnameHashMap["<S9>/Num"] = {sid: "untitled:20"};
	this.sidHashMap["untitled:20"] = {rtwname: "<S9>/Num"};
	this.rtwnameHashMap["<S9>/Den"] = {sid: "untitled:21"};
	this.sidHashMap["untitled:21"] = {rtwname: "<S9>/Den"};
	this.rtwnameHashMap["<S10>/In"] = {sid: "untitled:29"};
	this.sidHashMap["untitled:29"] = {rtwname: "<S10>/In"};
	this.rtwnameHashMap["<S10>/Add"] = {sid: "untitled:30"};
	this.sidHashMap["untitled:30"] = {rtwname: "<S10>/Add"};
	this.rtwnameHashMap["<S10>/Unit Delay"] = {sid: "untitled:31"};
	this.sidHashMap["untitled:31"] = {rtwname: "<S10>/Unit Delay"};
	this.rtwnameHashMap["<S10>/a1"] = {sid: "untitled:32"};
	this.sidHashMap["untitled:32"] = {rtwname: "<S10>/a1"};
	this.rtwnameHashMap["<S10>/b0"] = {sid: "untitled:33"};
	this.sidHashMap["untitled:33"] = {rtwname: "<S10>/b0"};
	this.rtwnameHashMap["<S10>/Out"] = {sid: "untitled:34"};
	this.sidHashMap["untitled:34"] = {rtwname: "<S10>/Out"};
	this.rtwnameHashMap["<S11>/In"] = {sid: "untitled:37"};
	this.sidHashMap["untitled:37"] = {rtwname: "<S11>/In"};
	this.rtwnameHashMap["<S11>/Den Selector"] = {sid: "untitled:38"};
	this.sidHashMap["untitled:38"] = {rtwname: "<S11>/Den Selector"};
	this.rtwnameHashMap["<S11>/Num Selector"] = {sid: "untitled:39"};
	this.sidHashMap["untitled:39"] = {rtwname: "<S11>/Num Selector"};
	this.rtwnameHashMap["<S11>/Num"] = {sid: "untitled:40"};
	this.sidHashMap["untitled:40"] = {rtwname: "<S11>/Num"};
	this.rtwnameHashMap["<S11>/Den"] = {sid: "untitled:41"};
	this.sidHashMap["untitled:41"] = {rtwname: "<S11>/Den"};
	this.rtwnameHashMap["<S12>/In"] = {sid: "untitled:49"};
	this.sidHashMap["untitled:49"] = {rtwname: "<S12>/In"};
	this.rtwnameHashMap["<S12>/Add"] = {sid: "untitled:50"};
	this.sidHashMap["untitled:50"] = {rtwname: "<S12>/Add"};
	this.rtwnameHashMap["<S12>/Unit Delay"] = {sid: "untitled:51"};
	this.sidHashMap["untitled:51"] = {rtwname: "<S12>/Unit Delay"};
	this.rtwnameHashMap["<S12>/a1"] = {sid: "untitled:52"};
	this.sidHashMap["untitled:52"] = {rtwname: "<S12>/a1"};
	this.rtwnameHashMap["<S12>/b0"] = {sid: "untitled:53"};
	this.sidHashMap["untitled:53"] = {rtwname: "<S12>/b0"};
	this.rtwnameHashMap["<S12>/Out"] = {sid: "untitled:54"};
	this.sidHashMap["untitled:54"] = {rtwname: "<S12>/Out"};
	this.rtwnameHashMap["<S13>/In"] = {sid: "untitled:57"};
	this.sidHashMap["untitled:57"] = {rtwname: "<S13>/In"};
	this.rtwnameHashMap["<S13>/Den Selector"] = {sid: "untitled:58"};
	this.sidHashMap["untitled:58"] = {rtwname: "<S13>/Den Selector"};
	this.rtwnameHashMap["<S13>/Num Selector"] = {sid: "untitled:59"};
	this.sidHashMap["untitled:59"] = {rtwname: "<S13>/Num Selector"};
	this.rtwnameHashMap["<S13>/Num"] = {sid: "untitled:60"};
	this.sidHashMap["untitled:60"] = {rtwname: "<S13>/Num"};
	this.rtwnameHashMap["<S13>/Den"] = {sid: "untitled:61"};
	this.sidHashMap["untitled:61"] = {rtwname: "<S13>/Den"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
