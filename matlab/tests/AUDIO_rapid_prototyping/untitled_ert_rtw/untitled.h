/*
 * File: untitled.h
 *
 * Code generated for Simulink model 'untitled'.
 *
 * Model version                  : 1.0
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Thu Dec 14 14:31:53 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_untitled_h_
#define RTW_HEADER_untitled_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef untitled_COMMON_INCLUDES_
# define untitled_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* untitled_COMMON_INCLUDES_ */

#include "untitled_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals (auto storage) */
typedef struct {
  real32_T MatrixConcatenate[44100];   /* '<S4>/Matrix Concatenate' */
  int16_T rtb_MatrixConcatenate_m[44100];
  real32_T Gain[22050];                /* '<S3>/Gain' */
  real32_T BiquadFilter[22050];        /* '<S7>/Biquad Filter' */
  real32_T BiquadFilter_k[22050];      /* '<S6>/Biquad Filter' */
  real32_T BiquadFilter_a[22050];      /* '<S5>/Biquad Filter' */
  int16_T Buffer[22050];               /* '<S2>/Buffer' */
  int16_T FromWorkspace;               /* '<S2>/From Workspace' */
} B_untitled_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK;               /* '<S2>/From Workspace' */

  real32_T UnitDelay_DSTATE[5];        /* '<S12>/Unit Delay' */
  real32_T BiquadFilter_FILT_STATES[2];/* '<S7>/Biquad Filter' */
  real32_T UnitDelay_DSTATE_b[5];      /* '<S10>/Unit Delay' */
  real32_T BiquadFilter_FILT_STATES_o[2];/* '<S6>/Biquad Filter' */
  real32_T UnitDelay_DSTATE_m[5];      /* '<S8>/Unit Delay' */
  real32_T BiquadFilter_ZERO_STATES[2];/* '<S5>/Biquad Filter' */
  real32_T BiquadFilter_POLE_STATES[2];/* '<S5>/Biquad Filter' */
  int32_T Buffer_inBufPtrIdx;          /* '<S2>/Buffer' */
  int32_T Buffer_outBufPtrIdx;         /* '<S2>/Buffer' */
  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK;               /* '<S2>/From Workspace' */

  int16_T Buffer_CircBuf[44100];       /* '<S2>/Buffer' */
} DW_untitled_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: device
   * Referenced by: '<Root>/ALSA Audio Playback1'
   */
  uint8_T ALSAAudioPlayback1_p1[8];
} ConstP_untitled_T;

/* Parameters (auto storage) */
struct P_untitled_T_ {
  real32_T CoeffsMatrix1[5];           /* Variable: CoeffsMatrix1
                                        * Referenced by:
                                        *   '<S1>/Coeffs for Band1'
                                        *   '<S8>/Unit Delay'
                                        */
  real32_T CoeffsMatrix2[5];           /* Variable: CoeffsMatrix2
                                        * Referenced by:
                                        *   '<S1>/Coeffs for Band2'
                                        *   '<S10>/Unit Delay'
                                        */
  real32_T CoeffsMatrix3[5];           /* Variable: CoeffsMatrix3
                                        * Referenced by:
                                        *   '<S1>/Coeffs for Band3'
                                        *   '<S12>/Unit Delay'
                                        */
  real32_T SmoothCoefficientsLeakyIntegrat;/* Mask Parameter: SmoothCoefficientsLeakyIntegrat
                                            * Referenced by: '<S8>/b0'
                                            */
  real32_T SmoothCoefficientsLeakyIntegr_f;/* Mask Parameter: SmoothCoefficientsLeakyIntegr_f
                                            * Referenced by: '<S10>/b0'
                                            */
  real32_T SmoothCoefficientsLeakyIntegr_p;/* Mask Parameter: SmoothCoefficientsLeakyIntegr_p
                                            * Referenced by: '<S12>/b0'
                                            */
  real32_T a1_Gain;                    /* Computed Parameter: a1_Gain
                                        * Referenced by: '<S8>/a1'
                                        */
  real32_T a1_Gain_d;                  /* Computed Parameter: a1_Gain_d
                                        * Referenced by: '<S10>/a1'
                                        */
  real32_T a1_Gain_k;                  /* Computed Parameter: a1_Gain_k
                                        * Referenced by: '<S12>/a1'
                                        */
  real32_T Gain_Gain;                  /* Computed Parameter: Gain_Gain
                                        * Referenced by: '<S3>/Gain'
                                        */
  int16_T Buffer_ic[22050];            /* Computed Parameter: Buffer_ic
                                        * Referenced by: '<S2>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_untitled_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    struct {
      uint16_T TID[2];
    } TaskCounters;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_untitled_T untitled_P;

/* Block signals (auto storage) */
extern B_untitled_T untitled_B;

/* Block states (auto storage) */
extern DW_untitled_T untitled_DW;

/* Constant parameters (auto storage) */
extern const ConstP_untitled_T untitled_ConstP;

/* External function called from main */
extern void untitled_SetEventsForThisBaseStep(boolean_T *eventFlags);

/* Model entry point functions */
extern void untitled_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void untitled_initialize(void);
extern void untitled_step(int_T tid);
extern void untitled_terminate(void);

/* Real-time Model object */
extern RT_MODEL_untitled_T *const untitled_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'untitled'
 * '<S1>'   : 'untitled/Equalizer1'
 * '<S2>'   : 'untitled/Signal From Workspace1'
 * '<S3>'   : 'untitled/Sum left & right channels  and to single1'
 * '<S4>'   : 'untitled/Write to DAC1'
 * '<S5>'   : 'untitled/Equalizer1/Band 1  DF1'
 * '<S6>'   : 'untitled/Equalizer1/Band 2  DF2'
 * '<S7>'   : 'untitled/Equalizer1/Band 3  DF2T'
 * '<S8>'   : 'untitled/Equalizer1/Band 1  DF1/Smooth Coefficients (Leaky Integrator)'
 * '<S9>'   : 'untitled/Equalizer1/Band 1  DF1/Split Coefficients'
 * '<S10>'  : 'untitled/Equalizer1/Band 2  DF2/Smooth Coefficients (Leaky Integrator)'
 * '<S11>'  : 'untitled/Equalizer1/Band 2  DF2/Split Coefficients'
 * '<S12>'  : 'untitled/Equalizer1/Band 3  DF2T/Smooth Coefficients (Leaky Integrator)'
 * '<S13>'  : 'untitled/Equalizer1/Band 3  DF2T/Split Coefficients'
 */
#endif                                 /* RTW_HEADER_untitled_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
