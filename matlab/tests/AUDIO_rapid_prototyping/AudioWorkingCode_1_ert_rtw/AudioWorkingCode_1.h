/*
 * File: AudioWorkingCode_1.h
 *
 * Code generated for Simulink model 'AudioWorkingCode_1'.
 *
 * Model version                  : 1.12
 * Simulink Coder version         : 8.12 (R2017a) 16-Feb-2017
 * C/C++ source code generated on : Thu Dec 14 15:31:26 2017
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_AudioWorkingCode_1_h_
#define RTW_HEADER_AudioWorkingCode_1_h_
#include <math.h>
#include <string.h>
#include <float.h>
#include <stddef.h>
#ifndef AudioWorkingCode_1_COMMON_INCLUDES_
# define AudioWorkingCode_1_COMMON_INCLUDES_
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "alsa_audio_playback_macro.h"
#endif                                 /* AudioWorkingCode_1_COMMON_INCLUDES_ */

#include "AudioWorkingCode_1_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm)     ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals (auto storage) */
typedef struct {
  real32_T MatrixConcatenate_m[44100]; /* '<S12>/Matrix Concatenate' */
  int16_T Switch[44100];               /* '<Root>/Switch' */
  int16_T Buffer[22050];               /* '<S4>/Buffer' */
  int16_T Buffer_d[22050];             /* '<S1>/Buffer' */
  int16_T Buffer_m[22050];             /* '<S3>/Buffer' */
  int16_T Buffer_ms[22050];            /* '<S2>/Buffer' */
  int16_T FromWorkspace;               /* '<S4>/From Workspace' */
  int16_T FromWorkspace_h;             /* '<S1>/From Workspace' */
  int16_T FromWorkspace_l;             /* '<S3>/From Workspace' */
  int16_T FromWorkspace_i;             /* '<S2>/From Workspace' */
} B_AudioWorkingCode_1_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK;               /* '<S4>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_b;             /* '<S1>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_m;             /* '<S3>/From Workspace' */

  struct {
    void *TimePtr;
    void *DataPtr;
    void *RSimInfoPtr;
  } FromWorkspace_PWORK_l;             /* '<S2>/From Workspace' */

  int32_T Buffer_inBufPtrIdx;          /* '<S4>/Buffer' */
  int32_T Buffer_outBufPtrIdx;         /* '<S4>/Buffer' */
  int32_T Buffer_inBufPtrIdx_l;        /* '<S1>/Buffer' */
  int32_T Buffer_outBufPtrIdx_f;       /* '<S1>/Buffer' */
  int32_T Buffer_inBufPtrIdx_n;        /* '<S3>/Buffer' */
  int32_T Buffer_outBufPtrIdx_h;       /* '<S3>/Buffer' */
  int32_T Buffer_inBufPtrIdx_i;        /* '<S2>/Buffer' */
  int32_T Buffer_outBufPtrIdx_g;       /* '<S2>/Buffer' */
  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK;               /* '<S4>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_o;             /* '<S1>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_m;             /* '<S3>/From Workspace' */

  struct {
    int_T PrevIndex;
  } FromWorkspace_IWORK_f;             /* '<S2>/From Workspace' */

  int16_T Buffer_CircBuf[44100];       /* '<S4>/Buffer' */
  int16_T Buffer_CircBuf_p[44100];     /* '<S1>/Buffer' */
  int16_T Buffer_CircBuf_i[44100];     /* '<S3>/Buffer' */
  int16_T Buffer_CircBuf_o[44100];     /* '<S2>/Buffer' */
} DW_AudioWorkingCode_1_T;

/* Constant parameters (auto storage) */
typedef struct {
  /* Expression: device
   * Referenced by: '<Root>/ALSA Audio Playback8'
   */
  uint8_T ALSAAudioPlayback8_p1[8];
} ConstP_AudioWorkingCode_1_T;

/* Parameters (auto storage) */
struct P_AudioWorkingCode_1_T_ {
  real_T Constant6_Value;              /* Expression: 1
                                        * Referenced by: '<Root>/Constant6'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<Root>/Switch1'
                                        */
  real_T Constant5_Value;              /* Expression: 1
                                        * Referenced by: '<Root>/Constant5'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<Root>/Switch'
                                        */
  real_T Constant7_Value;              /* Expression: 1
                                        * Referenced by: '<Root>/Constant7'
                                        */
  real_T Switch2_Threshold;            /* Expression: 0
                                        * Referenced by: '<Root>/Switch2'
                                        */
  real32_T Gain_Gain;                  /* Computed Parameter: Gain_Gain
                                        * Referenced by: '<S8>/Gain'
                                        */
  real32_T Gain_Gain_d;                /* Computed Parameter: Gain_Gain_d
                                        * Referenced by: '<S7>/Gain'
                                        */
  real32_T Gain_Gain_a;                /* Computed Parameter: Gain_Gain_a
                                        * Referenced by: '<S6>/Gain'
                                        */
  real32_T Gain_Gain_e;                /* Computed Parameter: Gain_Gain_e
                                        * Referenced by: '<S5>/Gain'
                                        */
  int16_T Buffer_ic[22050];            /* Computed Parameter: Buffer_ic
                                        * Referenced by: '<S4>/Buffer'
                                        */
  int16_T Buffer_ic_l[22050];          /* Computed Parameter: Buffer_ic_l
                                        * Referenced by: '<S1>/Buffer'
                                        */
  int16_T Buffer_ic_a[22050];          /* Computed Parameter: Buffer_ic_a
                                        * Referenced by: '<S3>/Buffer'
                                        */
  int16_T Buffer_ic_d[22050];          /* Computed Parameter: Buffer_ic_d
                                        * Referenced by: '<S2>/Buffer'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_AudioWorkingCode_1_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    uint32_T clockTick1;
    struct {
      uint16_T TID[2];
    } TaskCounters;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (auto storage) */
extern P_AudioWorkingCode_1_T AudioWorkingCode_1_P;

/* Block signals (auto storage) */
extern B_AudioWorkingCode_1_T AudioWorkingCode_1_B;

/* Block states (auto storage) */
extern DW_AudioWorkingCode_1_T AudioWorkingCode_1_DW;

/* Constant parameters (auto storage) */
extern const ConstP_AudioWorkingCode_1_T AudioWorkingCode_1_ConstP;

/* External function called from main */
extern void AudioWorkingCode_1_SetEventsForThisBaseStep(boolean_T *eventFlags);

/* Model entry point functions */
extern void AudioWorkingCode_1_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void AudioWorkingCode_1_initialize(void);
extern void AudioWorkingCode_1_step(int_T tid);
extern void AudioWorkingCode_1_terminate(void);

/* Real-time Model object */
extern RT_MODEL_AudioWorkingCode_1_T *const AudioWorkingCode_1_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'AudioWorkingCode_1'
 * '<S1>'   : 'AudioWorkingCode_1/Signal From Workspace10'
 * '<S2>'   : 'AudioWorkingCode_1/Signal From Workspace11'
 * '<S3>'   : 'AudioWorkingCode_1/Signal From Workspace12'
 * '<S4>'   : 'AudioWorkingCode_1/Signal From Workspace9'
 * '<S5>'   : 'AudioWorkingCode_1/Sum left & right channels  and to single3'
 * '<S6>'   : 'AudioWorkingCode_1/Sum left & right channels  and to single4'
 * '<S7>'   : 'AudioWorkingCode_1/Sum left & right channels  and to single5'
 * '<S8>'   : 'AudioWorkingCode_1/Sum left & right channels  and to single6'
 * '<S9>'   : 'AudioWorkingCode_1/Write to DAC3'
 * '<S10>'  : 'AudioWorkingCode_1/Write to DAC4'
 * '<S11>'  : 'AudioWorkingCode_1/Write to DAC5'
 * '<S12>'  : 'AudioWorkingCode_1/Write to DAC6'
 */
#endif                                 /* RTW_HEADER_AudioWorkingCode_1_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
